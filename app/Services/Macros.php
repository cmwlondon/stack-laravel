<?php
namespace App\Services;
use Collective\Html\FormBuilder;

class Macros extends FormBuilder {

	// select colour for people item name underline 
	public function selectPeopleColour($name, $selected = null, $options = array())
    {
		$peopleColourOptions = [
			'' => 'Select Colour',
			'lineGreen' => 'Green',
			'lineBlue' => 'Blue',
			'linePurple' => 'Purple',
			'lineYellow' => 'Yellow'
		];

		return $this->select($name, $peopleColourOptions, $selected, $options);
    }

    // select underline colour for casestudy thumbnail

    // select casestudy page colour scheme (headers/back link underlines)

    // select module type for case study module
    /* 	id, title, ref (template blade name)
	1 evaluate2video
	2 copy
	3 cev2header
	4 inline-image
	5 image-grid
	6 video-manual
	7 brochure-video
	8 reveal-image
    */
}