<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class AdminService extends Facade{
	// App/Providers/AdminServiceProvider register bind name 'adminServiceFacade'
    protected static function getFacadeAccessor() { return 'adminServiceFacade'; }
}