<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Models\Casestudy;
use App\Models\Module;
use App\Models\Module_Types;
use App\Models\Casestudies_Modules;

class WorkController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// no landing page for this, only item pages
	// work/[title]/[id]
	public function index(Casestudy $casestudies)
	{	
		// model: work

		$this->context['meta']['title'] = 'STACK Our work';
		$this->context['meta']['desc'] = 'Our latest campaigns featuring CRM, digital, content, branding, social and technology.';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['pageClass'] = 'workindex';
		$this->context['colourScheme'] = 'colourSchemeBlue';

		$this->context['casestudies'] = $casestudies->getAllCasestudies();

		return view('Main.Work.index', $this->context);
	}

	// public function casestudy(string $slug, int $id, Casestudy $casestudies, Module $module)
	public function casestudy(string $slug, Casestudy $casestudies, Module $module, Module_Types $moduleTypes)
	{	
		// model: work
		// $thisCasestudy = $casestudies->$casestudyslug($id);
		$thisCasestudy = $casestudies->getCasestudyBySlug($slug);

		$this->context['slug'] = $slug;
		$this->context['id'] = $thisCasestudy['id'];

		$this->context['meta']['title'] = $thisCasestudy['meta_title'];
		$this->context['meta']['desc'] = $thisCasestudy['meta_desc'];
		$this->context['meta']['keywords'] = $thisCasestudy['meta_keywords'];
		$this->context['pageClass'] = 'casestudy';
		$this->context['colourScheme'] = $thisCasestudy['page_colour'];
		$this->context['category'] = $thisCasestudy['branch'];
		$this->context['modules'] = [];
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['test'] = [
			'slug' => $slug,
			'id' => $thisCasestudy['id'],
			'title' => $thisCasestudy['meta_title'],
			'module_ids' => $thisCasestudy['modules']
		];

		// currently uses 'modules' field in casestudies table to link a case study to its modules
		$moduleList = explode(',',$thisCasestudy['modules']);

		if ( count($moduleList) > 1 )
		{
			foreach( $moduleList AS $moduleID )
			{
				$_module = $module->where('id', '=', $moduleID)->first();
				
				$_moduleType = $moduleTypes->getByID( $_module->type );

				$this->context['modules'][] = [
					'module-id' => $moduleID,
					'type-id' => $_module->type,
					'type' => $_moduleType->ref,
					'meta' => unserialize($_module->meta)
				];
			}
		}

		return view('Main.Work.casestudy', $this->context);
	}

}
