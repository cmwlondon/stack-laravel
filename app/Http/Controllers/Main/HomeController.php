<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Models\Panel;

class HomeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function index(Panel $panels)
	{	
		$this->context['isHomePage'] = true;

		$this->context['meta']['title'] = 'STACK Creativity, technology, strategy and data';
		$this->context['meta']['desc'] = 'One of UK\'s leading customer agencies, specialising in CRM, digital and social advertising.';
		$this->context['meta']['keywords'] = 'STACK msq partners creativity technology strategy data';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['panels'] = $panels->getAll();
		
		return view('Main.Home.index', $this->context);
	}

}
