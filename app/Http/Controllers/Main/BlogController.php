<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

use Illuminate\Http\Request;

use App\Models\Blog;
use App\Models\Person;
use App\Models\Categories;
use App\Models\Tags;
use App\Models\RelatedLinks;

use App\Models\BlogAuthors;
use App\Models\BlogCategories;
use App\Models\BlogTags;
use App\Models\BlogRelatedLinks;

use App\Models\Metadata;

/*
use App\Models\Casestudy;
use App\Models\Module;
use App\Models\Module_Types;
use App\Models\Casestudies_Modules;
*/

class BlogController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Blog $blog)
	{
		parent::__construct();
		$this->blog = $blog;
		// authors
		// category
		// tags
		// related link
	}

/* ------------------------------------------------------ */

	// blog index page
	// set filter: launches,news,thought-pieces
	// display featured post + filtered posts or ignore featured post unless it is in selected category?
	public function index(Request $request)
	{	
		// get blog filter from url
		switch( $request->path() )
		{
			case 'blog' :
			case 'blog/' : {
				$filter ='';
			} break;
			case 'blog/news' : {
				$filter = 'news';
			} break;
			case 'blog/launch' : {
				$filter = 'launch';
			} break;
			case 'blog/stack-thinking' : {
				$filter = 'stack-thinking';
			} break;
		}

		$this->context['meta']['title'] = 'STACK Blog';
		$this->context['meta']['desc'] = 'The latest news and thoughts from STACK';
		$this->context['meta']['author'] = 'STACK';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['type'] = 'article';

		$this->context['pageClass'] = 'blogindex';
		$this->context['colourScheme'] = 'colourSchemeBlue';

		// build options for filter control widget
		$this->context['filters'] = [
			[
				'caption' => 'All',
				'value' => ''
			],
			[
				'caption' => 'News',
				'value' => 'news'
			],
			[
				'caption' => 'Campaign Launch',
				'value' => 'launch'
			],
			[
				'caption' => 'STACK Thinking',
				'value' => 'stack-thinking'
			],
		];
		$this->context['filter'] = $filter;

		$xPosts = [];

		// need to seperate out featured post and thre rest of the posts
		// get featured post id from metadata
		$featuredPostMetadata = Metadata::where('datakey','=','featured-post')->select('data')->first();

		if( $featuredPostMetadata->data !== '0')
		{
			$featuredPost = $this->blog->where('id','=',$featuredPostMetadata->data)->first();

			// get post authors
			// $authors = array_pluck($this->getAuthors($featuredPost->id),'name','id');

			// get category
			$postCategory = $this->getCategory($featuredPost->id);
			// remap category title
			switch( $postCategory->slug ) {
				case 'stack-thinking' : {
					$categoryTitle = 'STACK Thinking';
				} break;
				case 'news' : {
					$categoryTitle = 'News';
				} break;
				case 'launch' : {
					$categoryTitle = 'Campaign Launch';
				} break;
			}

			// $thisPostTimestamp = $featuredPost->created_at;
			// $postDate = $thisPostTimestamp->format('jS F Y');
		
			// add features post to page as first post
			$xPosts[] = [
				'id' => $featuredPost->id,
				'title' => $featuredPost->title,
				'slug' => $featuredPost->slug,
				'thumb' => $featuredPost->thumb,
				'image' => $featuredPost->image,
				'category' => $postCategory->slug,
				'categoryTitle' => $categoryTitle,
				'synopsis' => $featuredPost->synopsis,
				// 'timestamp' => $postDate,
				// 'authors' => implode(',',$authors),
				'featured' => true
			];

			// add rest of posts after featured post, in date order
			$latestPosts = $this->blog->where([['id','<>',$featuredPostMetadata->data],['status','=','published']])->orderBy('created_at','desc')->get();	
		} else {
			// list all posts in date order
			$latestPosts = $this->blog->where('status','=','published')->orderBy('created_at','desc')->get();
		}

		foreach( $latestPosts AS $post )
		{
			// get category
			$postCategory = $this->getCategory($post->id);
			// remap category title
			switch( $postCategory->slug ) {
				case 'stack-thinking' : {
					$categoryTitle = 'STACK Thinking';
				} break;
				case 'news' : {
					$categoryTitle = 'News';
				} break;
				case 'launch' : {
					$categoryTitle = 'Campaign Launch';
				} break;
			}

			// compare category against filter if filter is not 'all'
			if ( $filter === '' || $filter === $postCategory->slug )
			{
				// get post authors
				// $authors = array_pluck($this->getAuthors($post->id),'name','id');

				// $thisPostTimestamp = $post->created_at;
				// $postDate = $thisPostTimestamp->format('jS F Y');
			
				$xPosts[] = [
					'id' => $post->id,
					'title' => $post->title,
					'category' => $postCategory->slug,
					'categoryTitle' => $categoryTitle,
					'slug' => $post->slug,
					'thumb' => $post->thumb,
					'image' => $post->image,
					'synopsis' => $post->synopsis,
					// 'timestamp' => $postDate,
					// 'authors' => implode(',',$authors),
					'featured' => false
				];
			}
		}
		$this->context['blog'] = $xPosts;

		$this->context['test'] = [
		];

		return view('Main.Blog.index', $this->context);
	}

/* ------------------------------------------------------ */

	// public function casestudy(string $slug, int $id, Casestudy $casestudies, Module $module)
	public function post(string $slug)
	{	
		// firstorfail -> route to 404
		$thisBlogPost = $this->blog->where('slug','=',$slug)->first();

		// check for invalid blog post slug, route to 404
		if (is_null($thisBlogPost))
		{
			return redirect('404');
		}

		$thisPostTimestamp = $thisBlogPost->created_at;
		$postDate = $thisPostTimestamp->format('jS F Y');
		
		/*
		// get next/previous post ids
		$previous = $this->blog->where('created_at','<',$thisPostTimestamp)->select('slug','title')->first();
		$next = $this->blog->where('created_at','>',$thisPostTimestamp)->select('slug','title')->first();
		*/

		// get category
		$category = $this->getCategory($thisBlogPost->id);

		// get post authors
		$authors = $this->getAuthors($thisBlogPost->id);

		$tags = $this->getTags($thisBlogPost->id);

		$relatedLinks = $this->getRelatedLinks($thisBlogPost->id);

		$this->context['slug'] = $slug;
		$this->context['id'] = $thisBlogPost->id;
		$this->context['post'] = $thisBlogPost;
		$this->context['authors'] = $authors;
		$this->context['tags'] = $tags;
		$this->context['links'] = $relatedLinks;
		$this->context['postDate'] = $postDate;

		$this->context['meta']['og']['title'] = $thisBlogPost->title;
		$this->context['meta']['og']['type'] = 'article';
		// $this->context['meta']['og']['url'] = url()->current();
		$this->context['meta']['og']['image'] = '-';

		// set page metadata
		$this->context['meta']['title'] = $thisBlogPost->title;
		$this->context['meta']['desc'] = $thisBlogPost->synopsis;
		$this->context['meta']['keywords'] = implode(', ',array_pluck($tags,'text'));
		$this->context['meta']['author'] = (count($authors) > 0) ? implode(', ',array_pluck($authors,'name')) : 'STACK';

		$this->context['pageClass'] = 'blogpost';
		$this->context['colourScheme'] = $category->slug;

		// $this->context['previous'] = ($previous) ? [ 'title' => $previous->title, 'slug' => $previous->slug ] : null;
		// $this->context['next'] = ($next) ? ['title' => $next->title, 'slug' => $next->slug ] : null;

		/*
		$this->context['test'] = [
			'slug' => $slug,
			'id' => $thisBlogPost->id,
			'title' => $thisBlogPost->meta_title,
			'category' => $category->title,
			'tags' => implode(', ',array_pluck($postTagdata,'text')),
			'authors' => implode(', ',array_pluck($authors,'name'))
			// 'links' => $relatedLinks
			// 'post' => $thisBlogPost,
			// 'previous' => ($previous) ? [ 'title' => $previous->title, 'slug' => $previous->slug ] : null,
			// 'next' => ($next) ? ['title' => $next->title, 'slug' => $next->slug ] : null
		];
		*/

		return view('Main.Blog.post', $this->context);
	}

/* ------------------------------------------------------ */

	// move these to Blog model
	// issues with referencing other Models in Blog Model?
	private function getAuthors(int $postid)
	{	
		$thisPostAuthors = BlogAuthors::where('blog_id','=',$postid)->get();

		$aid = [];
		$authors = [];

		// get author IDs for post
		foreach ($thisPostAuthors AS $postAuthorLink)
		{
			$aid[] = $postAuthorLink->people_id;
			
		}

		// get author data
		return Person::wherein('id',$aid)->get();
	}

/* ------------------------------------------------------ */

	private function getCategory(int $postid)
	{	
		$blog_category = BlogCategories::where('blog_id','=',$postid)->select('categories_id')->first();
		$category = Categories::where('id','=', $blog_category['categories_id'])->first();
		return $category;
	}

/* ------------------------------------------------------ */

	private function getTags(int $id)
	{	
		$blogTagIDs = BlogTags::where([
			['blog_id', '=', $id]
		])->select('tags_id')->get();

		$LinkedTagIds = array_pluck($blogTagIDs,'tags_id');

		$linkedTags = Tags::findMany($LinkedTagIds);

		return $linkedTags;
	}

/* ------------------------------------------------------ */

	// interim: get posts with same category
	private function getRelatedLinks(int $id)
	{	
		/*
		// get category of post
		$targetCategory = $this->getCategory($id);

		// find ids ofposts with same category, excluding the original post
		$similarPostIDs = BlogCategories::where([
			['categories_id','=',$targetCategory->id],
			['blog_id','<>',$id]
		])->select('blog_id')->get();

		// get posts using list of ids
		$relatedPosts = $this->blog->whereIn('id', array_pluck($similarPostIDs,'blog_id'))->select('id','title','slug','thumb')->get();

		return $relatedPosts;
		*/

		$blogRelatedLinks = BlogRelatedLinks::where([
			['blog_id', '=', $id]
		])->select('links_id')->get();

		$LinkedLinkIds = array_pluck($blogRelatedLinks,'links_id');

		$linkedLinks = RelatedLinks::findMany($LinkedLinkIds);

		return $linkedLinks;

	}

/* ------------------------------------------------------ */

}
