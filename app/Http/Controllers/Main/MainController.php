<?php namespace App\Http\Controllers\Main;

use App;
use Session;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class MainController extends Controller {

	public function __construct() {
		$this->uri = Request::path();
		// Context now pulled from config so we can use it in error pages.
		$this->context = [];
		$this->context['meta'] = [];
		$this->context['meta']['title'] = 'PAGE TITLE';
		$this->context['meta']['desc'] = 'One of UK\'s leading customer agencies, specialising in CRM, digital and social advertising.';
		$this->context['meta']['keywords'] = 'PAGE KEYWORDS';
		$this->context['meta']['keyphrases'] = 'PAGE KEYPHRASES';
		$this->context['meta']['author'] = 'STACK';
		$this->context['meta']['pagetype'] = 'article';

		// OpenGraph
		// override in page controller
		/*
		<meta property="og:title" content="The Rock" />
		<meta property="og:type" content="video.movie" />
		<meta property="og:url" content="http://www.imdb.com/title/tt0117500/" />
		<meta property="og:image" content="http://ia.media-imdb.com/images/rock.jpg" />
		*/
		$this->context['meta']['og'] = [
			'title' => '',
			'type' => 'article',
			'url' => url()->current(),
			'image' => '-',
		];
		$this->domain = env('APP_URL'); // APP_URL in .env, no trailing slash
		$this->context['canonical'] = url()->current();
		$this->context['gaSiteID'] = env('GA_SITE_ID');

		$this->context['isHomePage'] = false;
		$this->context['pageClass'] = '';
		$this->context['colourScheme'] = '';
		$this->context['extra_js'] = '';
		$this->context['pageViewJS'] = '';

	}
}
