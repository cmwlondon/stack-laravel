<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

/* Models: */
use App\Models\Casestudy;
use App\Models\Blog;

use App\Models\Person AS People;
use App\Models\Client AS Clients;
use App\Models\Metadata;

class MiscController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Metadata $metadata)
	{
		parent::__construct();
		$this->context['meta']['keywords'] = 'PAGE KEYWORDS';
		$this->metadata = $metadata;
	}

	public function index()
	{	
		return view('Main.Misc.index', $this->context);
	}

	public function business(Casestudy $casestudies)
	{	
		// model: work

		$this->context['meta']['title'] = 'STACK business';
		$this->context['meta']['desc'] = 'Our specialist customer offering to the business to business marketplace.';
		$this->context['pageClass'] = 'branch businessBranch';
		$this->context['colourScheme'] = 'colourSchemeBlue';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		// $this->context['casestudies'] = $casestudies->getBusinessCasestudies(ACTIVE_CASESTUDIES);

		// get business case study order metadata
		$businessOrderMetadata = $this->metadata->where('datakey','=','business')->first()->data;

		if ($businessOrderMetadata !== '')
		{
			$businessOrderArray = explode(',',$businessOrderMetadata);

			$this->context['casestudies'] = $casestudies->whereIn('id', $businessOrderArray)
			->where('active','=',1)
			->orderByRaw("field(id,{$businessOrderMetadata})")
			->get();
		}

		return view('Main.Misc.business', $this->context);
	}

	public function consumer(Casestudy $casestudies)
	{	
		// model: work

		$this->context['meta']['title'] = 'STACK consumer';
		$this->context['meta']['desc'] = 'Our specialist customer offering to the business to consumer marketplace.';
		$this->context['pageClass'] = 'branch businessBranch';
		$this->context['colourScheme'] = 'colourSchemeYellow';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		// $this->context['casestudies'] = $casestudies->getConsumerCasestudies(ACTIVE_CASESTUDIES);

		// get consumer case study order metadata
		$consumerOrderMetadata = $this->metadata->where('datakey','=','consumer')->first()->data;

		if ($consumerOrderMetadata !== '')
		{
			$consumerOrderArray = explode(',',$consumerOrderMetadata);

			$this->context['casestudies'] = $casestudies->whereIn('id', $consumerOrderArray)
			->where('active','=',1)
			->orderByRaw("field(id,{$consumerOrderMetadata})")
			->get();
		}

		return view('Main.Misc.consumer', $this->context);
	}

	public function clients(Clients $clients, Metadata $metadata)
	{	
		// model: clients

		$this->context['meta']['title'] = 'STACK clients';
		$this->context['meta']['desc'] = 'The full list of clients we create campaigns and consult for.';
		$this->context['pageClass'] = 'clients';
		$this->context['colourScheme'] = '';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		// $this->context['clients'] = $clients->where('active',1)->get();

		// get client order metadata
		$clientOrderMetadata = $metadata->where('datakey','=','clients')->first();
		$clientOrderList = $clientOrderMetadata->data;

		if ($clientOrderList !== '')
		{
			$clientOrderArray = explode(',',$clientOrderList);

			// get client records, ordered by 'clients' metadata entry
			$this->context['clients'] = $clients->whereIn('id', $clientOrderArray)
			->orderByRaw("field(id,{$clientOrderList})")
			->get();

		} else {
			$this->context['clients'] = $clients->all();
		}

		return view('Main.Misc.clients', $this->context);
	}

	public function people(People $people)
	{	
		// model: people
		$peopleOrderMetadata = $this->metadata->where('datakey','=','people')->first();
		$this->order = $peopleOrderMetadata->data; // CSV string

		$this->context['meta']['title'] = 'STACK Management team';
		$this->context['meta']['desc'] = 'The management who run STACK, including the CEO, ECD, CSO, CD, FD, CSD, Design Director, Client Services Director and Data Strategy Director.';
		$this->context['pageClass'] = 'people';
		$this->context['colourScheme'] = 'colourSchemeGreen';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		if ($this->order !== '')
		{
			$peopleOrderArray = explode(',',$this->order);

			$pg = $people->whereIn('id', $peopleOrderArray)->where([
				['showInPeoplePage','=',1],
				['active','=',1]
			])->orderByRaw("field(id,{$this->order})")->get();
		} else {
			$pg = $people->where([
				['showInPeoplePage','=',1],
				['active','=',1]
			])->get();
		}

		$this->context['people'] = $pg;

		return view('Main.Misc.people', $this->context);
	}

	public function contact()
	{	
		$this->context['meta']['title'] = 'STACK Contact Us';
		$this->context['meta']['desc'] = 'How to contact STACK and how to find us, regarding all new business and other enquiries.';
		$this->context['pageClass'] = 'contact';
		$this->context['colourScheme'] = '';
		$this->context['extra_js'] = '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQGmPCT5YsHjNu_b1rjfsJSWoI24CUttw&libraries=places,geometry&callback=initmap" async defer></script>';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		return view('Main.Misc.contact', $this->context);
	}

	public function privacy()
	{	
		$this->context['meta']['title'] = 'STACK Privacy policy';
		$this->context['meta']['desc'] = 'Our privacy policy';
		$this->context['pageClass'] = 'genericText';
		$this->context['colourScheme'] = '';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		return view('Main.Misc.privacy', $this->context);
	}

	public function sitemap(Casestudy $casestudies, Blog $blog)
	{	
		$this->context['meta']['title'] = 'STACK sitemap';
		$this->context['meta']['desc'] = 'Our sitemap';
		$this->context['pageClass'] = 'genericText  sitemap';
		$this->context['colourScheme'] = '';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		// business casestudies
		$businessOrderMetadata = $this->metadata->where('datakey','=','business')->first()->data;

		if ($businessOrderMetadata !== '')
		{
			$businessOrderArray = explode(',',$businessOrderMetadata);

			$_businessCasestudies = $casestudies->whereIn('id', $businessOrderArray)
			->orderByRaw("field(id,{$businessOrderMetadata})")
			->get();
		}
		$this->context['businessCaseStudies'] = $_businessCasestudies;
		
		// consumer casestudies
		$consumerOrderMetadata = $this->metadata->where('datakey','=','consumer')->first()->data;
		if ($consumerOrderMetadata !== '')
		{
			$consumerOrderArray = explode(',',$consumerOrderMetadata);

			$_consumerCasestudies = $casestudies->whereIn('id', $consumerOrderArray)
			->orderByRaw("field(id,{$consumerOrderMetadata})")
			->get();
		}
		$this->context['consumerCaseStudies'] = $_consumerCasestudies;

		// blog posts
		$this->context['blogposts'] = $blog->where('status','=','published')->orderBy('created_at','desc')->get();

		return view('Main.Misc.sitemap', $this->context);
	}

}
