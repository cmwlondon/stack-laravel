<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Models\Story;

class NewsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function index(Story $stories)
	{	
		// model: news

		$this->context['meta']['title'] = 'Stack News';
		$this->context['meta']['desc'] = 'The latest news: new clients, new campaigns, new people';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['pageClass'] = 'newsindex';
		$this->context['colourScheme'] = 'colourSchemeGreen';

		$this->context['stories'] = $stories->getAllStories();

		return view('Main.News.index', $this->context);
	}

	public function story(string $title, int $id, Story $stories)
	{	
		// model: news
		$this->context['id'] = $id;

		$this->context['stories'] = $stories->getThisStory($id);

		$this->context['meta']['title'] = $this->context['stories']['item']['title'];
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		// $this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['pageClass'] = 'newsitem';
		$this->context['colourScheme'] = 'colourSchemeGreen';

		return view('Main.News.story', $this->context);
	}

}
