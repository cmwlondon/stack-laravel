<?php namespace App\Http\Controllers\Admin\People;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests;

use Request;

// use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Admin;

use App\Models\Person AS People;
use App\Models\Metadata;

use App\Http\Requests\Admin\PersonRequest as PersonRequest;

use Intervention\Image\ImageManagerStatic as Image;

class PeopleController extends AdminController {

	/**
	 * Create a new controller instance with dependency injection
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, People $people, Metadata $metadata) {

		parent::__construct($guard);

		$this->people = $people;
		$this->metadata = $metadata;
	}

	/*
	routes/web.php
	Route::get('people', 'Admin\People\PeopleController@index');
	Route::get('people/create', 'Admin\People\PeopleController@create');
	Route::post('people/store', 'Admin\People\PeopleController@store');
	Route::get('people/edit/{id}', 'Admin\People\PeopleController@edit');
	Route::put('people/update/{id}', 'Admin\People\PeopleController@update');
	Route::delete('people/destroy/{id}', 'Admin\People\PeopleController@destroy');

	Route::post('ajax/people/order', 'Admin\AjaxController@orderPeople');
	*/

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$authorsOnly = [];
		$inactivePeople = [];
		$extraPeople = [];

		$this->context['title'] = 'People Management';

		$all = $this->people->all();

		// get client order metadata
		$peopleOrderMetadata = $this->metadata->where('datakey','=','people')->first();
		$this->order = $peopleOrderMetadata->data; // CSV string

		if ($this->order !== '')
		{
			$peopleOrderArray = explode(',',$this->order);

			// get client records, ordered by 'clients' metadata entry
			$orderedPeople = $this->people
			->whereIn('id', $peopleOrderArray)
			->where([
				['showInPeoplePage','=',1],
				['active','=',1]
			])
			->orderByRaw("field(id,{$this->order})")
			->get();

			// find people with 'showInPeoplePage' === 1 not in order list (handle chnage ot properties of person on 'edit')
			$extraPeople = $this->people
			->whereNotIn('id', $peopleOrderArray)
			->where([
				['showInPeoplePage','=',1],
				['active','=',1]
			])
			->get();

			$n1 = [];
			$o1 = [];
			foreach($orderedPeople AS $p1) {
				$n1[] = $p1;
				$o1[] = $p1->id;
			}

			if ($extraPeople->count() > 0)
			{
				foreach($extraPeople AS $p2) {
					$n1[] = $p2;
					$o1[] = $p2->id;
				}

			}

			// store updated order
			$this->metadata->updateOrCreate(
				['datakey' => 'people'],
				['data' => implode(',',$o1)]
			);

			// $inactivePeople = $this->people
			// ->whereNotIn('id', $peopleOrderArray)
			// ->get();
		} else {
			$allPeople = $this->people
			->where([
				['showInPeoplePage','=',1],
				['active','=',1]
			])
			->get();

			foreach($allPeople AS $p1) {
				$n1[] = $p1;
				$o1[] = $p1->id;
			}
		}

		$this->context['all'] = $all;

		// $this->context['authors'] = $authorsOnly;
		// $this->context['inactivePeople'] = $inactivePeople;

		// $this->context['people'] = $orderedPeople;
		// $this->context['peopleOrder'] = $this->order;

		$this->context['people'] = $n1;
		$this->context['peopleOrder'] = implode(',',$o1);

		$this->context['test'] = [
			// 'ordered' => $orderedPeople,
			// 'extra' => $extraPeople,
			'o1' => $o1,
			'merged' => $n1
		];
		$this->context['pageViewJS'] = 'admin/people-index.min';
		return view('admin.people.main', $this->context);
	}

	// new person blank form
	public function create()
	{
		/*
		$this->context['person'] = [
			'active' => 1,
			'showInPeoplePage' => 1,
			'author' => 0
		];
		*/
		$this->context['pageViewJS'] = 'admin/people-create-edit.min';
		return view('admin.people.create', $this->context);
	}

	// new person process validated submission
	public function store(PersonRequest $request)
	{
		// $portrait = $this->processPortrait($request);

		$data = [
			'name' => $request->name,
			'slug' => $request->slug,
			'role' => $request->role,
			'bio' => $request->bio,
			'portrait' => $request->portrait,
			'twitter' => $request->twitter,
			'linkedin' => $request->linkedin,
			'telephone' => $request->telephone,
			'email' => $request->email,
			'colour' => $request->colour,
			'active' => $request->active,
			'showInPeoplePage' => $request->showInPeoplePage,
			'author' => $request->author
		];

	  	$this->people->fill($data);
		$this->people->save();

		$this->response = array('response_status' => 'success', 'message' => 'This person has been added successfully.');

		return redirect('admin/people')->with('response', $this->response);
	}

	public function edit(int $id)
	{
		$this->context['person'] = $this->people->where('id', '=', $id)->first();
		$this->context['pageViewJS'] = 'admin/people-create-edit.min';
		return view('admin.people.edit', $this->context);
	}

	public function update(int $id, PersonRequest $request)
	{
		// check to see if portrait has changed, if so process new image otherwise skip the portrait field

		$subject = $this->people->where('id', '=', $id)->first();

		$data = [
			'name' => $request->name,
			'slug' => $request->slug,
			'role' => $request->role,
			'bio' => $request->bio,
			'portrait' => $request->portrait,
			'twitter' => $request->twitter,
			'linkedin' => $request->linkedin,
			'telephone' => $request->telephone,
			'email' => $request->email,
			'colour' => $request->colour,
			'active' => $request->active,
			'showInPeoplePage' => $request->showInPeoplePage,
			'author' => $request->author
		];

	  	$subject->update($data);

		$this->response = array('response_status' => 'success', 'message' => 'This person has been updated successfully.');
		return redirect('admin/people')->with('response', $this->response);
	}

	public function destroy(int $id)
	{
		$deadPerson = $this->people->findOrFail($id);

		// unlink logo image
		// $deadClient->logo

		$deadPerson->delete();

		$this->response = array('response_status' => 'success', 'message' => 'This person has been deleted successfully.');

		return redirect('admin/people')->with('response', $this->response);
	}

	private function processPortrait($request) {
		$filename = $request->slug . '-' . time() . '.' . $request->file('portrait')->getClientOriginalExtension();

		// /public/uploads/
		$request->file('portrait')->move(
	        public_path().'/uploads/people/', $filename
	    );

		// open file a image resource
		$img = Image::make('uploads/people/'.$filename);

		$img->fit(513, 513, function ($constraint) {
				//$constraint->aspectRatio();
			    $constraint->upsize();
			})->save();
		return $filename;
	}

}
