<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

use Session;
use Admin;
use Carbon\Carbon;

class AdminController extends Controller {
	
	protected $filters;
	protected $response = [];
	
	public function __construct() {
		$this->middleware('auth');
		
		// Admin wont work here as Auth isn't set up yet
		// Admin::register();

		$this->context = array(
			'meta'				=> [
				'author' => 'STACK'
				,'title' => ''
				,'desc' => ''
				,'keywords' => ''
			]
			,'pageViewCSS' 		=> ''
			,'pageViewJS'		=> ''
			,'now'				=> Carbon::now()
		);

		if (Session('response') !== NULL) {
			$this->context['response'] = Session('response');
		}


		$this->filters = [];
	}

	public function getFilters($key, $default) {
		//Session::flush();
		return Session::get('filters.'.$key, $default);
	}

	public function setFilters($key,$value) {
		Session::put('filters.'.$key,$value);
	}

	public function prepDefaultFilters($key, $options) {
		$filters = $this->getFilters($key, []);

		foreach ($options as $k => $v) {
			$filters[$k] = (isset($filters[$k]) ? $filters[$k] : $v);
		}

		return $filters;
	}

	public function delTree($dir) { 
	   $files = array_diff(scandir($dir), array('.','..')); 
	    foreach ($files as $file) { 
	      (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file"); 
	    } 
	    return rmdir($dir); 
	}

	public function boolCheckbox($inp) {
		return ($inp == "1") ? 1 : 0;
	}
}