<?php namespace App\Http\Controllers\Admin\CaseStudy;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests;

use Request;

//use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Admin;

use App\Models\Casestudy AS CaseStudies;
use App\Models\Module AS Module;
use App\Models\Module_Types;
use App\Models\Casestudies_Modules;

use App\Models\Metadata;

use App\Http\Requests\Admin\CasestudyRequest as CaseStudyRequest;

// use Intervention\Image\ImageManagerStatic as Image;
// use App\Services\SitemapPackager;

class CaseStudyController extends AdminController {

	/**
	 * Create a new controller instance with dependency injection
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, CaseStudies $casestudies, Module $module, Module_Types $moduleTypes, Metadata $metadata) {

		parent::__construct($guard);

		$this->casestudies = $casestudies;

		$this->metadata = $metadata;
		$this->module = $module;
		$this->moduleTypes = $moduleTypes;

		$this->modulesFields = config('modules.fields'); // config/modules.php

		// thumb colour options
		$thumbColourOptions = config('casestudy.enumerateFields.thumb_colour');
		$thumbColourList = [
			[
				'value' => '',
				'caption' => 'Select a colour',
				'colour' => '#fff'
			]
		];
		$this->thumbColourList = array_merge( $thumbColourList, $thumbColourOptions );
		
		// page colur options
		$pageColourOptions = config('casestudy.enumerateFields.page_colour');
		$pageColourList = [
			[
				'value' => '',
				'caption' => 'Select a colour',
				'colour' => '#fff'
			]
		];
		$this->pageColourList = array_merge( $pageColourList, $pageColourOptions );

		// case study catgeory: 'consumer' / 'business'
		$branchOptions = config('casestudy.enumerateFields.branch');
		$branchList = [
			[
				'value' => '',
				'caption' => 'Select a category'
			]
		];
		$this->branchList = array_merge( $branchList, $branchOptions );

		$this->context['canonical'] = url()->current();
		$this->context['gaSiteID'] = env('GA_SITE_ID');
		$this->context['isHomePage'] = false;
		$this->context['pageClass'] = '';
		$this->context['colourScheme'] = '';
		$this->context['extra_js'] = '';
		$this->context['pageViewJS'] = '';

		$this->context['thumbColourList'] = $this->thumbColourList;
		$this->context['pageColourList'] = $this->pageColourList;
		$this->context['branchList'] = $this->branchList;

		$this->context['moduleList'] = array_pluck($moduleTypes->all(), 'title', 'ref');

		/*
		casestudy order fields:
			business
			consumer
		*/

		// /uploads/assets/images/casestudies/[casestudy->slug]/[image]
		$this->imageRoot = '/uploads/assets/images/casestudies/';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['title'] = 'Case Study Management';

		// all (active) consumer casestudies - listed in 'consumer' metadata entry
		$_consumer = $this->getOrderedCasestudies('consumer');
		$this->context['consumerOrder'] = $_consumer['order'];
		$this->context['consumerCasestudies'] = $_consumer['casestudies'];

		// all (active) business casestudies - listed in 'business' metadata entry
		$_business = $this->getOrderedCasestudies('business');
		$this->context['businessCasestudies'] = $_business['casestudies'];
		$this->context['businessOrder'] = $_business['order'];

		// All case studies
		$AllCaseStudyCollection = $this->casestudies->getCombinedCasestudies(COMBINED_CASESTUDIES);

		// combined: all active and inactice casestudies
		$this->context['casestudies'] = $AllCaseStudyCollection;

		$this->context['test'] = [
		];

		$this->context['pageViewJS'] = 'admin/casestudy-index.min';
		return view('admin.casestudies.main', $this->context);
	}

	public function create()
	{
		$this->context['id'] = 0; // new casestudy, not assigned an id yet
		$this->context['nextId'] = $this->module->max('id') + 1;
		$this->context['test'] = ['id' => 0];
		$this->context['fieldIndex'] = $this->modulesFields; // pass fieldnames through to view / js
		$this->context['pageViewJS'] = 'admin/casestudy-create-edit.min';
		return view('admin.casestudies.create', $this->context);
	}

	public function fe_create()
	{
		$this->context['id'] = 0; // new casestudy, not assigned an id yet
		$this->context['nextId'] = $this->module->max('id') + 1;
		$this->context['test'] = ['id' => 0];
		$this->context['fieldIndex'] = $this->modulesFields; // pass fieldnames through to view / js
		$this->context['pageViewJS'] = 'admin/casestudy-fe-create-edit.min';
		return view('admin.casestudies.fe_create', $this->context);
	}

	// need to handle storing new module instances and related media files (image/video/svg)
	public function store(CaseStudyRequest $request)
	{
	  	// iterate through selected modules
	  	// get module typefields
	  	// switch( moduleType)
	  	$fieldMap = config('modules.map');
		$modulesData = json_decode($request->modulesFieldList);
		$casestudyModules = [];
		echo "<p>".$request->id."</p>";
		echo "<pre>".print_r($modulesData, true)."</pre>";

		// at this point *all* the modules are new
		foreach( $modulesData AS $moduleData)
		{
			// get data from form based on module type
			if ( $moduleData->type !== 'brochure-video' ) {
				$meta = [];
				$_fields = $fieldMap[$moduleData->type];
				foreach( $_fields AS $_field )
				{
					$_fieldName = $_field['form'].$moduleData->suffix;
					echo "<p>form: '$_fieldName' =&gt; data: '".$_field['data']."'</p>\n\n";
					$meta[ $_field['data'] ] = $request->$_fieldName;
				}
			} else {
				// populate meta data for module type 'brochure-video' type id: 7
				$meta = unserialize('a:6:{s:5:"class";s:0:"";s:3:"vid";s:0:"";s:3:"mp4";s:31:"/video/casestudies/infinity.mp4";s:4:"webm";s:0:"";s:3:"ogv";s:0:"";s:6:"poster";s:43:"/img/casestudy/citroen-c3-aircross/pic2.jpg";}');
			}

			$mdata = [
				'type' => $this->moduleTypes->getByReference($moduleData->type)->id,
				'meta'=> serialize($meta),
			];

			$moduleItem = new Module;
			$moduleItem->fill($mdata);
		  	$moduleItem->save();
			$newModuleID = $moduleItem->id;
			$casestudyModules[] = $newModuleID;
		}

		$this->casestudies->fill([
			'title'				=> $request->title,
			'slug'				=> $request->slug,
			'branch'			=> $request->branch,
			'thumb_main'		=> $request->thumb_main,
			'thumb_sub'			=> $request->thumb_sub,
			'thumb_image'		=> $request->thumb_image,
			'thumb_colour'		=> $request->thumb_colour,
			'meta_title'		=> $request->meta_title,
			'meta_desc'			=> $request->meta_desc,
			'meta_keywords'		=> $request->meta_keywords,
			'page_colour'		=> $request->page_colour,
			'modules' 			=> implode(',', $casestudyModules),
			'active' 			=> $request->active
		]);
	  	$this->casestudies->save();

	  	// get new casestudy id
	  	$newId = $this->casestudies->id;

		$metadataNew = $this->metadata->where('datakey','=',$request->branch)->first();
		$newOrderList = $metadataNew->data;
		$newOrderArray = explode(',', $newOrderList );

		// prepend to new order category
		array_unshift($newOrderArray, $newId);

		$metadataNew->data = implode(',', $newOrderArray);
		$metadataNew->save();

	  	// modules: inline-image,image-grid,reveal-image,video-manual
	  	// move image/video files to relevant media subdirectory

		$this->response = array('response_status' => 'success', 'message' => 'This case study has been created successfully.');
		return redirect('admin/casestudies')->with('response', $this->response);
	}

	public function edit(int $id)
	{
		$casestudy = $this->casestudies->find($id);

		if ( $casestudy === NULL)
		{
			// casestudy not found!
			return redirect('admin/casestudies');
		}

		// get casestudy properties and process to popukate edit form

		// get module data
		$moduleList = explode(',',$casestudy->modules);

		if (old('resub') !== null) {
			if (old('modules') !== $casestudy->modules) {
				$moduleList = explode( ',', old('modules') );
			}
		}

		// get old('modulesFieldList')
		$formState = (old('modulesFieldList') !== NULL) ? json_decode(old('modulesFieldList')) : NULL;
		$moduleTypes = [];
		$moduleFieldList = [];
		$modulesData = [];
		$changes = [];
		$testMeta = [];

		// build list of module fields
		if ( count($moduleList) > 0 )
		{
			foreach( $moduleList AS $moduleIndex => $moduleID )
			{
				$_module = $this->module->find($moduleID);

				if( gettype($_module) !== 'NULL' )
				{
					$_moduleType = $this->moduleTypes->getByID( $_module->type );
					$_suffix = '_'.$id.'_'.$moduleID;

					$thisModuleType = $_moduleType->ref;
					$thisModuleMeta = unserialize($_module->meta);
					$testMeta[] = [
						'db-type' => $_moduleType->ref,
						'db-meta' => $thisModuleMeta
					];
					// do form values exist (redisplay after validation fail)
					if ( $formState !==  NULL)
					{
						$formType = $formState[$moduleIndex]->type;
						// has one of the modules changes?
						if ( $formType !== $_moduleType->ref)
						{
							// yes, use form type instead of db type
							$meta = [];

							// provide appropriate meta based on new module type
							switch( $formType )
							{
								case 'copy' :
								{
									$fields = ['html'];
									foreach( $fields AS $field)
									{
										$meta[$field] = NULL;
									}
								}
								break;
								case 'video-manual' :
								{
									$fields = ['class','vid','poster','mp4','webm','ogv'];
									foreach( $fields AS $field)
									{
										$meta[$field] = NULL;
									}
				                }
								case 'inline-image' :
								{
									$fields = ['image','alt'];
									foreach( $fields AS $field)
									{
										$meta[$field] = NULL;
									}
								}
								break;
								case 'reveal-image' :
								{
									$fields = ['image','alt'];
									foreach( $fields AS $field)
									{
										$meta[$field] = NULL;
									}
								}
								break;
								case 'image-grid' :
								{
									$fields = ['images','alt'];
									$meta['images'] = [ NULL, NULL, NULL, NULL ];
									$meta['alt'] = NULL;
								}
								break;
								case 'brochure-video' :
								{
									$fields = ['class','vid','poster','mp4','webm','ogv'];
									$meta = unserialize('a:6:{s:5:"class";s:0:"";s:3:"vid";s:0:"";s:3:"mp4";s:31:"/video/casestudies/infinity.mp4";s:4:"webm";s:0:"";s:3:"ogv";s:0:"";s:6:"poster";s:43:"/img/casestudy/citroen-c3-aircross/pic2.jpg";}');
								}
								break;
								case 'evaluate2video' :
								{
									$fields = ['poster','mp4','webm','ogv'];
									$meta = unserialize('a:4:{s:3:"mp4";s:33:"/video/casestudies/evaluate_2.mp4";s:4:"webm";s:0:"";s:3:"ogv";s:0:"";s:6:"poster";s:33:"/img/casestudy/evaluate2/pic2.jpg";}');
								}
								break;
								case 'ev2header' :
								{
								}
							}

							$changes[] = [
								'index' => $moduleIndex,
								'form' => $formType,
								'db' => $_moduleType->ref,
								'meta' => $meta
							];

							// replace module type in db with module specified in form (displayed after invalid submission)
							$thisModuleType = $formType;
							$thisModuleMeta = $meta;
						}
					}

					$thisModuleFields = [];
					$_fields = $this->modulesFields[$thisModuleType];
					foreach( $_fields AS $_field )
					{
						$thisModuleFields[] = $_field.$_suffix;
					}	

					$moduleTypes[] = $thisModuleType;
						
					$moduleFieldList[] = [
						"type" => $thisModuleType,
						"id" => $moduleID,
						'suffix' => $_suffix
					];

					$modulesData[] = [
						'suffix' => $_suffix,
						'module-id' => $moduleID,
						// 'type' => $_moduleType->ref, // ignores form data, chaages to module type will not be reflected if the form re-renders after invalid submission
						'type' => $thisModuleType,
						'meta' => $thisModuleMeta
					];
				}
			}
		}

		$test = [
			/*
			'id' => $id,
			'oldstates' => $formState,
			'modules' => $moduleList,
			'types' => $moduleTypes,
			'changes' => $changes,
			'testmeta' => $testMeta,
			'data' => $modulesData
			*/
		];
		$this->context['test'] = $test;

		$this->context['modulesData'] = $modulesData;
		$this->context['id'] = $id;
		$this->context['nextId'] = $this->module->max('id') + 1;
		$this->context['casestudy'] = $casestudy;
		$this->context['thumbColourList'] = $this->thumbColourList;
		$this->context['pageColourList'] = $this->pageColourList;
		$this->context['fieldIndex'] = $this->modulesFields;
		$this->context['modulesFieldList'] = json_encode($moduleFieldList);

		// echo "<pre>".print_r($test, true)."</pre>";
		// die();

		$this->context['pageViewJS'] = 'admin/casestudy-create-edit.min';
		return view('admin.casestudies.edit', $this->context);
	}

	/*
	front end editor version of case study editod
	add/edit/delete/reorder modules via js/AJAX
	*/
	/*
	figure out
	ckedit / in page text editor
	video / image edit
	*/
	public function fe_edit(int $id, Module $module, Module_Types $moduleTypes)
	{
		$casestudy = $this->casestudies->find($id);

		if ( $casestudy === NULL)
		{
			// casestudy not found!
			return redirect('admin/casestudies');
		}

		// get module data
		$moduleList = explode(',',$casestudy->modules);
		$moduleFieldList = [];

		if ( count($moduleList) > 1 )
		{
			foreach( $moduleList AS $moduleID )
			{
				$_module = $module->where('id', '=', $moduleID)->first();
				
				$_moduleType = $moduleTypes->getByID( $_module->type );

				$this->context['modules'][] = [
					'module-id' => $moduleID,
					'type-id' => $_module->type,
					'type' => $_moduleType->ref,
					'meta' => unserialize($_module->meta)
				];
			}
		}

		$this->context['meta']['title'] = $casestudy['meta_title'];
		$this->context['meta']['desc'] = $casestudy['meta_desc'];
		$this->context['meta']['keywords'] = $casestudy['meta_keywords'];
		$this->context['pageClass'] = 'casestudy';
		$this->context['colourScheme'] = $casestudy['page_colour'];
		$this->context['category'] = $casestudy['branch'];
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['id'] = $id;
		$this->context['nextId'] = $this->module->max('id') + 1;
		$this->context['casestudy'] = $casestudy;
		$this->context['thumbColourList'] = $this->thumbColourList;
		$this->context['pageColourList'] = $this->pageColourList;
		$this->context['fieldIndex'] = $this->modulesFields;
		$this->context['modulesFieldList'] = json_encode($moduleFieldList);
		
		$this->context['pageViewJS'] = 'admin/casestudy-fe-create-edit.min';
		return view('admin.casestudies.fe_edit', $this->context);
	}

	public function fe_update_module(int $id, Module $module, Module_Types $moduleTypes)
	{
	}

	public function fe_add_module(int $id, Module $module, Module_Types $moduleTypes)
	{
	}

	public function fe_delete_module(int $id, Module $module, Module_Types $moduleTypes)
	{
	}

	public function fe_reorder_modules(int $id, Module $module, Module_Types $moduleTypes)
	{
	}

	// need to handle storing new module instances and related media files (image/video/svg)
	/*
	bug here related to metatdata.[category]
	deactivating a casestudy apperas to be removing incorrect items from metadata.[category]

	correct behaviour
		+ add new caststudy: add new casestudy to category as first 0th item
		- deactivate/delete: remove casestudy id from metadata.[category], (delete casetudy (+ related modules))
		/ move case study between categories: remove casestudy id from metadata.[category.original], add casestudy id to metadata.[category.new] as first 0th item
	*/
	public function update(CaseStudyRequest $request)
	{
		$id = $request->id;
		$item = $this->casestudies->find($id);

		// echo "<pre>".print_r($item,true)."</pre>";

		$data = [
			'title'				=> $request->title, // string
			'slug'				=> $request->slug, // string
			'branch'			=> $request->branch, // string 
			'thumb_main'		=> $request->thumb_main, // string
			'thumb_sub'			=> $request->thumb_sub, // string
			'thumb_image'		=> $request->thumb_image, // string
			'thumb_colour'		=> $request->thumb_colour, // string
			'meta_title'		=> $request->meta_title, // string
			'meta_desc'			=> $request->meta_desc, // string
			'meta_keywords'		=> $request->meta_keywords, // string
			'page_colour'		=> $request->page_colour, // string
			'modules' 			=> $request->modules, // string
			'active' 			=> $request->active // string
		];

		echo "<pre>".print_r($data,true)."</pre>";

		// ----------------------------------------------------
		// handle metadata.[categpry] updates 
		// ----------------------------------------------------

		$originalBranch = $item->branch;

		if ( $data['active'] === '1' ) {
			// case study is still active

			// properties may have changed
			// potential for change of category

			// get category in form
			$newBranch = $request->branch;

			// compare with original category
			if ( $originalBranch !== $newBranch ) {
				// changed category

				// $originalOrder = $this->getOrderArray($originalBranch);
				// $newOrder = $this->getOrderArray($newBranch);						

				// 34,40,1,39,4,31,5,6,7
				// 33,38,32,37,9,41,13,12,10,29,17,36,11,14,16,18,19,20,21,22,23,24,25,26,27,28

				// remove from current category
				$updatedOrder = $this->removeFromOrderList($originalBranch, $id);
				// echo "<p>$originalBranch updatedOrder: [$updatedOrder]</p>";

				// add to start of list for new catgeory
				$updatedOrder = $this->addToOrderList($newBranch, $id);
				// echo "<p>$newBranch updatedOrder: [$updatedOrder]</p>";
			}
		} else {
			// case study deactivated

			// remove from metadata.[category]
			$updatedOrder = $this->removeFromOrderList($originalBranch, $id);
			// echo "<p>$originalBranch updatedOrder: [$updatedOrder]</p>";
		}

		// ----------------------------------------------------
		// handle module updates 
		// ----------------------------------------------------

		// --------------------------
		// handle updated/new modules
		// --------------------------
		
		$moduleFieldList = json_decode($request->modulesFieldList);

		$casestudyModules = [];
		foreach($moduleFieldList AS $moduleField)
		{
			// options
			// a) existing modules will be updated to reflect new type & content
			// better use of db space, no undo for module changes
			// b) insert new modules every time the case study is update
			// uses more space but stores the state of modules in the db

			// go with option a for the moment
			// loop through modules in case study
			// if the id exists in the database, retrieve the module and update it
			// if the id does not exist, insert a new module

			$moduleItem = $this->module->find($moduleField->id);

			$action = ( $moduleItem === NULL ) ? 'insert' : 'update';
			$meta = [];

			switch( $moduleField->type ) {
				case 'copy' :
				{
					// iimage => image
					$thisField = 'html'.$moduleField->suffix;
					$meta['html'] = $request->$thisField;
				}
				break;
				case 'video-manual' :
				{
					$fields = ['class','vid','poster','mp4','webm','ogv'];
					foreach( $fields AS $field )
					{
						$thisField = $field.$moduleField->suffix;
						$value = $request->$thisField;
						$meta[$field] = $value;
					}
                }
				case 'autoplayvideo' :
				{
					$fields = ['class','vid','poster','mp4','webm','ogv'];
					foreach( $fields AS $field )
					{
						$thisField = $field.$moduleField->suffix;
						$value = $request->$thisField;
						$meta[$field] = $value;
					}
                }
				case 'inline-image' :
				{
					// iimage => image
					$thisField = 'iimage'.$moduleField->suffix;
					$meta['image'] = $request->$thisField;

					// ialt => alt
					$thisField = 'ialt'.$moduleField->suffix;
					$meta['alt'] = $request->$thisField;
				}
				break;
				case 'reveal-image' :
				{
					// rimage => image
					$thisField = 'rimage'.$moduleField->suffix;
					$meta['image'] = $request->$thisField;

					// ralt => alt
					$thisField = 'ralt'.$moduleField->suffix;
					$meta['alt'] = $request->$thisField;
				}
				break;
				break;
				case 'image-grid' :
				{
					// gimg1,gimg2,gimg3,gimg4 => images[0,1,2,3]
					// galt => alt
					$meta['images'] = [];
					foreach( ['gimg1','gimg2','gimg3','gimg4'] AS $mark)
					{
						$thisField = $mark.$moduleField->suffix;
						$meta['images'][] = $request->$thisField;
					}
					$thisField = 'galt'.$moduleField->suffix;
					$meta['alt'] = $request->$thisField;
				}
				break;
				case 'brochure-video' :
				{
					// class, vid, mp4, webm, ogv, poster
					$meta = unserialize('a:6:{s:5:"class";s:0:"";s:3:"vid";s:0:"";s:3:"mp4";s:31:"/video/casestudies/infinity.mp4";s:4:"webm";s:0:"";s:3:"ogv";s:0:"";s:6:"poster";s:43:"/img/casestudy/citroen-c3-aircross/pic2.jpg";}');
				}
				break;
				case 'evaluate2video' :
				{
					// mp4, webm, ogv, poster
					$meta = unserialize('a:4:{s:3:"mp4";s:33:"/video/casestudies/evaluate_2.mp4";s:4:"webm";s:0:"";s:3:"ogv";s:0:"";s:6:"poster";s:33:"/img/casestudy/evaluate2/pic2.jpg";}');
				}
				break;
				case 'ev2header' :
				{
					$meta = '';
				}
				break;
			}

			// map modules.type <=> module_types.id on modules.type = module_types.ref
			$mdata = [
				'type' => $this->moduleTypes->getByReference($moduleField->type)->id,
				'meta'=> serialize($meta),
			];

			if ( $action === 'insert' )
			{
				// new module
				// insert
				$moduleItem = new Module;
				$moduleItem->fill($mdata);
			  	$moduleItem->save();

				// get new insert id 
			  	$newModuleID = $moduleItem->id;
				// $newModuleID = $moduleField->id; // fake id for testing

				$casestudyModules[] = $newModuleID;
			}
			else
			{	
				// existing module
				// update
				$moduleItem->update($mdata);
				$casestudyModules[] = $moduleField->id;
			}
		}

		// --------------------------------------

		$data['modules'] = implode(',',$casestudyModules);

		$item->update($data);

		// --------------------------------------

		$this->response = array('response_status' => 'success', 'message' => 'This case study has been updated successfully.');
		return redirect('admin/casestudies')->with('response', $this->response);
	}

	public function destroy(int $id)
	{
		// delete case study
		$casestudy = $this->casestudies->find($id);

		// does the casestudy exist
		if ( $casestudy === NULL)
		{
			// no, do nothing
			return redirect('admin/casestudies');
		}

		// $casestudy->delete();

		// remove from metadata 'consumer'/'business' order field
		// optional: delete related modules/images/video

		$this->response = array('response_status' => 'success', 'message' => 'This case study has been deleted successfully.');

		return redirect('admin/casestudies')->with('response', $this->response);

	}

	private function getOrderedCasestudies(String $category)
	{
		// $category 'business','consumer'
		$OrderMetadata = $this->metadata->where('datakey','=',$category)->first()->data;

		if ($OrderMetadata !== '')
		{
			// get casestudies based on metadata.[category]
			$OrderArray = explode(',',$OrderMetadata);

			$_casestudies = $this->casestudies->whereIn('id', $OrderArray)
			->where('active', '=', 1)
			->orderByRaw("field(id,{$OrderMetadata})")
			->get();
		}
		else
		{
			// get active casestudies of [category] from casestudies table 
			$_casestudies = $this->casestudies->where([['active', '=', 1],['branch','=', $category]])
			->get();
		}

		return [
			'order' => $OrderMetadata,
			'casestudies' =>$_casestudies
		];
	}

	private function getFilenameAndExtension(string $file)
	{
		// (?:.+\/)*([a-z0-9\-\_\+]+)\.([a-zA-Z]+)$ get filename and file extension
		$matcherFlag = preg_match("/(?:.+\/)*([a-zA-Z0-9\-\_\+]+)\.([a-zA-Z0-9]+)$/", $file, $matches);

		// php bug
		// $extension = $matches[2]; does not work error: undefined offset 2
		$extension = $matches[2] ?? ''; // works
		$filename = $matches[1] ?? '';

		return [
			'filename' => $filename,
			'extension' => $extension
		];
	}

	private function getOrderArray(string $category)
	{
		$listMetadata = $this->metadata->where('datakey','=',$category)->first();
		$order = explode(',', $listMetadata->data );

		return [
			'metadata' => $listMetadata,
			'array' => $order
		];
	}

	private function removeFromOrderList(string $category, int $id)
	{
		$orderMetadata = $this->getOrderArray($category);
		$order = $orderMetadata['array'];		

		$index = array_search($id, $order);

		if( gettype($index) === 'integer') {
			array_splice($order, $index, 1);
			$orderMetadata['metadata']->data = implode(',', $order);
			$orderMetadata['metadata']->save();
		}
		return implode(',', $order);
	}

	private function addToOrderList(string $category, int $id, int $position = 0)
	{
		$orderMetadata = $this->getOrderArray($category);
		$order = $orderMetadata['array'];		

		// $position 0:first, 1:last
		switch( $position) {
			case 0 : {
				array_unshift( $order, $id ); // prepend new id
			} break;
			case 1 : {
				array_push( $order, $id ); // append new id
			} break;
		}	

		$orderMetadata['metadata']->data = implode(',', $order);
		$orderMetadata['metadata']->save();
		return implode(',', $order);
	}

	private function fileCopy(array $images)
	{
	}

	private function normalizeImagepaths ( $AllCaseStudyCollection )
	{
	}
}
