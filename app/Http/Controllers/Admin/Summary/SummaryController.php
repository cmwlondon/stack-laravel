<?php

namespace App\Http\Controllers\Admin\Summary;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;

use App\Models\Blog;
use App\Models\Person AS People;
use App\Models\Client AS Clients;
use App\Models\Casestudy;


class SummaryController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Blog $blog, People $people, Clients $clients, Casestudy $casestudy)
    {   
        $this->middleware('auth');

        $this->blog = $blog;
        $this->people = $people;
        $this->clients = $clients;
        $this->casestudy = $casestudy;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->context['title'] = 'System Summary';
        $this->context['pageViewJS'] = '';

        $allBlog = $this->blog->orderby('created_at','desc')->get();

        $uploadsPath = '/uploads/assets/blog/';
        $blogsroot = public_path().$uploadsPath;

        $blogItems = [];
        foreach ( $allBlog AS $blog)
        {
            // create unique directory for each blog
            $thisBlogRoot = $blogsroot . $blog->slug.'-'.$blog->id.'/';

            /*
            // find images in copy of each blog -> 'body'
            // remap image path to new directory structure
            // /uploads/assets/images/blog/square_02.jpg
            // regex: \/uploads\/assets\/images\/blog\/([a-z0-9_-]+)\.(jpg|png)
            // /uploads/assets/blog/[slug]-[id]/ = $thisBlogRoot
            $body = $blog->body;
            preg_match_all("/\/uploads\/assets\/images\/blog\/(([a-z0-9_-]+)\.(jpg|png|mp4))/",$body,$matches);

            $newbody = $blog->body;
            $x = [];
            foreach ( $matches[1] AS $ref )
            {
                $v1 = '/uploads/assets/images/blog/'.$ref;
                $v2 = '/uploads/assets/images/blog/' . $blog->slug.'-'.$blog->id.'/'.$ref;
                $newbody = str_replace($v1, $v2, $newbody);
                $x[] = [$v1, $v2];

                // if( !file_exists(public_path().$v2) )
                    // rename( public_path().$v1, public_path().$v2);
            }
            $blog->body = $newbody;
            $blog->save();
            */

            /*
            // remap /uploads/assets/blog/ -> /uploads/assets/images/blog/
            $ip1 = $blog->image;
            $ip2 = str_replace('/uploads/assets/blog/','/uploads/assets/images/blog/',$ip1);

            $tp1 = $blog->thumb;
            $tp2 = str_replace('/uploads/assets/blog/'.$blog->slug.'-'.$blog->id.'/thumbs/','/uploads/assets/images/blog/'.$blog->slug.'-'.$blog->id.'/thumbs/',$tp1);

            $blog->image = $ip2;
            $blog->thumb = $tp2;
            $blog->save();
            */


            $blogItems[] = [
                'id' => $blog->id,
                'title' => $blog->title,
                'slug' => $blog->slug,
                'path' => $thisBlogRoot,
                'image' => $blog->image,
                'thumb' => $blog->thumb,
                'status' => $blog->status,
                'category' => $blog->getCategory(),
                'created' => $blog->created_at->format('d-m-Y') 
            ];
        }

        $this->context['blogcount'] = count($blogItems);
        $this->context['blog'] = $blogItems;

        $allPeople = $this->people->all();
        $this->context['peoplecount'] = count($allPeople);

        $allClients = $this->clients->all();
        $this->context['clientcount'] = count($allClients);

        $allCasestudies = $this->casestudy->all();
        $this->context['casestudycount'] = count($allCasestudies);

        return view('admin.summary.home', $this->context);
    }
}
