<?php namespace App\Http\Controllers\Admin\Clients;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests;

use Request;

//use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Admin;

use App\Models\Client AS Clients;
use App\Models\Metadata;

use App\Http\Requests\Admin\ClientRequest as ClientRequest;

class ClientsController extends AdminController {

	/**
	 * Create a new controller instance with dependency injection
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, Clients $clients, Metadata $metadata) {

		parent::__construct($guard);

		$this->clients = $clients;
		$this->metadata = $metadata;

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['title'] = 'Client Management';

		// get client order metadata
		$clientOrderMetadata = $this->metadata->where('datakey','=','clients')->first();
		$this->order = $clientOrderMetadata->data; // CSV string

		if ($this->order !== '')
		{
			$clientOrderArray = explode(',',$this->order);

			// get client records, ordered by 'clients' metadata entry
			$orderedClients = $this->clients::whereIn('id', $clientOrderArray)
			->orderByRaw("field(id,{$this->order})")
			->get();

			$inactiveClients = $this->clients::whereNotIn('id', $clientOrderArray)
			->get();
		} else {
			$orderedClients = $this->clients->all();
			$inactiveClients = [];
		}


		$this->context['clients'] = $orderedClients;
		$this->context['inactiveClients'] = $inactiveClients;
		$this->context['clientOrder'] = $this->order;

		$this->context['test'] = [
			'metadata'=> [
				'list' => $this->order
			],
			'clients' => array_pluck($orderedClients,'title')
		];

		$this->context['pageViewJS'] = 'admin/clients-index.min';

		return view('admin.clients.main', $this->context);
	}

	// display blank client logo form
	public function create()
	{
		$this->context['id'] = '0'; // new client logo, not assigned an id yet

		$this->context['pageViewJS'] = 'admin/clients-create-edit.min';

		return view('admin.clients.create', $this->context);
	}

	// store new client logo
	public function store(ClientRequest $request)
	{
		// $tp2Fragments = explode('/',$request->logo);
		// $tp2Filename = $tp2Fragments[count($tp2Fragments) - 1];

	  	$this->clients->fill([
			'active'			=> 1,
			'title'				=> $request->title,
			'text'				=> $request->text,
			'logo'      		=> $request->logo,
			'created_at'		=> $request->created_at
		]);
	  	$this->clients->save();
	  	$insertedId = $this->clients->id;

	  	// add (prepend/append) new ID to end of order list
		$clientOrderMetadata = $this->metadata->where('datakey','=','clients')->first();
		$this->order = $clientOrderMetadata->data; // CSV string
	  	$clientOrderArray = explode(',',$this->order);
	  	$clientOrderArray[] = $insertedId;

		$this->metadata->updateOrCreate(
			['datakey' => 'clients'],
			['data' => implode(',',$clientOrderArray)]
		);

		$this->response = array('response_status' => 'success', 'message' => 'This client has been created successfully.');

		return redirect('admin/clients')->with('response', $this->response);
	}

	// edit existing client logo
	public function edit($id)
	{
		$this->context['id'] = $id;
		$this->context['client'] = $this->clients->find($id);

		$this->context['pageViewJS'] = 'admin/clients-create-edit.min';
		return view('admin.clients.edit', $this->context);
	}

	// update client logo
	public function update(int $id, ClientRequest $request)
	{

		$item = $this->clients->find($id);
		
		$data = [
			'title'				=> $request->title,
			'text'				=> $request->text,
			'logo'      		=> $request->logo
		];

		$item->update($data);

		$this->response = array('response_status' => 'success', 'message' => 'This client has been updated successfully.');

		return redirect('admin/clients')->with('response', $this->response);
	}

	// delete client logo
	public function destroy($id)
	{

		/*
		// get client order
		$clientOrderMetadata = $this->metadata->where('datakey','=','clients')->first();
		$this->order = $clientOrderMetadata->data; // CSV string

		if ($this->order !== '')
		{
			$clientOrderArray = explode(',',$this->order);
		}

		// remove toby id from order
		if (($key = array_search($id, $clientOrderArray)) !== false) {
		    unset($clientOrderArray[$key]);
		    $new_order = implode(',',$clientOrderArray);
		}

		// store updated order
		$this->metadata->updateOrCreate(
			['datakey' => 'clients'],
			['data' => $new_order]
		);
		*/

		$deadClient = $this->clients->findOrFail($id);
		// $deadClient->delete();

		$this->response = array('response_status' => 'success', 'message' => 'This client has been deleted successfully.');

		return redirect('admin/clients')->with('response', $this->response);
	}

	private function remapLogoPaths()
	{
		$this->bodyFileRegex = "/^\/uploads\/assets\/images\/clients\//";
		$allClients = $this->clients->get();

		foreach($allClients AS $client )
		{
			preg_match_all($this->bodyFileRegex, $client->logo, $matches);

			if ( count($matches[0])  === 0)
			{
				// directory prefix not found, append to 
				$client->logo = '/uploads/assets/images/clients/'.$client->logo;
				$client->update();
			}
		}
	}
}
