<?php namespace App\Http\Controllers\Admin\Blog;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests;

use Request;

// use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Admin;

use App\Models\Blog;
use App\Models\Person AS People;
use App\Models\Categories;
use App\Models\Tags;
use App\Models\RelatedLinks;

use App\Models\BlogAuthors;
use App\Models\BlogCategories;
use App\Models\BlogTags;
use App\Models\BlogRelatedLinks;

use App\Models\Metadata;

use App\Http\Requests\Admin\FeaturedRequest as FeaturedRequest;
use App\Http\Requests\Admin\BlogRequest as BlogRequest;

use Intervention\Image\ImageManagerStatic as Image;

// use App\Services\SitemapPackager;

class BlogController extends AdminController {

	/**
	 * Create a new controller instance with dependency injection
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, Blog $blog, Categories $categories, People $people, Tags $tags) {

		parent::__construct($guard);

		$this->blog = $blog;
		$this->categories = $categories; 
		$this->tags = $tags;

		$this->blogRoot = '/uploads/assets/images/blog/';
		$this->bodyFileRegex = "/\/uploads\/assets\/images\/blog\/((([a-zA-Z0-9_-]|%20)+)\.(jpg|png|gif|mp4|JPG|JPEG|PNG|GIF))/";

		// select active authors from people
		$this->authors = $people->where([
			['active','=',1],
			['author','=',1]
		])->get();

		// get list of all categories
		$this->categories = $this->categories->orderby('id','asc')->get();
	}


	public function index()
	{
		$this->context['title'] = 'Blog Management';

		$allBlog = $this->blog->orderby('created_at','desc')->get();

		$blogItems = [];
		foreach ( $allBlog AS $blog)
		{
			$blogItems[] = [
				'id' => $blog->id,
				'title' => $blog->title,
				'slug' => $blog->slug,
				'status' => $blog->status,
				'category' => $blog->getCategory(),
				'created' => $blog->created_at->format('d-m-Y') 
			];
		}
		
		$this->context['blog'] = $blogItems;

		// add featured article select field + submit button to index page
		// build list of posts 
		$postTitles = [
			'Select featured post'
		];

		foreach ($allBlog AS $blog)
		{
			$postTitles[$blog->id] = $blog->title;
		}

		// get featured post from metadata
		$metadata = new Metadata;
		$featured = $metadata->where('datakey','=','featured-post')->first();

		$this->context['test'] = [
			'featured' => $featured->data,
			'postTitles' => $postTitles
		];
		$this->context['featured'] = $featured->data;
		$this->context['featuredPostList'] = $postTitles;
		
		// get all tags in alphabetical order
		// $this->context['tags'] = $this->tags->orderby('text','asc')->get();

		// test: store list of tag IDs in metadata key:'tagtest'
		$this->context['tags'] = [];	
		$tagtestSD = Metadata::where('datakey', '=', 'tagtest')->select('data')->first();
		// serialize( $DATA )
		// unserialize( $DATATSRING )

		if ( !is_null($tagtestSD->data) ) {
			$linkedTagsIDs = explode(',',$tagtestSD->data);
			$this->context['tags'] = Tags::whereIn('id',$linkedTagsIDs)->select('id','text')->get();
		}
		
		$this->context['pageViewJS']		= 'admin/blog-index.min';

		return view('admin.blog.main', $this->context);
	}

	public function setfeatured(FeaturedRequest $request)
	{
		// store value of 
		// $request->featured
		// in metadata.datakey = 'featured-post'
		// firstOrCreate / firstOrNew / updateOrCreate
		Metadata::updateOrCreate(
			['datakey' => 'featured-post'],
			['data' => $request->featured]
		);

		$this->response = array('response_status' => 'success', 'message' => 'the featured post has been set');
		return redirect('admin/blog')->with('response', $this->response);
	}

	// GET create -> POST store
	public function create()
	{
		// build category dropdown
		$categoryOptions = [
			0 => 'Select category'
		];
		$this->context['title']				= 'Create Blog';
		$this->context['categoryList']		= $categoryOptions + array_pluck($this->categories,'title','id');
		$this->context['authorList']		= array_pluck($this->authors,'name','id');
		$this->context['pageViewCSS']		= '';
		$this->context['pageViewJS']		= 'admin/blog-create-edit.min';
		$this->context['id'] = '0'; // new blog, not assigned an id yet

		// blank form
		// reset tag fields
		// reset metadata:'tagtest'
		Metadata::updateOrCreate(
			[ 'datakey' => 'tagtest' ],
			[ 'data' => '' ]
		);
		$this->context['newtags'] = '';
		$this->context['tags'] = [];
		$this->context['links'] = [];

		return view('admin.blog.create', $this->context);
	}

	public function store(BlogRequest $request)
	{
		// insert record into database with placeholder image and thumb values
		// need to have id of new post to generate new image folder

		$data = [
			'revision'			=> time(),
			'status'			=> $request->status,
			'title'				=> $request->title,
			'slug'				=> $request->slug,
			'thumb'      		=> $request->thumb,
			'image'      		=> $request->image,
			'synopsis'			=> $request->synopsis,
			// 'body'				=> 'request->body',
			'body'				=> $request->body,
			'meta_title'		=> ($request->meta_title !== NULL) ? $request->meta_title : '-',
			'meta_description'	=> ($request->meta_description !== NULL) ? $request->meta_description : '-',
			'created_at'		=> $request->created_at
		];
	  	$this->blog->fill($data);
	  	$this->blog->save();
	  	$insertedId = $this->blog->id;



	  	/* attach authors */
		if (!is_null($request->authors)) 
		{
			// authors selected
		  	// $request->authors = array of people.id
		  	foreach ($request->authors AS $authorID)
		  	{
		  		$newItem = new BlogAuthors;
		  		$newItem->fill([
					'blog_id' => $insertedId,
					'people_id' => $authorID
		  		]);
		  		$newItem->save();
		  	}
		}

	  	/* attach category */
		// blog category: update or create blog_id,cagories_id
		BlogCategories::updateOrInsert(
	        ['blog_id' => $insertedId],
	        ['categories_id' => $request->category]
	    );

	  	/* attach tags */
	  	// new blog post ps no id exists until final submission
	  	// store tags in metadata:'tagtest' until successful submission
	  	// on submission clear metadata:'tagtest'

	  	// probably don't need to do this as this relating to a new blog
	  	BlogTags::where('blog_id', '=', $insertedId)->delete();

		$tagInserts = json_decode($request->newtags);
	  	$tagsToInsert = [];
	  	// ['blog_id' => $arbitraryBlogID, 'tags_id' => 0],

	  	// check for no tags specified
	  	// insert tags
	  	if( !is_null($tagInserts) ) {
		  	foreach($tagInserts->items AS $newTag)
		  	{
		  		$tagsToInsert[] = ['blog_id' => $insertedId, 'tags_id' => $newTag->id];
		  	}
			BlogTags::insert($tagsToInsert);
		}

	  	/* attach related links */
		// $request->rlink-meta JSON encoded array of link objects, important field is 'id'
		// iterate through items building array if link ids
		// add items to Blog_links, blog_id,links_id

		$linksToInsert = [];
		$newLinksMeta = json_decode($request->rlink_meta);
		if (!is_null($newLinksMeta))
		{
		  	foreach($newLinksMeta AS $newLink)
		  	{
		  		$linksToInsert[] = ['blog_id' => $insertedId, 'links_id' => $newLink->id];
		  	}

		  	// clear existing links to this blog
		  	// BlogRelatedLinks::where('blog_id', '=', $insertedId)->delete();

		  	// insert new links
			BlogRelatedLinks::insert($linksToInsert);
		}

		// $pkg_response = $this->packager->create(); // Update the Sitemap
		
		$this->response = array('response_status' => 'success', 'message' => 'This post has been added successfully.');

		return redirect('admin/blog')->with('response', $this->response);
	}

	// GET edit -> PUT update
	public function edit($id)
	{
		$post = $this->blog->findOrFail($id);
		//dd($post->categories->all()->to);

		// $post->current_categories = array_pluck($post->categories->all(),'title','id');
		//dd($post->current_categories);

		$this->context['title'] = 'Blog Management : Editing ' . $post->title;

		$this->context['post'] 			= $post;

		$this->context['authorList']		= array_pluck($this->authors,'name','id');

		$this->context['pageViewCSS']		= '';
		$this->context['pageViewJS']		= 'admin/blog-create-edit.min';

		$this->context['id'] = $id;

		$categoryOptions = [
			0 => 'Select category'
		];
		$this->context['categoryList']		= $categoryOptions + array_pluck($this->categories,'title','id');
		$this->context['category'] = $post->getCategory($id);
		$existingBlogAuthorList = $this->getAuthorIDs($id);
		$this->context['authors'] = $existingBlogAuthorList;

		/*
		// test: store list of tag IDs in metadata key:'tagtest'
		$this->context['tags'] = [];	
		$tagtestSD = Metadata::where('datakey', '=', 'tagtest')->select('data')->first();
		// serialize( $DATA )
		// unserialize( $DATATSRING )

		if ( !is_null($tagtestSD->data) ) {
			$linkedTagsIDs = explode(',',$tagtestSD->data);
			$this->context['tags'] = Tags::whereIn('id',$linkedTagsIDs)->select('id','text')->get();
		}
		$this->context['newtags'] = $tagtestSD->data;
		*/

		// get tags linked to this blog
		$blogTagIDs = BlogTags::where([
			['blog_id', '=', $id]
		])->select('tags_id')->get();

		$LinkedTagIds = array_pluck($blogTagIDs,'tags_id');

		$linkedTags = Tags::findMany($LinkedTagIds);
		$this->context['test'] = [
			'blogTagIDs' => $linkedTags
		];

		$this->context['newtags'] = '';
		$this->context['tags'] = $linkedTags;

		// get related links
		$blogRelatedLinks = BlogRelatedLinks::where([
			['blog_id', '=', $id]
		])->select('links_id')->get();

		$LinkedLinkIds = array_pluck($blogRelatedLinks,'links_id');

		$linkedLinks = RelatedLinks::findMany($LinkedLinkIds);

		$this->context['links'] = $linkedLinks;

		$this->context['test'] = [
			'blogLinkIDs' => $LinkedLinkIds,
			'blogLinks' => $linkedLinks
		];

		return view('admin.blog.edit', $this->context);
	}


	public function update(int $id, BlogRequest $request)
	{
		$post = $this->blog->findOrFail($id);

		$newPostRootDirectory = '/uploads/assets/images/blog/'.$request->slug.'-'.$id;

		if ( $post->image !== $request->image )
		{
	        $post->image = $request->image;
		}

		if ( $post->thumb !== $request->thumb )
		{
	        $post->thumb = $request->thumb;
		}
		$data = [
			'status'			=> $request->status,
			'title'				=> $request->title,
			'slug'				=> $request->slug,
			'synopsis'			=> $request->synopsis,
			'body'				=> $request->body,
			'meta_title'		=> ($request->meta_title !== NULL) ? $request->meta_title : '-',
			'meta_description'	=> ($request->meta_description !== NULL) ? $request->meta_description : '-',
			'created_at'		=> $request->created_at
		];

		$post->update($data);

		// check to see if blog-author link(s) have changed:
		// compare existing links against form data
		$existingBlogAuthorLinks = $this->getAuthorIDs($id);
		$formBlogAuthorLinks = $request->authors;

		if (!is_null($formBlogAuthorLinks)) 
		{
			// authors selected

			// check against existing authors
			// if there are authors linked, delete these links
			if ( $existingBlogAuthorLinks ) {
				BlogAuthors::where('blog_id','=',$id)->delete();
			}

			// insert new blog-author links
		  	foreach ($formBlogAuthorLinks AS $authorID)
		  	{
		  		$newItem = new BlogAuthors;
		  		$newItem->fill([
					'blog_id' => $id,
					'people_id' => $authorID
		  		]);
		  		$newItem->save();
		  	}
		} else {
			// no authors selected
			// remove any existing blog-author links
			BlogAuthors::where('blog_id','=',$id)->delete();
		}

		// blog category: update or create blog_id,cagories_id
		BlogCategories::updateOrInsert(
	        ['blog_id' => $id],
	        ['categories_id' => $request->category]
	    );

		// update tags

		// update related posts

		// $pkg_response = $this->packager->create(); // Update the Sitemap
		$this->response = array('response_status' => 'success', 'message' => 'This post has been updated successfully.');

		return redirect('admin/blog')->with('response', $this->response);
	}

	public function show($id)
	{
		//
	}

	public function destroy($id)
	{
		$post = $this->blog->findOrFail($id);

		// delete entries in
		// BlogAuthors
		// BlogCategory
		// Blogtags
		// Metadata datakey:'featured-post'

		/* remove images associated with blog post
		if ($post->thumb != "") {
			$thumb = public_path().'/uploads/blog/thumbs/'.$post->thumb;
			if (file_exists($thumb)) {
				unlink ($thumb);
			}
		}

		if ($post->image != "") {
			$image = public_path().'/uploads/blog/'.$post->image;
			if (file_exists($image)) {
				unlink ($image);
			}
		}
		*/

		$post->delete();

		// $pkg_response = $this->packager->create(); // Update the Sitemap

		$this->response = array('response_status' => 'success', 'message' => 'This post has been deleted successfully.');

		return redirect('admin/blog')->with('response', $this->response);
	}

	private function processThumbImage($request) {

		$filename = $request->slug . '-' . time() . '.' . $request->file('thumb')->getClientOriginalExtension();
		$filepath = public_path().'/uploads/assets/images/blog/thumbs/';

		// move uploaded file from temp location to 
		// /public/uploads/
		$request->file('thumb')->move(
	        $filepath, $filename
	    );
		// open file a image resource
		$img = Image::make($filepath.$filename);

		$img->fit(500)->save();
		return $filename;
	}

	private function processMainImage($request) {
		$filename = $request->slug . '-' . time() . '.' . $request->file('image')->getClientOriginalExtension();
		$filepath = public_path().'/uploads/assets/images/blog/';

		// /public/uploads/
		$request->file('image')->move(
	        $filepath, $filename
	    );

		// open file a image resource
		$img = Image::make($filepath.$filename);

		$img->fit(1920,775, function ($constraint) {
				//$constraint->aspectRatio();
			    $constraint->upsize();
			})->save();
		return $filename;
	}

	private function getBlogCategory(int $blogpostid)
	{
		$blog_category = BlogCategories::where('blog_id','=',$blogpostid)->select('categories_id')->first();

		$category = Categories::where('id','=', $blog_category['categories_id'])->first();
		return $category;
	}

	private function getAuthorIDs(int $blogpostid)
	{
		// generate array of author ids
		$existingLinks = BlogAuthors::where('blog_id','=',$blogpostid)->select('people_id')->get();
		$existingBlogAuthorList = [];
		foreach ( $existingLinks AS $link)
		{
			$existingBlogAuthorList[] = $link->people_id;
		}

		// returns array of author IDs or false
		return ( count($existingBlogAuthorList) > 0 ) ? $existingBlogAuthorList : false;
	}
}
