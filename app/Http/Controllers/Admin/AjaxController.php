<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use Config;
use Admin;

use App\Models\Blog;
use App\Models\Tags;
use App\Models\RelatedLinks;
use App\Models\Client AS Clients;

use App\Models\BlogTags;
use App\Models\BlogRelatedLinks;

use App\Models\Casestudy AS CaseStudies;
use App\Models\Module AS Module;
use App\Models\Module_Types;
use App\Models\Casestudies_Modules;

use App\Models\Metadata;

class AjaxController extends AdminController
{
	public function __construct(Blog $blog,Tags $tags, BlogTags $blogtags, Metadata $metadata)
	{
		$this->blog = $blog;
		$this->tags = $tags;
		$this->blogtags = $blogtags;
		$this->isNewBlog = true;

		$this->modulesFields = config('modules.fields');

		$this->metadata = $metadata;
	}

	/*
	--------------------------------------------------------------------------------------------

	Tags
	Model: Tags table: 'tags' relevant fields: 'id','text'
	Model: Blog table: 'blog' relevant fields: 'id'
	Model: BlogTags table: 'blog_tags' relevant fields: 'blog_id','tags_id' 
	*/

	public function linkTag(int $blog_id, int $tags_id) {
		// blog_id,tags_id
		// updateorcreate

		switch( $this->isNewBlog )
		{
			case '0' : {
				// existing blog post, has id

				// check to see if tag has already been linked to blog
				$blogTagCheck = BlogTags::where([
					['blog_id', '=', $blog_id],
					['tags_id', '=', $tags_id]
				])->get();

				if ( count($blogTagCheck) > 0 ) {
					// yes
					$tagIsLinked = true;

				} else {
					// insert entries into blog_tags
					BlogTags::insert(
				        ['blog_id' => $blog_id, 'tags_id' => $tags_id]
				    );
					$tagIsLinked = false;
				}

			} break;
			case '1' : {
				// new blog post, no ID as yet
				// insert entries into metadata:'tagtest'

				// check to see if tag has already been linked to blog
				$tagtestSD = Metadata::where('datakey', '=', 'tagtest')->select('data')->first();
				$tagtestList = ( $tagtestSD->data !== '' ) ? explode(',',$tagtestSD->data) : [];

				if( !in_array($tags_id, $tagtestList))
				{
					// tag not linked
					// link blog_tags: blog_id,tags_id
					// $this->link(newid blogid)
					$tagIsLinked = false;
					$tagtestList[] = $tags_id;
					Metadata::updateOrCreate(
						[ 'datakey' => 'tagtest' ],
						[ 'data' => implode(',',$tagtestList) ]
					);
				} else {
					$tagIsLinked = true;
				}

			} break;
		}

		return $tagIsLinked;
	}

	public function unlinkTag(int $blog_id, int $tags_id) {
		// blog_id,tags_id

		switch( $this->isNewBlog )
		{
			case '0' : {
				// existing blog post, has id
				// remove blog_tags entry:
				// blog_id = $blog_id, tags_id = $tags_id
				$blogTagCheck = BlogTags::where([
					['blog_id', '=', $blog_id],
					['tags_id', '=', $tags_id]
				])->delete();

			} break;
			case '1' : {
				// new blog post, no ID as yet
				// remove tag id from metadata:'tagtest'
				$tagtestSD = Metadata::where('datakey', '=', 'tagtest')->select('data')->first();
				$tagtestList = ( $tagtestSD->data !== '' ) ? explode(',',$tagtestSD->data) : [];

				if (($key = array_search($tags_id, $tagtestList)) !== false) {
		 		   unset($tagtestList[$key]);

					Metadata::updateOrCreate(
						[ 'datakey' => 'tagtest' ],
						[ 'data' => implode(',',$tagtestList) ]
					);
		 		}

				return 1;
			} break;
		}

	}

	public function addTag(Request $request) {
		// client sends text for new tag and blog id
		// $request->text
		// $request->blog

		$this->isNewBlog = $request->mode;

		$tagExists = false;
		$tagIsLinked = false;

		// check to see if tag already exists
		$tagCheck = Tags::where('text','=',$request->text)->first();
		if ( is_null($tagCheck) )
		{
			// text not in tags
			// insert new tags into 'tags' table
			// get new id
			$newTag = new Tags;
			$newTag->text = $request->text;
			$newTag->save();
			$tagID = $newTag->id;

		} else {
			// text is already in tags, do nothing
			$tagExists = true;
			$tagID = $tagCheck->id;
		}

		// check to see if in newblog mode
		// if so store tag list in metadata field 
		// (store final tag list in blog_tags when new post if successfully submitted)
		// other wise, blog exists, ($request->blog > 0, insert into blog_tags
		$tagIsLinked = $this->linkTag($request->blog, $tagID);
		// server returns status,new tag id

		// status
		// 0 = new tag created and linked -> add new item to tag list
		// 1 = text is already in tags but not linked -> add new item to tag list
		// 3 = text is in tags and linked -> do nothing
		$status = (1 * (true && $tagExists)) + (2 * (true && $tagIsLinked));

		$data = [
			'message' => 'addTag',
			'status' => $status,
			'mode' => $this->isNewBlog,
			'text' => $request->text,
			'blog' => $request->blog,
			'newid' => $tagID
		];
		return response()->json($data, 200);
	}

	public function removeTag(Request $request) {
		// client sends tag id, blog id
		// unlink blog_tags: blog_id,Tags_id
		$this->isNewBlog = $request->mode;

		$status = $this->unlinkTag($request->blog, $request->id);

		$data = [
			'message' => 'removeTag',
			'status' => $status,
			'mode' => $this->isNewBlog,
			'id' => $request->id,
			'blog' => $request->blog
		];
		return response()->json($data, 200);
	}

	public function autoTag(Request $request) {
		// get tags which current text field contents
		// client sends text to match against existing tags
		// check against tags currently linked to this blog, only return matches which aren't linked
		// server returns list of matches[id,text] to build HTML drop down list

		$text = $request->text;

		$matches = ( !is_null($text) && !preg_match('/$\s^/', $text)) ? $this->tags->where('text','LIKE',"{$text}%")->select('id','text')->orderby('text','asc')->get() : [];

		$data = [
			'message' => 'autoTag',
			'status' => 1,
			'text' => $text,
			'tags' => $matches
		];
		return response()->json($data, 200);
	}

	public function selectTag(Request $request) {
		// user selects an entry in the HTML drop down
		// client sends tag id, blog id
		// check to see if tag has already been linked to blog
		// link blog_tags: blog_id,tags_id
		$this->isNewBlog = $request->mode;

		// check to see if in newblog mode
		// if so store tag list in metadata field 
		// (store final tag list in blog_tags when new post if successfully submitted)
		// other wise, blog exists, ($request->blog > 0, insert into blog_tags
		$status = $this->linkTag($request->blog, $request->id);

		$data = [
			'message' => 'selectTag',
			'status' => ($status) ? 1 : 0,
			'mode' => $this->isNewBlog,
			'id' => $request->id,
			'blog' => $request->blog
		];
		return response()->json($data, 200);
	}

	/*
	--------------------------------------------------------------------------------------------

	Related links
	*/
	/*
	'blog_links' -> BlogRelatedLinks [blog_id,links_id]
	'links' -> RelatedLinks [id,title,url,thumbnail]
	'blog' -> Blog [id,*]
	*/
	public function addRelatedLink(Request $request) {

		$this->isNewBlog = $request->mode;

		// process image, move into blog subdirectory
		$thumbnail = (!is_null($request->thumbnail) && $request->thumbnail !== '') ? $request->thumbnail : '-';

		// insert and get new ID
		$newLink = new RelatedLinks;
		$newLink->fill([
			'url' => $request->url,
			'title' => $request->title,
			'external' => $request->external,
			'thumbnail' => $thumbnail
		]);
		$newLink->save();
		$newLinkID = $newLink->id;

		// link blog
		$status = $this->linkBlogLink($request->blog, $newLinkID);

		$data = [
			'message' => 'addRelatedLink',
			'status' => $status,
			'mode' => $this->isNewBlog,
			'blog' => $request->blog,
			'title' => $request->title,
			'url' => $request->url,
			'external' => $request->external,
			'thumbnail' => $thumbnail,
			'newid' => $newLinkID
		];
		return response()->json($data, 200);
	}	

	// remove related link completely
	// /admin/ajax/link/delete
	// blog id (if attached), 0 otherwise
	// relatedlink id
	// unlink from blog if attached to blog
	// delete item from relateedlinks table
	public function deleteRelatedLink(Request $request) {
		$this->isNewBlog = $request->mode;
		$status = $this->unlinkBlogLink($request->blog, $request->id);
		$uLink = RelatedLinks::find($request->id);
		if($uLink !== NULL)
		{
			$uLink->delete();
		}

		$data = [
			'message' => 'deleteRelatedLink',
			'status' => $status,
			'id' => $request->id,
			'blog' => $request->blog
		];
		return response()->json($data, 200);
	}

	// forge link between relatedlink and blog
	// /admin/ajax/link/link
	public function linkRelatedLink(Request $request) {
		$this->isNewBlog = $request->mode;

		$status = $this->linkBlogLink($request->blog, $request->id);

		$uLink = RelatedLinks::find($request->id);		

		$data = [
			'message' => 'linkRelatedLink',
			'status' => $status,
			'id' => $request->id,
			'title' => $uLink->title,
			'url' => $uLink->url,
			'external' => $uLink->external,
			'thumbnail' => $uLink->thumbnail,
			'blog' => $request->blog
		];
		return response()->json($data, 200);
	}

	// breaks link bewteen blog and link, does *not* delete the link from the relatedlinks table
	public function removeRelatedLink(Request $request) {

		$this->isNewBlog = $request->mode;

		$status = $this->unlinkBlogLink($request->blog, $request->id);

		$data = [
			'message' => 'removeRelatedLink',
			'status' => $status,
			'mode' => $this->isNewBlog,
			'id' => $request->id,
			'blog' => $request->blog
		];
		return response()->json($data, 200);
	}

	public function updateRelatedLink(Request $request) {
		$this->isNewBlog = $request->mode;

		// process image, move into blog subdirectory
		$thumbnail = (!is_null($request->thumbnail) && $request->thumbnail !== '') ? $request->thumbnail : '-';

		$uLink = RelatedLinks::find($request->id);
		$uLink->title = $request->title;
		$uLink->url = $request->url;
		$uLink->external = $request->external;
		$uLink->thumbnail = $thumbnail;
		$uLink->save();

		$data = [
			'message' => 'updateRelatedLink',
			'status' => 0,
			'mode' => $this->isNewBlog,
			'id' => $request->id
		];
		return response()->json($data, 200);
	}

	public function linkBlogLink(int $blog_id, int $link_id) {
		// two modes: create and edit
		// create: store blog-link linkages if form field & metadata as the new blog has no ID yet
		// edit: store blog-link linkages in RelatedLinks against blog id, link id

		// check to see if link exists for link and blog already (shouldn't do as the link id is always new)
		switch( $this->isNewBlog )
		{
			case '0' : {
				// existing blog post, has id
				$newLinkage = new BlogRelatedLinks;
				$newLinkage->fill([
					'blog_id' => $blog_id,
					'links_id' => $link_id
				]);
				$newLinkage->save();
			} break;
			case '1' : {
				// new blog post, no ID as yet
			} break;
		}

		return 0;
	}

	public function unlinkBlogLink(int $blog_id, int $link_id) {
		// two modes: create and edit
		// create: remove blog-link linkage from metadata
		// edit: remove blog-link linkage from RelatedLinks

		switch( $this->isNewBlog )
		{
			case '0' : {
				// existing blog post, has id
				BlogRelatedLinks::where([
					['blog_id','=',$blog_id],
					['links_id','=',$link_id]
				])->delete();

			} break;
			case '1' : {
				// new blog post, no ID as yet
			} break;
		}
		return 0;
	}

	/*
	case study order
	 filter : business/consumer
	 metadata keys:
	 'business-order'
	 'consumer-order'
	*/

	/*
	people order
	 metadata keys:
	 'people'
	*/

	/*
	--------------------------------------------------------------------------------------------
	
	clients order
	 metadata keys:
	 'clients'
	Model: Clients
	$this->metadata
	*/

	public function addClient(Request $request) {
	} 

	public function updateClient(Request $request) {
	} 

	public function removeClient(Request $request) {

		$toby = $request->id; // client 'to be' removed - finnegan sinister and ramone dexter

		$clientOrderMetadata = $this->metadata->where('datakey','=','clients')->first();
		$this->order = $clientOrderMetadata->data; // CSV string

		if ($this->order !== '')
		{
			$clientOrderArray = explode(',',$this->order);
		}

		// remove toby id from order
		if (($key = array_search($toby, $clientOrderArray)) !== false) {
		    unset($clientOrderArray[$key]);
		    $new_order = implode(',',$clientOrderArray);
		}

		// store updated order
		$this->metadata->updateOrCreate(
			['datakey' => 'clients'],
			['data' => $new_order]
		);

		// remove item
		$deadClient = Clients::findOrFail($toby);
		$deadClient->delete();

		$data = [
			'message' => 'removeClient',
			'id' => $toby,
			'status' => 1,
			'order' => $this->order,
			'order2' => $new_order
		];
		return response()->json($data, 200);
	} 

	// update order of clients
	public function orderClients(Request $request) {
		$this->metadata->updateOrCreate(
			['datakey' => 'clients'],
			['data' => $request->order]
		);

		$data = [
			'message' => 'orderClients',
			'status' => 1,
			'order' => $request->order
		];
		return response()->json($data, 200);
	} 

	/*
	--------------------------------------------------------------------------------------------
	
	People
	*/

	public function orderPeople(Request $request) {
		$this->metadata->updateOrCreate(
			['datakey' => 'people'],
			['data' => $request->order]
		);

		$data = [
			'message' => 'orderClients',
			'status' => 1,
			'order' => $request->order
		];
		return response()->json($data, 200);
	} 

	/*
	--------------------------------------------------------------------------------------------

	Modules
	*/

	public function getModule(Module $module, Module_Types $moduleTypes, Request $request) {

		$moduleList = array_pluck($moduleTypes->all(), 'title', 'ref');

		$requestedModuleType = $request->moduleType;

		$nextId = $request->module;
		$suffix = '_'.$request->casestudy.'_'.$nextId;

		$moduleType = $moduleTypes->getByReference( $requestedModuleType );

		$fields = $this->modulesFields[$requestedModuleType];

		$moduleData = [
			'suffix' => $suffix,
			'module-id' => $nextId,
			'type-id' => $moduleType->id,
			'type' => $requestedModuleType
		];

		$html = view('admin.casestudies.partials.modules.module-container', ['id' => $request->casestudy, 'key' => $request->key, 'metakey' => $request->metakey, 'moduleData' => $moduleData, 'moduleList' => $moduleList ]);

		return $html;
	}

	/*
	--------------------------------------------------------------------------------------------

	Casestudies
	*/

	public function orderCasestudies(Request $request) {

		$this->metadata->updateOrCreate(
			['datakey' => $request->category],
			['data' => $request->order]
		);

		$data = [
			'message' => 'orderCasestudies',
			'status' => 1,
			'category' => $request->category,
			'order' => $request->order
		];
		return response()->json($data, 200);
	} 

	public function removeCasestudy(Request $request) {
		$deadCasestudy = CaseStudies::find($request->id);

		$data = [
			'message' => 'removeCasestudy',
			'status' => 999,
			'id' => 0,
			'category' => '',
			'order' => ''
		];

		if($deadCasestudy !==  NULL)
			{
			// get case study order from metadata
			$OrderMetadata = explode(',', $this->metadata->where('datakey','=',$request->category )->first()->data );

			// find id in order
			$key = array_search($request->id, $OrderMetadata );

			// remove id from order
			array_splice($OrderMetadata,$key,1);

			// store updated order in metadata
			$this->metadata->updateOrCreate(
				['datakey' => $request->category],
				['data' => implode(',',$OrderMetadata)]
			);

			// finally, delete case study
			$deadCasestudy->delete();

			$data = [
				'message' => 'removeCasestudy',
				'status' => 1,
				'id' => $request->id,
				'category' => $request->category,
				'order' => implode(',', $OrderMetadata ) // return order to client to synchronise with the db and the drag-and-drop sorter
			];
		}

		return response()->json($data, 200);
	}

	/*
	--------------------------------------------------------------------------------------------
	*/
}
