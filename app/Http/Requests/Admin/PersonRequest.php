<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class PersonRequest extends FormRequest {

	public function __construct() {
	
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

/*
'name',
'slug',
'role',
'bio',
'portrait',
'twitter',
'linkedin',
'telephone',
'email',
'colour',
'active',
'showInPeoplePage',
'author'
*/
		$rules = [
			'name' => 'required|min:3|max:255',
			'role' => 'required|min:3|max:255',
			'bio' => 'required',
			// 'portrait' => 'mimes:png,jpeg,jpg',
			'portrait' => 'required',
			'active' => 'required',
			'showInPeoplePage' => 'required',
			'author' => 'required'
		];		

		/*
		$rules = [
			'title'				=> 'required|min:3|max:255',
			'slug'				=> 'required|unique:blog|min:3|max:255',
			'thumb'      		=> 'required|mimes:png,jpeg,jpg|max:2048',
			'image'      		=> 'mimes:png,jpeg,jpg|max:2048',
			'synopsis'			=> 'required|min:3|max:255',
			'body'				=> 'required',
			'meta_title'		=> 'max:255',
			'meta_description'	=> 'max:255',
			'status'			=> 'required'
		];
		
		if (Request::input('update')) {
			$id = Request::segment(4);
			$rules['thumb'] = 'mimes:png,jpeg,jpg|max:2048';
			$rules['image'] = 'mimes:png,jpeg,jpg|max:2048';
			$rules['slug'] = 'required|unique:blog,slug,'.$id.'|min:3|max:255';
		}
		*/

		return $rules;
	}

}
