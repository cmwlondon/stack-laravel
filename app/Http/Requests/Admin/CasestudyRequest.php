<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

use App\Models\Casestudy AS CaseStudies;

class CasestudyRequest extends FormRequest {

	public function __construct() {
	
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

		// store action: case study does not exist so return id: 0
		// update action: get current post id
		$thisCasestudy = CaseStudies::find($this->id);
		$ignoreID = ( $thisCasestudy !== NULL ) ? $thisCasestudy->id : 0;

		// validation rules for static case study fields
		$rules = [
			'title'				=> 'required|min:3|max:255',
			'slug'				=> "required|min:3|max:255|unique:casestudies,slug,{$ignoreID}", // check for unique slug / edit, dont compare with slug of subject casestudy
			'branch'			=> 'required',
			'thumb_main'		=> 'required|min:3|max:255',
			'thumb_sub'			=> 'max:255',
			'thumb_image'		=> 'required',
			'thumb_colour'		=> 'required',
			'meta_title'		=> 'required',
			'meta_desc'			=> 'max:255',
			'meta_keywords'		=> 'max:255',
			'page_colour'		=> 'required',
			'modules' 			=> 'required',
			'active'			=> 'required'
		];

		if( $this->modulesFieldList !== NULL )
		{	
			$moduleData = json_decode($this->modulesFieldList);
			$modulesFields = config('modules.fields');
			$values = [];

			// echo "<pre>".print_r($moduleData, true)."</pre>";

			foreach($moduleData AS $module)
			{
				// echo "<p>Type: '{$module->type}' id: {$module->id} suffix: {$module->suffix}</p>";
				switch($module->type)
				{
					case 'copy' :
					{
						$key = 'html'.$module->suffix;
						$rules[$key] = 'required';
						$values[] = [
							'key' => $key,
							'value' => $this->$key
						];
					}
					break;
					case 'inline-image' :
					{
						$key = 'iimage'.$module->suffix;
						$rules[$key] = 'required';
						$values[] = [
							'key' => $key,
							'value' => $this->$key
						];
					}
					break;
					case 'reveal-image' :
					{
						$key = 'rimage'.$module->suffix;
						$rules[$key] = 'required';
						$values[] = [
							'key' => $key,
							'value' => $this->$key
						];
					}
					break;
					case 'video-manual' :
					{
						$rules['poster'.$module->suffix] = 'required';

						// check to see if at least one video file from 'mp4','ovg','webm' has been selected
						$rules['mp4'.$module->suffix] = 'required';

						$rules['vid'.$module->suffix] = 'required';
					}
					break;
					case 'autoplayvideo' :
					{
						$rules['poster'.$module->suffix] = 'required';

						// check to see if at least one video file from 'mp4','ovg','webm' has been selected
						$rules['mp4'.$module->suffix] = 'required';

						$rules['vid'.$module->suffix] = 'required';
					}
					break;
					case 'image-grid' :
					{
						$rules['gimg1'.$module->suffix] = 'required';
						$rules['gimg2'.$module->suffix] = 'required';
						$rules['gimg3'.$module->suffix] = 'required';
						$rules['gimg4'.$module->suffix] = 'required';
					}
					break;
					case 'brochure-video' :
					{

					}
					break;
					case 'evaluate2video' :
					{

					}
					break;
					case 'ev2header' :
					{

					}
					break;
				}
			}

		}
		// echo "<pre>".print_r($rules, true)."</pre>";
		// echo "<pre>".print_r($values, true)."</pre>";
		// die();

		return $rules;
	}

	public function messages()
	{
		// echo "<p>messages</p>";

		$messages = [
			'title.required'			=> 'Please select a title',
			'slug.required'				=> 'Please specify a URL for this case study',
			'slug.unique'				=> 'Please specify a unique url',
			'branch.required'			=> 'Please select a category',
			'thumb_image.required'		=> 'Please select a thumbnail image',
			'thumb_main.required'		=> 'Please specify a title for the thumbnail',
			'thumb_sub.max:255'			=> 'Please use less then 255 characters',
			'thumb_colour'				=> 'Please select an underline colour for the thumbnail caption',
			'meta_title'				=> 'Please specify a page title for SEO',
			'page_colour'				=> 'Please select a page coulour scheme',
			'modules.required' 					=> 'Please select at least one module',
			'active.required' 			=> 'Please state whether this case study is active or not'
	    ];

		if( $this->modulesFieldList !== NULL )
		{	
			$moduleData = json_decode($this->modulesFieldList);
			// echo "<pre>".print_r($moduleData, true)."</pre>";

			$modulesFields = config('modules.fields');

			foreach($moduleData AS $module)
			{

				switch($module->type)
				{
					case 'copy' :
					{
						$messages['html'.$module->suffix.'.required'] = 'Please add some copy to this module';
					}
					break;

					case 'inline-image' :
					{
						$messages['iimage'.$module->suffix.'.required'] = 'Please select an image for this module';
					}
					break;

					case 'reveal-image' :
					{
						$messages['rimage'.$module->suffix.'.required'] = 'Please select an image for this module';
					}
					break;

					case 'video-manual' :
					{
						$messages['poster'.$module->suffix.'.required'] = 'Please select a placeholder image for this module';

						// check to see if at least one video file from 'mp4','ovg','webm' has been selected
						$messages['mp4'.$module->suffix.'.required'] = 'Please select an MP4 format video file for this module';

						$messages['vid'.$module->suffix.'.required'] = 'Please specifiy a unique id for this video';
					}
					break;

					case 'autoplayvideo' :
					{
						$messages['poster'.$module->suffix.'.required'] = 'Please select a placeholder image for this module';

						// check to see if at least one video file from 'mp4','ovg','webm' has been selected
						$messages['mp4'.$module->suffix.'.required'] = 'Please select an MP4 format video file for this module';

						$messages['vid'.$module->suffix.'.required'] = 'Please specifiy a unique id for this video';
					}
					break;

					case 'image-grid' :
					{
						$messages['gimg1'.$module->suffix.'.required'] = 'Please select an image for this module';
						$messages['gimg2'.$module->suffix.'.required'] = 'Please select an image for this module';
						$messages['gimg3'.$module->suffix.'.required'] = 'Please select an image for this module';
						$messages['gimg4'.$module->suffix.'.required'] = 'Please select an image for this module';
					}
					break;

					case 'brochure-video' :
					{

					}
					break;

					case 'evaluate2video' :
					{

					}
					break;

					case 'ev2header' :
					{

					}
					break;
				}
			}

		}

		// echo "<pre>".print_r($messages, true)."</pre>";
		// die();

	    return $messages;
	}

	// compare supplied slug/url with casestudy slug field in db, check to see if a slug has already been used
	public function withValidator($validator)
	{
		$slugs = CaseStudies::all(['slug'])->toArray();
		$slugs2 = array_map(
			function($slug)
			{
				return $slug['slug'];
			},
			$slugs
		);

		if( in_array( $this->slug, $slugs2) )
		{
			$validator->errors()->add('slug', 'Please specify a unique url.'); // return error
		}
	}
}
