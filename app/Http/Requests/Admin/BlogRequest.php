<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class BlogRequest extends FormRequest {

	public function __construct() {
	
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		/* */
		$rules = [
			'title'				=> 'required|min:3|max:255',
			'category'			=> 'required|numeric|gt:0',
			'slug'				=> 'required|unique:blog|min:3|max:255',
			// 'thumb'      		=> 'required|mimes:png,jpeg,jpg|max:2048',
			// 'image'      		=> 'mimes:png,jpeg,jpg|max:2048',
			'thumb'      		=> 'required',
			'image'      		=> 'required',
			'synopsis'			=> 'required|min:3|max:255',
			'body'				=> 'required',
			// 'authors'			=> 'required',
			'meta_title'		=> 'max:255',
			'meta_description'	=> 'max:255',
			'status'			=> 'required'
		];
		
		if (Request::input('update')) {
			$id = Request::segment(4);
			// $rules['thumb'] = 'mimes:png,jpeg,jpg|max:2048';
			// $rules['image'] = 'mimes:png,jpeg,jpg|max:2048';
			$rules['thumb'] = 'required';
			$rules['image'] = 'required';
			$rules['slug'] = 'required|unique:blog,slug,'.$id.'|min:3|max:255';
		}
		/* */

		return $rules;
	}

	public function messages()
	{
	    return [
	    	'category.gt:0' => 'Please select a category'
	    ];
	}
}
