<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class ClientRequest extends FormRequest {

	public function __construct() {
	
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		/* */
		$rules = [
			'title'				=> 'required|min:3|max:255',
			'text'				=> 'required|min:3|max:255',
			'logo'      		=> 'required'
			// 'active'			=> 'required'
		];
		
		/*
		if (Request::input('update')) {
			$id = Request::segment(4);
			// $rules['thumb'] = 'mimes:png,jpeg,jpg|max:2048';
			// $rules['image'] = 'mimes:png,jpeg,jpg|max:2048';
			$rules['thumb'] = 'required';
			$rules['image'] = 'required';
			$rules['slug'] = 'required|unique:blog,slug,'.$id.'|min:3|max:255';
		}
		*/

		return $rules;
	}

	public function messages()
	{
	    return [
	    ];
	}
}
