<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Casestudies_Modules extends Model
{
	/*
	id
	casestudies_id
	module_id
	created_at
	updated_at
	*/

    protected $table = 'casestudies_modules';
    protected $primaryKey = "id";
    public $timestamps = true;
	protected $fillable = ['casestudies_id', 'modules_id'];
}