<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blog';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['revision', 'status', 'title', 'slug', 'thumb', 'image', 'body', 'synopsis', 'meta_title', 'meta_description', 'created_at', 'updated_at'];

    // has one category
    // has none - many authors
    // has none - many tags
    // has none - many related links

	/**
     * Get the categories this post belongs to
     */
    /*
    public function categories()
    {
        return $this->belongsToMany('App\Models\Categories', 'blog_categories', 'blog_id', 'categories_id');
    }
    */
    
    public function scopePreview($query) {
        return $query->select('title','slug','thumb','synopsis');
    }

    public function scopePublished($query,$preview) {
        if ($preview == "true") {
            return $query;
        } else {
            return $query->where('status','published');
        }
    }

     public function scopeSlug($query, $slug) {
        return $query->where('slug',$slug);
    }

    //
    public function scopeSearch($query, $terms) {
        foreach ($terms as $term) {
            $query->where('title','LIKE','%'.$term.'%');
        }
        return $query;
    }

    public function scopeOrdered($query) {
         return $query->orderBy('order','desc');
    }

    public function scopeOrderedDate($query) {
         return $query->orderBy('created_at','desc');
    }

    public function scopeOlder($query, $date) {
        $query->where('created_at','<',$date);
        return $query->orderBy('created_at','desc');
    }

    public function scopeNewer($query, $date) {
        $query->where('created_at','>',$date);
        return $query->orderBy('created_at','asc');
    }

    public function getAuthors()
    {   
        // $this->id
        $thisPostAuthors = BlogAuthors::where('blog_id','=',$this->id)->get();

        $aid = [];
        $authors = [];

        // get author IDs for post
        foreach ($thisPostAuthors AS $postAuthorLink)
        {
            $aid[] = $postAuthorLink->people_id;
            
        }

        // get author data
        return Person::wherein('id',$aid)->get();
    }

    public function getCategory()
    {   
        $blog_category = BlogCategories::where('blog_id','=',$this->id)->select('categories_id')->first();
        $category = Categories::where('id','=', $blog_category['categories_id'])->first();
        return $category;
    }

    public function getTags()
    {   
        // $this->id
    }

    public function getRelatedLinks()
    {   
        // $this->id
    }

}
