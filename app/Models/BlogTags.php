<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogTags extends Model
{

	/*
	blog_tags:
		id
		blog_id
		tags_id
		created_at
		updated_at
	*/

    protected $table = 'blog_tags';

    protected $primaryKey = "id";

    public $timestamps = true;

	protected $fillable = ['blog_id','tags_id'];


	// $ready = InstagramModel::where('state', 'ready')->latest()->limit(20)->get();

}











