<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogRelatedLinks extends Model
{

	/*
	metadata:
		id
		blog_id
		link_id
		created_at
		updated_at
	*/

    protected $table = 'blog_links';

    protected $primaryKey = "id";

    public $timestamps = true;

	protected $fillable = ['blog_id','links_id'];


	// $ready = InstagramModel::where('state', 'ready')->latest()->limit(20)->get();

}











