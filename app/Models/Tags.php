<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{

	/*
	tags:
		id
		text
		created_at
		updated_at
	*/

    protected $table = 'tags';

    protected $primaryKey = "id";

    public $timestamps = true;

	protected $fillable = ['text'];


	// $ready = InstagramModel::where('state', 'ready')->latest()->limit(20)->get();

}











