<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelatedLinks extends Model
{

	/*
	metadata:
		id
		title
		url
		'external'
		thumbnail
		created_at
		updated_at
	*/

    protected $table = 'links';

    protected $primaryKey = "id";

    public $timestamps = true;

	protected $fillable = ['title','url','external','thumbnail'];


	// $ready = InstagramModel::where('state', 'ready')->latest()->limit(20)->get();

}











