<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
	/*
    protected $table = 'consultation';
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function getProducts(string $answers)
    {
    	return $this->where('REF','=',$answers)->get();
    }
	*/

    protected $seven = [
	  [
	    "id" => 1,
	    "title" => "STACK BOLSTERS DATA DEPT. WITH TWO SENIOR HIRES",
	    "date" => "November 2018",
	    "page" => "datahire01",
	    "story" => "201811-datahire01"
	  ],
	  [
	  	"id" => 2,
	    'title' => 'THE STORY BEHIND SOLACE&rsquo;S #STOPITCOMINGHOME CAMPAIGN',
	    'date' => 'October 2018',
	    'page' => 'solace02',
	    'story' => '201810-solace02'
	  ],
	  [
	    "id" => 3,
	    'title' => 'STACK LAUNCHES NEW MINI-BLOCKBUSTER TV ADVERT FOR WEBUYANYHOME',
	    'date' => 'September 2018',
	    'page' => 'wbah01',
	    'story' => '201809-wbah01'
	  ],
	  [
	    "id" => 4,
	    'title' => 'HERTZ CELEBRATE THEIR 100TH YEAR ANNIVERSARY WITH A NEW CAMPAIGN FROM STACK',
	    'date' => 'September 2018',
	    'page' => 'hertz100',
	    'story' => '201809-hertz01'
	  ],
	  [
	    "id" => 5,
	    'title' => 'STACK launch World Cup campaign for Solace',
	    'date' => 'July 2018',
	    'page' => 'solace',
	    'story' => '201807-solace'
	  ],
	  [
	    "id" => 6,
	    'title' => 'MSQ Partners win Hertz 100',
	    'date' => 'July 2018',
	    'page' => 'hertz',
	    'story' => '201807-hertz'
	  ],
	  [
	    "id" => 7,
	    'title' => 'Blu appoints MSQ Partners',
	    'date' => 'June 2018',
	    'page' => 'blu',
	    'story' => '201807-blu'
	  ]
	];

	public function getAllStories() 
	{
		return $this->seven;
	}

	public function getThisStory(int $story) 
	{
		$otherStories = [];
		$thisStory = null;

		$l = count($this->seven);
		$n = 0;
		

		while ($n < $l) {
			$i = $this->seven[$n];
			if ($i['id'] === $story)
			{
				$thisStory = $i;
			} else {
				$otherStories[] = $i;
			}
			$n++;
		}

		return [
			"item" => $thisStory,
			"extras" => $otherStories
		];	
	}

	public function getLatestSix(int $story) 
	{
		return $this->seven;	
	}
}
