<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategories extends Model
{

	/*
	blog_categories:
		id
		blog_id
		categories_id
		created_at
		updated_at
	*/

    protected $table = 'blog_categories';

    protected $primaryKey = "id";

    public $timestamps = true;

	protected $fillable = ['blog_id','categories_id'];


	// $ready = InstagramModel::where('state', 'ready')->latest()->limit(20)->get();

}











