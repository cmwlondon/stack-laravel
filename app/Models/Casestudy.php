<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

define('ALL_CASESTUDIES', 1);
define('ACTIVE_CASESTUDIES', 2);
define('INACTIVE_CASESTUDIES', 3);
define('COMBINED_CASESTUDIES', 4);

class Casestudy extends Model
{
	/*
id
title
slug
branch
thumb_main (t1)
thumb_sub (t2)
thumb_image
thumb_colour
meta_title
meta_desc
meta_keywords
page_colour
modules -> maybe break this field out into a JOIN table casestudies_modules
active
created_at
updated_at
*/

    protected $table = 'casestudies';
    protected $primaryKey = "id";
    public $timestamps = true;
	protected $fillable = ['title', 'slug', 'branch', 'thumb_main', 'thumb_sub', 'thumb_image', 'thumb_colour', 'meta_title', 'meta_desc', 'meta_keywords', 'page_colour', 'modules', 'active'];

    // passing queries as parameters to other functions
    public function getAlpha($query)
    {
        return $query->where([['active', '=', 1]])->get();
    }

    public function getBeta()
    {
        return $this->getAlpha( $this->where('branch', '=', 'consumer') );
    }

    private function getCaseStudiesByState($query, int $mode)
    {
        switch( $mode )
        {
            // ALL_CASESTUDIES
            case 1:
            {
                return $query->get();
            }
            break;

            // ACTIVE_CASESTUDIES
            case 2:
            {
                return $query->where([
                    ['active', '=', 1]
                ])->get();
            }
            break;

            // INACTIVE_CASESTUDIES
            case 3:
            {
                return $query->where([
                    ['active', '=', 0]
                ])->get();
            }
            break;

            // COMBINED_CASESTUDIES
            case 4:
            {
                return $query
                ->orderBy('branch','asc')
                ->orderBy('title','asc')
                ->get();
            }
            break;
        }
    }

    public function getCombinedCasestudies(int $mode = 1)
    {
        return $this->getCaseStudiesByState( $this, $mode );
    }

    private function getBranchCasestudies(string $branch, int $mode)
    {
        return $this->getCaseStudiesByState( $this->where([['branch', '=', $branch]]), $mode );
    }

    public function getBusinessCasestudies(int $mode = 1)
    {
    	return $this->getBranchCasestudies('business', $mode);
    }

    public function getConsumerCasestudies(int $mode = 1)
    {
    	return $this->getBranchCasestudies('consumer', $mode);
    }

    public function getCasestudyByID(int $casestudy)
    {
    	return $this->where('id',$casestudy)->first();
    }

    public function getCasestudyBySlug(string $casestudyslug)
    {
    	return $this->where('slug',$casestudyslug)->first();
    }

}
