<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Panel extends Model
{
	/*
    protected $table = 'consultation';
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function getProducts(string $answers)
    {
    	return $this->where('REF','=',$answers)->get();
    }
	*/

    protected $panels = [
	    [
	    	'id' => '2',
	    	'desktop' => 'home1.jpg',
	    	'mobile' => 'home1.jpg',
	    	'xc' => 0,
	    	'colour' => '#67b2e8',
	    	'copy' => 'The more time your audience spends thinking about your brand, the more likely they are to choose&nbsp;you'
	    ],
	    [
	    	'id' => '3',
	    	'desktop' => 'home2.jpg',
	    	'mobile' => 'home2.jpg',
	    	'xc' => 0,
	    	'colour' => '#ffb819',
	    	'copy' => 'But creating time isn&rsquo;t easy – people have less of it, and want more from&nbsp;it'
	    ],
		[
	    	'id' => '4',
	    	'desktop' => 'home3.jpg',
	    	'mobile' => 'home3.jpg',
	    	'xc' => 0,
	    	'colour' => '#e19cdf',
	    	'copy' => 'We combine insight and inspiration by stacking <span class="textstream ts4"><span class="on">Strategists</span><span>Writers</span><span>Data Scientists</span><span>Artists</span><span>Film-makers</span><span>Game designers</span><span>Behavioural experts</span><span>Tech Gods</span><span>Design Gurus</span></span>'
	    ],
	    [
	    	'id' => '5',
	    	'desktop' => 'home4.jpg',
	    	'mobile' => 'home4.jpg',
	    	'xc' => 0,
	    	'colour' => '#67b2e8',
	    	'copy' => 'And we make time pay by fitting your brand into your audience&rsquo;s&nbsp;world'
	    ]
    ];

    public function getAll()
    {
    	return $this->panels;
    }
}
