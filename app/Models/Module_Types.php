<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module_Types extends Model
{
    protected $table = 'module_types';
    protected $primaryKey = "id";
    public $timestamps = true;
	protected $fillable = [ 'title', 'ref', 'screenshot', 'comments' ];

	public function getByReference(string $ref)
	{
		return $this->where('ref','=',$ref)->first();
	}

	public function getByID(int $id)
	{
		return $this->where('id','=',$id)->first();
	}
}