<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Metadata extends Model
{

	/*
	metadata:
		id
		datakey
		data
		created_at
		updated_at
	*/

    protected $table = 'metadata';

    protected $primaryKey = "id";

    public $timestamps = true;

	protected $fillable = ['datakey','data'];


	// $ready = InstagramModel::where('state', 'ready')->latest()->limit(20)->get();

}











