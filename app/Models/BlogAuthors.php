<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogAuthors extends Model
{

	/*
	blog_authors:
		id
		blog_id
		people_id
		created_at
		updated_at
	*/

    protected $table = 'blog_authors';

    protected $primaryKey = "id";

    public $timestamps = true;

	protected $fillable = ['blog_id','people_id'];


	// $ready = InstagramModel::where('state', 'ready')->latest()->limit(20)->get();

}











