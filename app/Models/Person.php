<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{

	/*
	people:
		id
		name
		slug (data-name)
		role
		bio
		portrait (media:image:path)
		twitter
		linkedin
		telephone
		email
		colour
		active (true/false : 1/0)
		showInPeoplePage  (true/false : 1/0)
		author  (true/false : 1/0)
		created_at
		updated_at
	*/

    protected $table = 'people';

    protected $primaryKey = "id";

    public $timestamps = true;

	protected $fillable = ['name', 'slug', 'role', 'bio', 'portrait', 'twitter', 'linkedin', 'telephone', 'email', 'colour', 'active','showInPeoplePage','author'];


	// $ready = InstagramModel::where('state', 'ready')->latest()->limit(20)->get();

}











