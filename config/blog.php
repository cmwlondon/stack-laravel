<?php

return [
    // map post category slug to pretty category title, for blog index page
    'filters' => [
        [
            'caption' => 'All',
            'value' => ''
        ],
        [
            'caption' => 'News',
            'value' => 'news'
        ],
        [
            'caption' => 'Campaign Launch',
            'value' => 'launch'
        ],
        [
            'caption' => 'STACK Thinking',
            'value' => 'stack-thinking'
        ]
    ]
];
