<?php
/*
Case Study configuration
*/

return [
	/*
    |--------------------------------------------------------------------------
    | enumerateFields
    |--------------------------------------------------------------------------
    |
    | describes which form fields have specified options
    |
	*/
	'enumerateFields' => [
		'thumb_colour' => [
			[
				'value' => 'linePurple',
				'caption' => 'Purple',
				'colour' => '#e19cdf'
			],
			[
				'value' => 'lineBlue',
				'caption' => 'Blue',
				'colour' => '#67b2e8'
			],
			[
				'value' => 'lineYellow',
				'caption' => 'Yellow',
				'colour' => '#ffb819'
			],
			[
				'value' => 'lineGreen',
				'caption' => 'Green',
				'colour' => '#95d600'
			]
		],
		'page_colour' => [
			[
				'value' => 'colourSchemePurple',
				'caption' => 'Purple',
				'colour' => '#e19cdf'
			], 
			[
				'value' => 'colourSchemeBlue',
				'caption' => 'Blue',
				'colour' => '#67b2e8'
			], 
			[
				'value' => 'colourSchemeYellow',
				'caption' => 'Yellow',
				'colour' => '#ffb819'
			], 
			[
				'value' => 'colourSchemeGreen',
				'caption' => 'Green',
				'colour' => '#95d600'
			]
			
		],
		'branch' => [
			[
				'value' => 'business',
				'caption' => 'Business'
			],
			[
				'value' => 'consumer',
				'caption' => 'Consumer'
			]
		]
	]
];