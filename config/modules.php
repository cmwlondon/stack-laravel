<?php
/*
Case Study Modules configuration
*/

return [
	/*
    |--------------------------------------------------------------------------
    | fields
    |--------------------------------------------------------------------------
    |
    | describes which form fields relate to which module type
    |
	*/

	'fields' => [
		'copy' => [
			'html'
		],

		'inline-image' => [
			'iimage',
			'ialt'
		],
		'reveal-image' => [
			'rimage',
			'ralt'
		],

		'video-manual' => [
			'poster',
			'mp4',
			'webm',
			'ogv',
			'class',
			'vid'
		],

		'image-grid' => [
			'gimg1',
			'gimg2',
			'gimg3',
			'gimg4',
			'galt'
		],

		'brochure-video' => [
			'poster',
			'mp4',
			'webm',
			'ogv'
		],

		'evaluate2video' => [
			'poster',
			'mp4',
			'webm',
			'ogv',
			'class',
			'vid'
		],

		'autoplayvideo' => [
			'poster',
			'mp4',
			'webm',
			'ogv',
			'class',
			'vid'
		],

		'ev2header' => []
	],
	'map' => [
		'copy' => [
			[
				'form' => 'html',
				'data' => 'html'
			]
		],

		'inline-image' => [
			[
				'form' => 'iimage',
				'data' => 'image'
			],
			[
				'form' => 'ialt',
				'data' => 'alt'
			]
		],
		'reveal-image' => [
			[
				'form' => 'rimage',
				'data' => 'image'
			],
			[
				'form' => 'ralt',
				'data' => 'alt'
			]
		],

		'video-manual' => [
			[
				'form' => 'poster',
				'data' => 'poster'
			],
			[
				'form' => 'mp4',
				'data' => 'mp4'
			],
			[
				'form' => 'webm',
				'data' => 'webm'
			],
			[
				'form' => 'ogv',
				'data' => 'ogv'
			],
			[
				'form' => 'class',
				'data' => 'class'
			],
			[
				'form' => 'vid',
				'data' => 'vid'
			]
		],

		'image-grid' => [
			[
				'form' => 'gimg1',
				'data' => 'images'
			],
			[
				'form' => 'gimg2',
				'data' => 'images'
			],
			[
				'form' => 'gimg3',
				'data' => 'images'
			],
			[
				'form' => 'gimg4',
				'data' => 'images'
			],
			[
				'form' => 'galt',
				'data' => 'alt'
			]
		],

		'brochure-video' => [
			[
				'form' => 'poster',
				'data' => 'poster'
			],
			[
				'form' => 'mp4',
				'data' => 'mp4'
			],
			[
				'form' => 'webm',
				'data' => 'webm'
			],
			[
				'form' => 'ogv',
				'data' => 'ogv'
			]
		],

		'evaluate2video' => [
			[
				'form' => 'poster',
				'data' => 'poster'
			],
			[
				'form' => 'mp4',
				'data' => 'mp4'
			],
			[
				'form' => 'webm',
				'data' => 'webm'
			],
			[
				'form' => 'ogv',
				'data' => 'ogv'
			],
			[
				'form' => 'class',
				'data' => 'class'
			],
			[
				'form' => 'vid',
				'data' => 'vid'
			]
		],

		'autoplayvideo' => [
			[
				'form' => 'poster',
				'data' => 'poster'
			],
			[
				'form' => 'mp4',
				'data' => 'mp4'
			],
			[
				'form' => 'webm',
				'data' => 'webm'
			],
			[
				'form' => 'ogv',
				'data' => 'ogv'
			],
			[
				'form' => 'class',
				'data' => 'class'
			],
			[
				'form' => 'vid',
				'data' => 'vid'
			]
		],
		
		'ev2header' => [
			[
				'form' => '',
				'data' => ''
			]
		]
	]
];