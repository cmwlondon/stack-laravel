<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// put redirect rules BEFORE the main route

// landing page /index.html -> / 301 redirect
Route::get('/index.html', function () {
    return redirect('/');
});
// general page 301 redirect
// /[PAGE].html -> /[PAGE]
Route::get('/{page}.html', function ($page) {
    return redirect('/'.$page);
});

Route::permanentRedirect('/casestudy/lechameau.html','/casestudy/lechameau-when-you-know');

Route::permanentRedirect('/casestudy/opg.html','/casestudy/opg-be-my-voice');

Route::permanentRedirect('/casestudy/solace.html','/casestudy/solace-stopitcominghome');

Route::permanentRedirect('/casestudy/blu.html','/casestudy/blu-never-look-back');

Route::permanentRedirect('/casestudy/amazon.html','/casestudy/amazon-think-big');

Route::permanentRedirect('/casestudy/wbah.html','/casestudy/wbah-mini-blockbusters');

Route::permanentRedirect('/casestudy/hertz.html','/casestudy/hertz-stop-before-you-get-there');

Route::permanentRedirect('/casestudy/mandp.html','/casestudy/m-and-p');

Route::permanentRedirect('/casestudy/clinique.html','/casestudy/clinique-imin');

Route::permanentRedirect('/casestudy/peugeot-ecomms.html','/casestudy/peugeot-buy-online');

Route::permanentRedirect('/casestudy/scotsman.html','/casestudy/the-scotsman-louder');

Route::permanentRedirect('/casestudy/nivea-double.html','/casestudy/nivea-men-the-double');

Route::permanentRedirect('/casestudy/evaluate2.html','/casestudy/evaluate-professionally-curious');

Route::permanentRedirect('/casestudy/flipside.html','/casestudy/white-oak-uk-flipside');

Route::permanentRedirect('/casestudy/jr.html','/casestudy/julius-rutherfoord');

Route::permanentRedirect('/casestudy/bartlett.html','/casestudy/bartlett-group');
// route people.html -> MiscController@people
// Route::get('people.html', 'Main\MiscController@people')->name('people');

// 301 redirect [PAGE].html -> /[PAGE]
/*
Route::get('/people.html', function () {
    return redirect('/people');
});
Route::get('/business.html', function () {
    return redirect('/business');
});
Route::get('/consumer.html', function () {
    return redirect('/consumer');
});
Route::get('/clients.html', function () {
    return redirect('/clients');
});
Route::get('/contact.html', function () {
    return redirect('/contact');
});
Route::get('/privacy.html', function () {
    return redirect('/privacy');
});
Route::get('/sitemap.html', function () {
    return redirect('/sitemap');
});
*/

Route::get('/', 'Main\HomeController@index')->name('home');

Route::get('/casestudy/{slug}', 'Main\WorkController@casestudy')->name('casestudy');

Route::get('/blog/', 'Main\BlogController@index')->name('all'); // all
Route::get('/blog/news', 'Main\BlogController@index')->name('news'); // only news
Route::get('/blog/launch', 'Main\BlogController@index')->name('launch'); // only launches
Route::get('/blog/stack-thinking', 'Main\BlogController@index')->name('stack-thinking'); // only thought pieces

Route::get('/blog/{slug}', 'Main\BlogController@post')->name('post');

// Route::get('/news', 'Main\NewsController@index')->name('news');
// Route::get('/news/{slug}/{id}', 'Main\NewsController@story')->name('story');

Route::get('/business', 'Main\MiscController@business')->name('business');
Route::get('/consumer', 'Main\MiscController@consumer')->name('consumer');
Route::get('/clients', 'Main\MiscController@clients')->name('clients');
Route::get('/people', 'Main\MiscController@people')->name('people');
Route::get('/contact', 'Main\MiscController@contact')->name('contact');
Route::get('/privacy', 'Main\MiscController@privacy')->name('privacy');
Route::get('/sitemap', 'Main\MiscController@sitemap')->name('sitemap');

/* default auth routes
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
*/

// Authentication Routes...
Auth::routes();

// -----------------------------------

// login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

// logut
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// -----------------------------------
// disable guest registration
// $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// $this->post('register', 'Auth\RegisterController@register');

// route /register to login controller
Route::get('register', 'Auth\LoginController@showLoginForm')->name('register');

// -----------------------------------

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
/* */

// -----------------------------------

Route::group(['prefix' => 'admin'], function() {

	Route::get('/', function() {
		return redirect('admin/summary');
	});
	Route::get('summary', 'Admin\Summary\SummaryController@index');

	// changeed from model binding
	// Route::get('users', 'Admin\Users\UserController@index');
	Route::get('users', [ 'as' => 'admin.users.index', 'uses' => 'Admin\Users\UserController@index']);
	Route::get('users/create', [ 'as' => 'admin.users.create', 'uses' => 'Admin\Users\UserController@create']);
	Route::post('users/store', [ 'as' => 'admin.users.store', 'uses' => 'Admin\Users\UserController@store']);
	Route::get('users/edit/{id}', [ 'as' => 'admin.users.edit', 'uses' => 'Admin\Users\UserController@edit']);
	Route::put('users/update/{id}', [ 'as' => 'admin.users.update', 'uses' => 'Admin\Users\UserController@update']);
	Route::delete('users/destroy/{id}', [ 'as' => 'admin.users.destroy', 'uses' => 'Admin\Users\UserController@destroy']);

	// blog admin routes
	Route::get('blog', 'Admin\Blog\BlogController@index');
	Route::post('blog/setfeatured', 'Admin\Blog\BlogController@setfeatured');
	Route::get('blog/create', 'Admin\Blog\BlogController@create');
	Route::post('blog/store', 'Admin\Blog\BlogController@store');
	Route::get('blog/edit/{id}', 'Admin\Blog\BlogController@edit');
	Route::put('blog/update/{id}', 'Admin\Blog\BlogController@update');
	Route::get('blog/destroy/{id}', 'Admin\Blog\BlogController@destroy');
	Route::delete('blog/destroy/{id}', 'Admin\Blog\BlogController@destroy');

	// Casestudies
	Route::get('casestudies', 'Admin\CaseStudy\CaseStudyController@index');
	Route::get('casestudies/create', 'Admin\CaseStudy\CaseStudyController@create');
	Route::post('casestudies/store', 'Admin\CaseStudy\CaseStudyController@store');
	Route::get('casestudies/edit/{id}', 'Admin\CaseStudy\CaseStudyController@edit');
	Route::put('casestudies/update/{id}', 'Admin\CaseStudy\CaseStudyController@update');
	Route::get('casestudies/destroy/{id}', 'Admin\CaseStudy\CaseStudyController@destroy');
	Route::delete('casestudies/destroy/{id}', 'Admin\CaseStudy\CaseStudyController@destroy');
	Route::get('casestudies/vt', 'Admin\CaseStudy\CaseStudyController@vt');
	Route::get('casestudies/fe-edit/{id}', 'Admin\CaseStudy\CaseStudyController@fe_edit');
	Route::get('casestudies/fe-create/{id}', 'Admin\CaseStudy\CaseStudyController@fe_create');

	// Clients
	Route::get('clients', 'Admin\Clients\ClientsController@index');
	Route::get('clients/create', 'Admin\Clients\ClientsController@create');
	Route::post('clients/store', 'Admin\Clients\ClientsController@store');
	Route::get('clients/edit/{id}', 'Admin\Clients\ClientsController@edit');
	Route::put('clients/update/{id}', 'Admin\Clients\ClientsController@update');
	Route::get('clients/destroy/{id}', 'Admin\Clients\ClientsController@destroy');
	Route::delete('clients/destroy/{id}', 'Admin\Clients\ClientsController@destroy');

	// People
	Route::get('people', 'Admin\People\PeopleController@index');
	Route::get('people/create', 'Admin\People\PeopleController@create');
	Route::post('people/store', 'Admin\People\PeopleController@store');
	Route::get('people/edit/{id}', 'Admin\People\PeopleController@edit');
	Route::put('people/update/{id}', 'Admin\People\PeopleController@update');
	Route::get('people/destroy/{id}', 'Admin\People\PeopleController@destroy');
	Route::delete('people/destroy/{id}', 'Admin\People\PeopleController@destroy');

	// admin ajax routes
	Route::post('ajax/test', 'Admin\AjaxController@test');

	Route::post('ajax/tag/auto', 'Admin\AjaxController@autoTag');
	Route::post('ajax/tag/select', 'Admin\AjaxController@selectTag');
	Route::post('ajax/tag/add', 'Admin\AjaxController@addTag');
	Route::post('ajax/tag/remove', 'Admin\AjaxController@removeTag');

	Route::post('ajax/link/add', 'Admin\AjaxController@addRelatedLink');
	Route::post('ajax/link/remove', 'Admin\AjaxController@removeRelatedLink');
	Route::post('ajax/link/update', 'Admin\AjaxController@updateRelatedLink');

	Route::post('ajax/client/add', 'Admin\AjaxController@addClient');
	Route::post('ajax/client/remove', 'Admin\AjaxController@removeClient');
	Route::post('ajax/client/update', 'Admin\AjaxController@updateClient');
	Route::post('ajax/client/order', 'Admin\AjaxController@orderClients');

	Route::post('ajax/module/getform', 'Admin\AjaxController@getModule');
	Route::post('ajax/casestudies/remove', 'Admin\AjaxController@removeCasestudy');
	Route::post('ajax/casestudies/order', 'Admin\AjaxController@orderCasestudies');
	Route::post('ajax/people/order', 'Admin\AjaxController@orderPeople');

	// Route::get('sitemap/refresh', 'Admin\Sitemap\SitemapController@refresh');

	/*
	Route::controllers([
		'auth'		=> 'Admin\Auth\AuthController',
		'password'	=> 'Admin\Auth\PasswordController',
	]);
	Route::resource('users', 'Admin\Users\UserController');
	*/

	/*
	// Media
	Route::get('media', 'Admin\Media\MediaController@index');
	Route::get('media/create', 'Admin\Media\MediaController@create');
	Route::post('media/store', 'Admin\Media\MediaController@store');
	Route::get('media/edit/{id}', 'Admin\Media\MediaController@edit');
	Route::put('media/update/{id}', 'Admin\Media\MediaController@update');
	Route::delete('media/destroy/{id}', 'Admin\Media\MediaController@destroy');
	*/

});

