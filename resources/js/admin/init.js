/**
 * init.js
 */
define(
	[
		 'jquery'
		,'common'
		,((pageViewJS === '') ? undefined : pageViewJS)
		,'backbone'
		,'unison-model'
		,'foundation'
	],
	
	function($, common, PageView, Backbone, UnisonModel, Foundation) {
		
		var me = Backbone.View.extend({

			initialize: function() {
				if ($("#noIE").css("position") != 'fixed') {
					window.UnisonModel = new UnisonModel();

					$(document).foundation();

					/**
					 * Initialise page actions if available
					 */
					if(typeof PageView !== 'undefined') {
						pageView = new PageView();
					}
				}
			}
			
		});
		
		return me;
	}
)