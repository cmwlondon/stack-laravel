/**
 * screen-script.js
 */
define(
	
	[
		 'jquery'
		,'backbone'
	],  
	
	function($, Backbone) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({

			initialize : function() {
				console.log('blog-index initialize function /public/js/admin/blog-index.min.js');

				zebra = this;

				$(".tagBox span.remove").bind("click", this.removeTag);
				$("#newTagAdd").bind("click", this.addTag);

				$("#newtag").bind("keyup",this.autoTag);
				// $("#newtag").bind("change",this.autoTag);
			}

			,lrvlAjax : function( parameters ) {
				// attach X_CSRF_TOKEN (header meta 'csrf-token') to ajax request
				var token = $('meta[name=csrf-token]').attr('content');

				$.ajaxSetup({
					"headers" : {
						"X-CSRF-TOKEN" : token
					}
				});

				// perform AJAX request
				// parameters.url ( string )
				// parameters.data ( object/array )
				// parameters.action ( function(<string> text ) )
				$.ajax({
					"type" : "POST",
					"url" : parameters.url,
					"data" : parameters.data,
					"dataType" : "text",
					"cache" : false,
					"success" : function (text) {
						parameters.action(text);
					},
					"complete" : function () {
					}
				});
			}
			/*
			Tags
			*/
			,removeTag : function(e) {
				e.preventDefault();
				var tag = $(this).parents('li').eq(0);

				// AJAX call to /admin/ajax/tag/remove
				// send: tag id
				// server:
				//  unlink tag

				zebra.lrvlAjax({
					"url" : "/admin/ajax/tag/remove",
					"data" : {
						"id" : tag.attr('data-tag-id'),
						"blog" : 8817
					},
					"action" : function(text) {
						tag.remove();
					}
				});
				
			}

			,addTag :  function(e) {
				e.preventDefault();

				// AJAX call to /admin/ajax/tag/add
				// send: contents of new tag text field
				// server:
				// 	insert new tag into 'tags', return new id
				//  link new tag to blog
				// client:
				//  add new HTML tag item to tag list
				//  clear new tag text field

				var newtag = $('#newtag').val();
				var tagDD = $('ul#tagdd');
				tagDD.empty();

				zebra.lrvlAjax({
					"url" : "/admin/ajax/tag/add",
					"data" : {
						"text" : newtag,
						"blog" : 8817
					},
					"action" : function(text) {
						var data = $.parseJSON(text);
						// data.newid
						// data.text

						if (data.status !== 3) {
							var tagBox = $('.tagBox');
							var nt = $('<li></li>').attr({"data-tag-id" : data.newid});
							var ns1 = $('<span></span>').addClass('text').text(data.text);
							var ns2 = $('<span></span>').text('X').addClass('remove').on('click',zebra.removeTag);
							nt.append(ns1).append(ns2);
							tagBox.append(nt);
						}
						$('#newtag').val('');
					}
				});
			}

			,autoTag :  function(e) {
				e.preventDefault();
				// AJAX call to /admin/ajax/tag/auto
				// send: contents of new tag text field
				// server:
				// 	get list of existing tags which match contents of new tag text field
				// client:
				//  build drop down list using returned tag data

				var newtag = $(this).val();
				var tagDD = $('ul#tagdd');

				zebra.lrvlAjax({
					"url" : "/admin/ajax/tag/auto",
					"data" : {
						"text" : newtag
					},
					"action" : function(text) {
						var data = $.parseJSON(text);

						var ddEntries = data.tags,
							ddCount = ddEntries.length,
							ddIndex = 0,
							ddx;

						tagDD.empty();

						if( ddCount > 0 ) {
							do {
								ddx = ddEntries[ddIndex];

								var nt = $('<li></li>').attr({"data-tag-id" : ddx.id}).text(ddx.text).on('click',zebra.selectTag);
								tagDD.append(nt);

								ddIndex++;
							} while (ddIndex < ddEntries.length)
						}

					}
				});
			}

			,selectTag :  function(e) {
				e.preventDefault();
				// AJAX call to /admin/ajax/tag/select
				// send: id of selected tag
				// server: 
				//  link tag to blog
				// client:
				//  close autocomplete dropdown
				//  clear new tag text field
				//  add tag entry to HTMl tag list

				var tagId = $(this).attr('data-tag-id');
				var tagText = $(this).text();

				zebra.lrvlAjax({
					"url" : "/admin/ajax/tag/select",
					"data" : {
						"id" : tagId,
						"blog" : 8817
					},
					"action" : function(text) {
						var data = $.parseJSON(text);
						// check for successful link
						if (data.status !== 1) {
							// add item to taglist
							var tagBox = $('.tagBox');
							var nt = $('<li></li>').attr({"data-tag-id" : tagId});
							var ns1 = $('<span></span>').addClass('text').text(tagText);
							var ns2 = $('<span></span>').text('X').addClass('remove').on('click',zebra.removeTag);
							nt.append(ns1).append(ns2);
							tagBox.append(nt);
						}
						// clear dd
						var tagDD = $('ul#tagdd');
						tagDD.empty();
					}
				});
			}

		});
		
		return PageView;
	}
);