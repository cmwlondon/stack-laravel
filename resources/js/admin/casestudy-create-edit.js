/*
case study creat/edit form
/admin/casestudies/create, /admin/casestudies/edit
/resources/view/admin/casestudies/create.blade.php, /resources/view/admin/casestudies/edit.blade.php
/app/Http/Controllers/Admin/CaseStudyController.php
/app/Http/Controllers/Admin/AjaxController.php
*/
define(
	
	[
		 'jquery'
		,'jquery-ui'
		,'backbone'
		,'ckeditor'
		,'ckfinder'
	],  
	
	function($, Jquery_UI, Backbone, ckEditor, CKFinder) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({
			
			initialize: function() {
				console.log('casestudy create/edit /resources/js/admin/casestudy-create-edit.js');

				$('#resub').val(1);

				this.fields = [];
				this.metadata = [];

				zebra = this;
				blankRegEx = /^[\s]*$/;

				this.casestudy = parseInt($('#id').val(),10);
				this.idListNode = $('#modules'); // 'a,b,c,..''
				this.fieldListNode = $('#modulesFieldList'); // [[<FIELDNAME>+suffix,...],...]
				this.metaNode = $('#modulesMeta'); // [{ "type" : STRING, "id" : INT, "suffix" : STRING, "fields" : [<FIELD>,..]},...]

				// ----------------------------
				// get highest module ID
				// get next module ID
				nextID = parseInt($('#moduleBox').attr('data-nextmoduleid'),10);

				// get existing module ids
				var idList = (zebra.idListNode.val() !== '') ? zebra.idListNode.val().split(',').map(function(id){ return parseInt(id,10); }) : [];
				
				var maxID = 0;
				idList.map(function(id) {
					if(id > maxID)
						maxID = parseInt(id,10);
				}, this);

				if (maxID >= nextID) {
					nextID = maxID + 1;

					$(".actionButton[data-action='add']").attr({"data-nextmodule" : nextID});
				}

				// bind events to buttons
				$('.imagepicker').bind('click',function(e){
					e.preventDefault();
					zebra.ckFinderImage( $(this) );
				});

				$('.videopicker').bind('click',function(e){
					e.preventDefault();
					zebra.ckFinderVieo( $(this) );
				});

				$('.colourPicker > li').bind('click',function(e){
					e.preventDefault();
					var item = $(this);
					var picker = item.parents('ul.colourPicker').eq(0);
					var field = picker.attr('data-field');
					var value = item.attr('data-value');

					picker.find('li.selected').removeClass('selected');
					item.addClass('selected');

					$('#' + field).val(value);
				});				

				$('.actionButton').bind('click',function(e){
					e.preventDefault();
					zebra.handleAction( $(this) );
				});			

			}

			,lrvlAjax : function( parameters ) {
				// attach X_CSRF_TOKEN (header meta 'csrf-token') to ajax request
				var token = $('meta[name=csrf-token]').attr('content');

				$.ajaxSetup({
					"headers" : {
						"X-CSRF-TOKEN" : token
					}
				});

				// perform AJAX request
				// parameters.url ( string )
				// parameters.data ( object/array )
				// parameters.action ( function(<string> text ) )
				$.ajax({
					"type" : "POST",
					"url" : parameters.url,
					"data" : parameters.data,
					"dataType" : "text",
					"cache" : false,
					"success" : function (text) {
						parameters.action(text);
					},
					"complete" : function () {
					}
				});
			}

			,handleAction : function(item) {
				var action = item.attr('data-action');

				switch( action ) {
					case 'add' : {
						zebra.addModule(item);
					} break;
					case 'remove' : {
						// add confirm alert
						// 'Do you want to remove this module?'
						var doRemove = window.confirm('Do you want to remove this module?');
						if ( doRemove ) {
							zebra.removeModule(item);
						}
							
					} break;
					case 'change' : {
						// add confirm alert
						// 'changing this module's type will remove all properties and settings. Do you want to continue?'
						var doChange = window.confirm('changing this module\'s type will remove all properties and settings. Do you want to continue?');
						if ( doChange ) {
							zebra.changeModule(item);
						}
							
					} break;
				}
			}

			,addModule : function(item) {
				// zebra.casestudy
				zebra.key = parseInt(item.attr('data-key'), 10);
				zebra.metakey = parseInt(item.attr('data-metakey'), 10);
				zebra.nextmoduleid = parseInt(item.attr('data-nextmodule'), 10);

				var newModule = $('select#newModuleSelector option:selected').val();

				// console.log("add new module type: %s id: %s", newModule, zebra.nextmoduleid);

				zebra.getModule({
					"action" : "getform",
					"casestudy" : zebra.casestudy, 	// INT
					"moduleType" : newModule, 		// STRING
					"module" : zebra.nextmoduleid, 	// INT

					"process" : function( html ){

						var newNode = $(html); 
						var suffix = newNode.data('suffix'); // field suffix for fields in new module
						var mf = newNode.data('fields'); // comma seperated string
						// var mf2 = ( mf !== '-' ) ? mf.split(',') : []; // ARRAY of STRING

						// bind events to buttons in new module
						zebra.bindEvents(newNode,newModule);

						// insert module into page
						$('#moduleBox').append(newNode);

						// update data attributes on 'add' button
						item.attr({"data-nextmodule" : (zebra.nextmoduleid + 1) });

						// STRING.split(',') ARRAY.join(',')
						// var idList = (zebra.idListNode.val() !== '') ? zebra.idListNode.val().split(',') : []; // ARRAY of STRING
						var idList = (zebra.idListNode.val() !== '') ? zebra.idListNode.val().split(',').map(function(id){ return parseInt(id,10); }) : []; // ARRAY of INT

						// JSON.parse(STRING) JSON.stringify(ARRAY)
						var fieldList = (zebra.fieldListNode.val() !== '') ? JSON.parse(zebra.fieldListNode.val()) : [];
						var meta = (zebra.metaNode.val() !== '' ) ? JSON.parse(zebra.metaNode.val()) : [];

						// ADD
						// Add new ID to end of idList
						idList.push( zebra.nextmoduleid );
						zebra.idListNode.val( idList.join(',') );

						// Add new [field+suffix] to end of fieldList
						// var mf3 = mf2.map(function(field){ return field + suffix; });
						fieldList.push( {
							"type" : newModule,
							"id" : zebra.nextmoduleid,
							"suffix" : suffix
							// "fields" : mf3
						} );
						zebra.fieldListNode.val( JSON.stringify(fieldList));

						// Add new meta {} to end of meta
						var newMeta = {
							"type" : newModule,
							"id" : zebra.nextmoduleid,
							"suffix" : suffix
							// "fields" : mf
						};
						meta.push(newMeta);
						zebra.metaNode.val( JSON.stringify(meta) );
					}
				});

			}

			,removeModule : function(item) {

				var parameters = {
					"suffix" : item.data('suffix'),
					"module" : parseInt(item.data('module'),10)
				};

				// console.log("remove module id: %s", parameters.module );

				// delete #module<suffix>
				$('#module' + parameters.suffix).remove();

				// STRING.split(',') / ARRAY.join(',')
				// var idList = (zebra.idListNode.val() !== '') ? zebra.idListNode.val().split(',') : []; // ARRAY of STRING
				var idList = (zebra.idListNode.val() !== '') ? zebra.idListNode.val().split(',').map(function(id){ return parseInt(id,10); }) : []; // ARRAY of INT

				// JSON.parse(STRING) / JSON.stringify(ARRAY)
				var fieldList = (zebra.fieldListNode.val() !== '') ? JSON.parse(zebra.fieldListNode.val()) : [];
				var meta = (zebra.metaNode.val() !== '' ) ? JSON.parse(zebra.metaNode.val()) : [];

				// REMOVE
				// REMOVE idList, fieldList
				var key = idList.indexOf(parameters.module);
				idList.splice(key,1);
				fieldList.splice(key,1);
				zebra.idListNode.val( idList.join(',') );
				zebra.fieldListNode.val( (fieldList.length > 0) ? JSON.stringify(fieldList) : '')

				// REMOVE meta
				var metakey = -1;
				meta.map(function(item,index){
					if ( item.id === parameters.module )
						metakey = index;
				});
				if ( metakey > -1) {
					meta.splice(metakey,1);
					zebra.metaNode.val( JSON.stringify(meta) );
				}

			}

			,changeModule : function(item) {
				var parameters = {
					"casestudy" : zebra.casestudy,
					"module" : parseInt(item.data('module'),10),
					"suffix" : item.data('suffix'),
					"current" : item.data('current'),
					"newModuleType" : $('select#moduleSelector' + parseInt(item.data('module'),10) + ' option:selected').val()
				};

				// console.log("change id: %s", parameters.module );

				if( parameters.current !== parameters.newModuleType) {

					zebra.getModule({
						"action" : "getform",
						"casestudy" : parameters.casestudy,
						"moduleType" : parameters.newModuleType,
						"module" : parameters.module,
						"process" : function( html ){

							console.log("moduleType: '%s'" , parameters.newModuleType);

							var newNode = $(html); 
							var suffix = newNode.data('suffix');
							// var mf = newNode.data('fields');
							// var mf2 = ( mf !== '-' ) ? mf.split(',') : []; // ARRAY of STRING

							// update fieldlist and metadata

							zebra.bindEvents(newNode, parameters.newModuleType);

							// var moduleTarget = $('#moduleTarget' + parameters.suffix);
							var moduleTarget = $('#module' + parameters.suffix);
							moduleTarget.replaceWith(newNode);

							// update 'change module' current value
							item.attr({"data-current" : parameters.selected });

							// STRING.split(',') / ARRAY.join(',')
							// var idList = (zebra.idListNode.val() !== '') ? zebra.idListNode.val().split(',') : []; // ARRAY of STRING
							var idList = (zebra.idListNode.val() !== '') ? zebra.idListNode.val().split(',').map(function(id){ return parseInt(id,10); }) : []; // ARRAY of INT

							// JSON.parse(STRING) / JSON.stringify(ARRAY)
							var fieldList = (zebra.fieldListNode.val() !== '') ? JSON.parse(zebra.fieldListNode.val()) : [];
							var meta = (zebra.metaNode.val() !== '' ) ? JSON.parse(zebra.metaNode.val()) : [];

							// CHANGE
							// CHANGE fieldList
							var key = idList.indexOf(parameters.module);
							// var mf3 = mf2.map(function(field){ return field + suffix; });
							fieldList[key] = {
								"type" : parameters.newModuleType,
								"id" : parameters.module,
								"suffix" : suffix
								// "fields" : mf3
							};
							zebra.fieldListNode.val( (fieldList.length > 0) ? JSON.stringify(fieldList) : '')

							// CHANGE meta
							var metakey = -1;
							meta.map(function(item,index){
								if ( item.id === parameters.module )
									metakey = index;
							});
							
							if ( metakey > -1) {
								var newMeta = {
									"type" : parameters.newModuleType,
									"id" : parameters.module,
									"suffix" : suffix
									// "fields" : mf
								};
								meta[metakey] = newMeta;
								zebra.metaNode.val( JSON.stringify(meta) );
							}
						}
					});

					
				}
			}

			,getModule : function( parameters ) {
				// get HTML for module

				zebra.lrvlAjax({
					"url" : "/admin/ajax/module/" + parameters.action,
					"data" : {
						"casestudy" : parameters.casestudy,
						"moduleType" : parameters.moduleType,
						"module" : parameters.module,
						"key" : parameters.key,
						"metakey" : parameters.metakey
					},
					"action" : function(html) {
						// HTMl response
						parameters.process( html );
					}
				});
			}

			,bindEvents : function( newNode, newModule ) {
				// bind 'change' and 'remove' buttons
				newNode.find('.actionButton').each(function(){
					$(this).bind('click',function(e){
						e.preventDefault();
						zebra.handleAction( $(this) );
					});				
				});

				// find image fields and bind ckfinder to these
				switch( newModule ){
					case "brochure-video" : {
						console.log(newModule);
					} break;

					case "ev2header" : {
						console.log(newModule);
					} break;

					case "evaluate2video" : {
						console.log(newModule);
					} break;

					case "copy" : {
						console.log(newModule);
						// bind ckedit to 'html_N_N'
					} break;

					case "inline-image" : {
						console.log(newModule);
						// bind ckfinder to 'image_N_N'
						// #ckft_inline{{ $moduleData['suffix'] }}
						// need to set unique field id AND unique preview element id
						// field: '#image'.$moduleData['suffix']
						// preview: div.'#previewImage'.$moduleData['suffix'] img.attr({'src' : $moduleData['image']})

						newNode.find('.imagepicker').bind('click',function(e){
							e.preventDefault();

							zebra.ckFinderImage( $(this) );

						});

					} break;

					case "image-grid" : {
						console.log(newModule);
						// bind ckfinder to 'img1_N_N','img2_N_N','img3_N_N','img4_N_N'
						// #ckft_grid1{{ $moduleData['suffix'] }}
						// #ckft_grid2{{ $moduleData['suffix'] }}
						// #ckft_grid3{{ $moduleData['suffix'] }}
						// #ckft_grid4{{ $moduleData['suffix'] }}
						// need to set unique field id AND unique preview element id
						newNode.find('.imagepicker').bind('click',function(e){
							e.preventDefault();

							zebra.ckFinderImage( $(this) );

						});

					} break;

					case "video-manual" : {
						console.log(newModule);
						// bind ckfinder to 'poster_N_N','mp4_N_N','webm_N_N','ogv_N_N'
						// #ckft_poster{{ $moduleData['suffix'] }}
						// #ckft_mp4{{ $moduleData['suffix'] }}
						// #ckft_webm{{ $moduleData['suffix'] }}
						// #ckft_ogv{{ $moduleData['suffix'] }}
						// need to set unique field id AND unique preview element id
						newNode.find('.imagepicker').bind('click',function(e){
							e.preventDefault();

							zebra.ckFinderImage( $(this) );

						});

						newNode.find('.videopicker').bind('click',function(e){
							e.preventDefault();

							zebra.ckFinderVideo( $(this) );

						});
					} break;

					case "autoplayvideo" : {
						console.log(newModule);
						// bind ckfinder to 'poster_N_N','mp4_N_N','webm_N_N','ogv_N_N'
						// #ckft_poster{{ $moduleData['suffix'] }}
						// #ckft_mp4{{ $moduleData['suffix'] }}
						// #ckft_webm{{ $moduleData['suffix'] }}
						// #ckft_ogv{{ $moduleData['suffix'] }}
						// need to set unique field id AND unique preview element id
						newNode.find('.imagepicker').bind('click',function(e){
							e.preventDefault();

							zebra.ckFinderImage( $(this) );

						});

						newNode.find('.videopicker').bind('click',function(e){
							e.preventDefault();

							zebra.ckFinderVideo( $(this) );

						});
					} break;

					case "reveal-image" : {
						console.log(newModule);
						// bind ckfinder to 'image_N_N'
						// #ckft_reveal{{ $moduleData['suffix'] }}
						// need to set unique field id AND unique preview element id
						newNode.find('.imagepicker').bind('click',function(e){
							e.preventDefault();

							zebra.ckFinderImage( $(this) );

						});

					} break;
				}
			}

			,ckFinderImage : function( item ) {
				var previewNode = item.data('preview');
				var fieldNode = item.data('field');

				CKFinder.modal({
					chooseFiles: true,
					width: 800,
					height: 600,
					onInit: function( finder ) {

						finder.on( 'files:choose', function( evt ) {
							var file = evt.data.files.first();
							var output = document.getElementById( fieldNode );
							var filePath = file.getUrl();
							output.value = filePath;

							// update logo image preview
							$( '#' + previewNode ).empty().append($('<img></img>').attr({ "src" : filePath }) );
						} );

						finder.on( 'file:choose:resizedImage', function( evt ) {
							var output = document.getElementById( fieldNode );
							output.value = evt.data.resizedUrl;
						} );
					}
			    });

			}

			,ckFinderVideo : function( item ) {
				var previewNode = item.data('preview');
				var fieldNode = item.data('field');

				console.log("ckFinderVideo previewNode: %s fieldNode: %s", previewNode, fieldNode);
			}
		})

		return PageView;
	}
)
