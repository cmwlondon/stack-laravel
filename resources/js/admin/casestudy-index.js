/*
client logos admin landing page
/admin/clients/
/resources/view/admin/Clients/main.blade.php
/app/Http/Controllers/Admin/ClientsController.php
/app/Http/Controllers/Admin/AjaxController.php
*/
define(
	
	[
		 'jquery'
		,'jquery-ui'
		,'backbone'
		,'ckeditor'
		,'ckfinder'
	],  
	
	function($, Jquery_UI, Backbone, ckEditor, CKFinder) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({
			
			initialize: function() {
				
				console.log('casestudy index/order /resources/js/admin/casestudy-index.js');

				zebra = this;
				blankRegEx = /^[\s]*$/;

				$( ".caseStudyIndexGrid" )
				.sortable({
					"placeholder" : "item-placeholder",
					"stop" : function(e,ui) {
						zebra.IndexActiveItems( $(this) );
					}
				});

				// casestudy delete
				$('.action.delete').bind('click',function(e){
					e.preventDefault();

					var item = $(this).parents('tr').eq(0);
					var id = item.attr('data-id');
					var title = item.find('td.title_column').eq(0).html();
					var category = item.attr('data-category');

					var c = confirm("do you want to delete the client '" + title + "'");
					if (c) {
						zebra.removeCasestudy({ "row" : item, "id" : id, "title" : title, "category" : category});
					}
				});

			}
			,lrvlAjax : function( parameters ) {
				// attach X_CSRF_TOKEN (header meta 'csrf-token') to ajax request
				var token = $('meta[name=csrf-token]').attr('content');

				$.ajaxSetup({
					"headers" : {
						"X-CSRF-TOKEN" : token
					}
				});

				// perform AJAX request
				// parameters.url ( string )
				// parameters.data ( object/array )
				// parameters.action ( function(<string> text ) )
				$.ajax({
					"type" : "POST",
					"url" : parameters.url,
					"data" : parameters.data,
					"dataType" : "text",
					"cache" : false,
					"success" : function (text) {
						parameters.action(text);
					},
					"complete" : function () {
					}
				});
			}

			,updateOrder : function(parameters) {
				console.log('updateOrder');
				console.log(parameters);

				zebra.lrvlAjax({
					"url" : "/admin/ajax/casestudies/order",
					"data" : {
						"category" : parameters.category,
						"order" : parameters.order
					},
					"action" : function(text) {
						var data = $.parseJSON(text);
						// new blog entry, add new id to hiden tag id list 
						console.log(data);
					}
				});

			}

			,removeCasestudy : function(parameters) {

				// parameters.row, parameters.id, parameters.title, parameters.category

				zebra.lrvlAjax({
					"url" : "/admin/ajax/casestudies/remove",
					"data" : {
						"id" : parameters.id,
						"category" : parameters.category
					},
					"action" : function(text) {
						var data = $.parseJSON(text);
						console.log(data);
						// remove casestudy from HTML list and sorter widget
						parameters.row.remove();
						$('.caseStudyIndexGrid li.thumbnail[data-id="' + data.id + '"]').remove();

						// update data-order attribute
						// <ul class="caseStudyIndexGrid" data-category="business" data-order="{{ $businessOrder }}">
						console.log( 'ul.caseStudyIndexGrid[data-order="' + data.category + '"]' );
						$( 'ul.caseStudyIndexGrid[data-category="' + data.category + '"]' ).attr({ "data-order" : data.order });
					}
				});
			}

			,IndexActiveItems : function(list) {
				// reindex items once sort/reorder operation has finished
				var order = [];

				list.find( "li" ).each(function(i){
					order.push($(this).attr('data-id'));
				})

				// store new order in data-order attribute
				list.attr({"data-order" : order.join(',')});

				// store order in database via AJAX call
				this.updateOrder({
					"order" : order.join(','),
					"category" : list.attr('data-category')
				});
			}

		});
		
		return PageView;
	}
);