/*
client logos admin landing page
/admin/clients/
/resources/view/admin/Clients/main.blade.php
/app/Http/Controllers/Admin/ClientsController.php
/app/Http/Controllers/Admin/AjaxController.php
*/
define(
	
	[
		 'jquery'
		,'jquery-ui'
		,'backbone'
		,'ckeditor'
		,'ckfinder'
	],  
	
	function($, Jquery_UI, Backbone, ckEditor, CKFinder) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({
			
			initialize: function() {
				console.log('clients index/order /resources/js/admin/clients-index.js');

				zebra = this;
				blankRegEx = /^[\s]*$/;

				// drag logos to change order
				$( "#clientSorter" ).sortable({
					/*
					"update" : function(e,ui) {
						console.log('sort update');
					},
					*/
					"stop" : function(e,ui) {
						console.log('sort stop');

						// index items
						var order = [];
						$( "#clientSorter > li" ).each(function(i){
							order.push($(this).attr('data-id'));
						})
						$( "#clientSorter" ).attr({"data-order" : order.join(',')});
						zebra.updateOrder(order.join(','));
					}
				});

				// client logo delete
				$('.controls .delete').bind('click',function(e){
					var item = $(this).parents('li.item').eq(0);
					var title = item.attr('title');

					var c = confirm("do you want to delete the client '" + title + "'");
					return c;
				});

				/*
				$('.sort').on('click',function(e) {
					e.preventDefault();

					var action = $(this).attr('data-action');

					switch( action ) {
						case 'alpha' : {
							console.log('sort alphabetical');
							var clist = zebra.alphaSort();
						} break;
						case 'id' : {
							console.log('sort id');
							var clist = zebra.idSort();
						} break;
					}
				});
				*/
			}
			,lrvlAjax : function( parameters ) {
				// attach X_CSRF_TOKEN (header meta 'csrf-token') to ajax request
				var token = $('meta[name=csrf-token]').attr('content');

				$.ajaxSetup({
					"headers" : {
						"X-CSRF-TOKEN" : token
					}
				});

				// perform AJAX request
				// parameters.url ( string )
				// parameters.data ( object/array )
				// parameters.action ( function(<string> text ) )
				$.ajax({
					"type" : "POST",
					"url" : parameters.url,
					"data" : parameters.data,
					"dataType" : "text",
					"cache" : false,
					"success" : function (text) {
						parameters.action(text);
					},
					"complete" : function () {
					}
				});
			}
			,updateOrder : function(order) {

				zebra.lrvlAjax({
					"url" : "/admin/ajax/client/order",
					"data" : {
						"order" : order
					},
					"action" : function(text) {
						var data = $.parseJSON(text);
						// new blog entry, add new id to hiden tag id list 
						console.log(data);
					}
				});

			}
			,alphaSort : function() {

				zebra.lrvlAjax({
					"url" : "/admin/ajax/client/alpha",
					"data" : {},
					"action" : function(text) {
						var data = $.parseJSON(text);
						// new blog entry, add new id to hiden tag id list 
						console.log(data);
						return data.clients;
					}
				});

			}
			,idSort : function() {

				zebra.lrvlAjax({
					"url" : "/admin/ajax/client/id",
					"data" : {},
					"action" : function(text) {
						var data = $.parseJSON(text);
						// new blog entry, add new id to hiden tag id list 
						console.log(data);
						return data.clients;
					}
				});

			}
			,addClient : function() {
				// /admin/ajax/client/add
			}
			,removeClient : function() {
				// /admin/ajax/client/remove
			}
			,updateClient : function() {
				// /admin/ajax/client/update
			}
		});
		
		return PageView;
	}
);