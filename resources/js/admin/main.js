/**
 * main.js
 * contents of main.js with extra configuration
 */
require.config({
	
	baseUrl: "/js/",
	
	urlArgs: "bust=" + (new Date()).getTime(),	// halt caching of .js files for development
	
	waitSeconds: 0, 							// disable script loading timeout 
    
	paths: {
		/**
		 * Libraries
		 */
        //'jquery'		 		: 'libs/jquery/jquery-1.11.3.min'
        'jquery'		 		: 'libs/jquery/jquery-2.2.4.min'
        ,'jquery-ui'		 	: 'libs/jquery-ui/jquery-ui.min'
        ,'json2' 				: 'libs/json2/json2-min'
        ,'underscore'	 		: 'libs/underscore/underscore-min'
        ,'backbone'				: 'libs/backbone/backbone-min'
        ,'handlebars'			: 'libs/handlebars/handlebars-v3.0.3'

        /* Foundation */
        ,'foundation'			: 'libs/foundation/foundation.min'


		/* Unison */
		,'unison'				: 'libs/unison/unison.min'

		/* hammer */
		,'hammer'				: 'libs/hammer/hammer.min'

		/* fancybox */
		,'fancybox'				: 'libs/fancybox/jquery.fancybox.pack'
		,'fancybox-media'		: 'libs/fancybox/helpers/jquery.fancybox-media'
		
		/* Video player engine */
		,'jplayer'				: 'libs/jplayer/jquery.jplayer.min'

		/* CKEditor */

		,'ckeditor'             : 'libs/ckeditor/ckeditor'
		,'ckfinder'             : '../ckfinder/ckfinder'

		/**
		 * Application Logic
		 */
		,'common'				: 'admin/common.min'
		,'init'					: 'admin/init.min'
		,'unison-model'			: 'admin/unison-model.min'
    },

	/**
	 * Use shim for non AMD (Asynchronous Module Definition) compatible scripts
	 */
	shim: {        
		'backbone': {
             deps	 : ['underscore', 'jquery']
            ,exports : 'Backbone'
        },

        'foundation': {
             deps	 : ['jquery']
            ,exports : 'foundation'
        },
		
        'underscore': {
            exports : '_'
        },

        'unison': {
            exports : 'Unison'
        },

        'fancybox': {
         	deps 	: ['jquery']
            ,exports : 'FancyBox'
        },

        'fancybox-media': {
         	deps 	: ['jquery','fancybox']
            ,exports : 'FancyBoxMedia'
        },

        'ckeditor': {
            deps    : ['jquery']
            ,exports : 'ckEditor'
        },

        'ckfinder': {
            deps    : ['jquery','ckeditor']
            ,exports : 'CKFinder'
        },
    }
});

/**
 * Load the init module
 */
require(['init'],
		
	function(App) {
		trace("here");
		window._app	= new App();
	}
);