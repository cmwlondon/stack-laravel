/*
client logos creat/edit form
/admin/clients/create, /admin/clients/create
/resources/view/admin/Clients/create.blade.php, /resources/view/admin/Clients/create.blade.php
/app/Http/Controllers/Admin/ClientsController.php
/app/Http/Controllers/Admin/AjaxController.php
*/
define(
	
	[
		 'jquery'
		,'jquery-ui'
		,'backbone'
		,'ckeditor'
		,'ckfinder'
	],  
	
	function($, Jquery_UI, Backbone, ckEditor, CKFinder) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({
			
			initialize: function() {
				console.log('clients create/edit /resources/js/admin/clients-create-edit.js');

				zebra = this;
				blankRegEx = /^[\s]*$/;

				// bind ckfinder to #logoTrigger
				// insert URL into #logo
				function selectFileWithCKFinder( elementId ) {
					console.log("selectFileWithCKFinder id:%s", elementId);

					CKFinder.modal({
						chooseFiles: true,
						width: 800,
						height: 600,
						onInit: function( finder ) {

							finder.on( 'files:choose', function( evt ) {
								var file = evt.data.files.first();
								var output = document.getElementById( elementId );
								var filePath = file.getUrl();
								output.value = filePath;

								// update logo image preview
								$('#previewLogo').empty().append($('<img></img>').attr({ "src" : filePath }) );
							} );

							finder.on( 'file:choose:resizedImage', function( evt ) {
								var output = document.getElementById( elementId );
								output.value = evt.data.resizedUrl;
							} );
						}
				    });
					
				};

				$('#logoTrigger').bind('click',function(e){
					e.preventDefault();
					selectFileWithCKFinder( 'logo' );
				});

			}
		})

		return PageView;
	}
)
