/*
people index
/admin/people/
/resources/view/admin/People/main.blade.php
/app/Http/Controllers/Admin/PeopleController.php
/app/Http/Controllers/Admin/AjaxController.php
*/
define(
	
	[
		 'jquery'
		,'jquery-ui'
		,'backbone'
		,'ckeditor'
		,'ckfinder'
	],  
	
	function($, Jquery_UI, Backbone, ckEditor, CKFinder) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({
			
			initialize: function() {
				console.log('people main /resources/js/admin/people-index.js');

				zebra = this;
				blankRegEx = /^[\s]*$/;

				// person delete
				$('.controls .delete').bind('click',function(e){
					var item = $(this).parents('li.item').eq(0);
					var title = item.attr('title');

					var c = confirm("do you want to delete the person '" + title + "'");
					return c;
				});
			}
		});

		return PageView;
	}
)