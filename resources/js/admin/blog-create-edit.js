/**
 * blog create / edit
 */
define(
	
	[
		 'jquery'
		,'jquery-ui'
		,'backbone'
		,'ckeditor'
		,'ckfinder'
	],  
	
	function($, Jquery_UI, Backbone, ckEditor, CKFinder) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({
			
			initialize: function() {
				console.log('blog create/edit /resources/js/admin/blog-create-edit.js');

				zebra = this;
				blankRegEx = /^[\s]*$/;
				blogID = $('#id').val();
				newBlogMode = ( blogID === '0' ) ? 1 : 0; // true, new blog, false, existing blog
				console.log("init: new blog mode %s", ( newBlogMode === 1 ) ? 'yes' : 'no' );

				// store related links in internal array
				// copy to metadata field '#rlink_meta'
				relatedLinks = [];
				relatedLinkCount = 0;

				var editor1 = CKEDITOR.replace('body',{
				    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				    filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				    height: '500px',
				    extraPlugins:'video',
				    extraAllowedContent: 'video[*]{*};source[*]{*}'
				});

				$("#title").bind("keyup",this.updateSlug);
				$("#title").bind("change",this.updateSlug);

				/*
				$(".bReplaceThumb").bind("click", this.replaceImageThumb);
				$(".bReplace").bind("click", this.replaceImage);

				$(".bRemoveThumb").bind("click", this.removeImageThumb);
				$(".bRemove").bind("click", this.removeImage);

				$("#gallerySelect").bind("click", this.selectGallery);
				$("#gallerySelect").bind("change", this.selectGallery);
				*/

				// add ckfinder modal/popup for thumb and main post images
				$('#ckfinder-popup-1').bind('click',function(e){
					e.preventDefault();
					selectFileWithCKFinder( 'thumb' );
				});

				$('#ckfinder-popup-2').bind('click',function(e){
					e.preventDefault();
					selectFileWithCKFinder( 'image' );
				});

				function selectFileWithCKFinder( elementId ) {
					console.log("selectFileWithCKFinder id:%s", elementId);

					CKFinder.modal({
						chooseFiles: true,
						width: 800,
						height: 600,
						onInit: function( finder ) {

							finder.on( 'files:choose', function( evt ) {
								var file = evt.data.files.first();
								var output = document.getElementById( elementId );
								var filePath = file.getUrl();
								output.value = filePath;

								switch( elementId ) {
									case "image" : {
										previewTarget = '#previewImage';
									} break;
									case "thumb" : {
										previewTarget = '#previewThumb';
									} break;
									case "rlink-thumb" : {
										previewTarget = '#rlinkthumb';
									} break;
								}

								$(previewTarget).empty().append($('<img></img>').attr({ "src" : filePath }) );
							} );

							finder.on( 'file:choose:resizedImage', function( evt ) {
								var output = document.getElementById( elementId );
								output.value = evt.data.resizedUrl;
							} );
						}
				    });
					
				};

				// tags
				if ( newBlogMode === 1 && !blankRegEx.test($('#image').val()) ) {
					$('div#previewImage').append($('<img></img>').attr({ "src" : $('#image').val() }));
				}

				if ( newBlogMode === 1 && !blankRegEx.test($('#thumb').val()) ) {
					$('div#previewThumb').append($('<img></img>').attr({ "src" : $('#thumb').val() }));
				}

				$(".tagBox span.remove").bind("click", this.removeTag);
				$("#newTagAdd").bind("click", this.addTag);

				$("#newtag").bind("keyup",this.autoTag);
				// $("#newtag").bind("change",this.autoTag);

				// need to build tag list in 'create blog' + failed submission using javascript
				// make ajax call with list of IDs to get relevant tag text
				if ( newBlogMode === 1 && $("#newtags").val() !== '' ) {
					this.buildTags($("#newtags").val());
				}

				// bind ckedit to rlink title/caption field
				var editor2 = CKEDITOR.replace('rlink-title',{
					// turn off automatic <p> tags, 'enter' key inserts linebreaks instead
					"enterMode" : CKEDITOR.ENTER_BR,

					// configure toolbar items
					"toolbar" : [
						{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
						{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
						{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
						'/',
						{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
					],

					// Toolbar groups configuration.
					"toolbarGroups" : [
						{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
						{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
						{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
						'/',
						{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] }
					],
					// set height of editor panel
				    height: '100px',
				});

				// bind ckfinder to rlink thumbnail field
				$('#selectRlinkThumb').bind('click',function(e){
					e.preventDefault();
					selectFileWithCKFinder( 'rlink-thumb' );
				});

				// edit rlink - copies data from list into editor fields
				$('.editRlink').bind('click',function(e){
					e.preventDefault();
					zebra.editRlink($(this));
				});

				// delete rlink
				$('.deleteRlink').bind('click',function(e){
					e.preventDefault();
					zebra.deleteRlink($(this));
				});

				// add new link to link list based on content of editor fields
				$('#addRlink').bind('click',function(e){
					e.preventDefault();
					zebra.addRlink();
				});


				$('#updateRlink').bind('click',function(e){
					e.preventDefault();
					zebra.updateRlink();
				});

				$('#resetRlink').bind('click',function(e){
					e.preventDefault();
					zebra.resetRlink();
				});

				// read related links items in blog edit page
				relatedLinkCount = $('#rlinkList > li').length;
				console.log("relatedLinkCount %s", relatedLinkCount);

				// populate related links list from metadata for 'create blog' form
				var relatedLinksMetaData = $('#rlink_meta').val();
				if ( !blankRegEx.test(relatedLinksMetaData) && relatedLinksMetaData !== '[]') {
					// field is not blank or empty array, convert to array
					var metaDataItems = JSON.parse(relatedLinksMetaData);
					relatedLinkCount = metaDataItems.length;
					if ( relatedLinkCount === 0 ) {
						$('.rlw').addClass('nolinks');
					} else {
						$('.rlw').removeClass('nolinks');

						// build related links list
						metaDataItems.map(function(link){
							relatedLinks.push({
								"id" : parseInt(link.id,10),
								"title" : link.title,
								"url" : link.url,
								"external" : link.external,
								"thumbnail" : link.thumbnail
							});

							this.buildRlink({
								"id" : link.id,
								"title" : link.title,
								"url" : link.url,
								"external" : link.external,
								"thumbnail" : link.thumbnail
							});

						}, this);
					}
				}
			}

			,updateSlug: function(e) {
				var newslug = slug($("#title").val());
				$('#slug').val(newslug);
			}

			,lrvlAjax : function( parameters ) {
				// attach X_CSRF_TOKEN (header meta 'csrf-token') to ajax request
				var token = $('meta[name=csrf-token]').attr('content');

				$.ajaxSetup({
					"headers" : {
						"X-CSRF-TOKEN" : token
					}
				});

				// perform AJAX request
				// parameters.url ( string )
				// parameters.data ( object/array )
				// parameters.action ( function(<string> text ) )
				$.ajax({
					"type" : "POST",
					"url" : parameters.url,
					"data" : parameters.data,
					"dataType" : "text",
					"cache" : false,
					"success" : function (text) {
						parameters.action(text);
					},
					"complete" : function () {
					}
				});
			}

			/* Tags */
			,removeTag : function(e) {
				e.preventDefault();
				var tag = $(this).parents('li').eq(0);

				// AJAX call to /admin/ajax/tag/remove
				// send: tag id
				// server:
				//  unlink tag

				zebra.lrvlAjax({
					"url" : "/admin/ajax/tag/remove",
					"data" : {
						"id" : tag.attr('data-tag-id'),
						"blog" : blogID,
						"mode" : newBlogMode
					},
					"action" : function(text) {
						tag.remove();

						var data = $.parseJSON(text);
						// new blog entry, add new id to hiden tag id list 
						zebra.removeNewTags(data.id);
					}
				});
				
			}

			,addTag :  function(e) {
				e.preventDefault();

				// AJAX call to /admin/ajax/tag/add
				// send: contents of new tag text field
				// server:
				// 	insert new tag into 'tags', return new id
				//  link new tag to blog
				// client:
				//  add new HTML tag item to tag list
				//  clear new tag text field

				var newtag = $('#newtag').val();
				var tagDD = $('ul#tagdd');
				
				tagDD.empty();

				if ( !blankRegEx.test(newtag) ) {
					zebra.lrvlAjax({
						"url" : "/admin/ajax/tag/add",
						"data" : {
							"text" : newtag,
							"blog" : blogID,
							"mode" : newBlogMode
						},
						"action" : function(text) {
							var data = $.parseJSON(text);
							// data.newid
							// data.text

							if (data.status !== 3) {
								zebra.buildTag(data.newid, data.text);

								// new blog entry, add new id to hiden tag id list 
								zebra.addNewTags(data.newid, data.text);
							}
							$('#newtag').val('');
						}
					});
				}
			}

			,autoTag :  function(e) {
				e.preventDefault();
				// AJAX call to /admin/ajax/tag/auto
				// send: contents of new tag text field
				// server:
				// 	get list of existing tags which match contents of new tag text field
				// client:
				//  build drop down list using returned tag data

				var newtag = $(this).val();
				var tagDD = $('ul#tagdd');

				zebra.lrvlAjax({
					"url" : "/admin/ajax/tag/auto",
					"data" : {
						"text" : newtag
					},
					"action" : function(text) {
						var data = $.parseJSON(text);

						var ddEntries = data.tags,
							ddCount = ddEntries.length,
							ddIndex = 0,
							ddx;

						tagDD.empty();

						if( ddCount > 0 ) {
							do {
								ddx = ddEntries[ddIndex];

								var nt = $('<li></li>').attr({"data-tag-id" : ddx.id}).text(ddx.text).on('click',zebra.selectTag);
								tagDD.append(nt);

								ddIndex++;
							} while (ddIndex < ddEntries.length)
						}

					}
				});
			}

			,selectTag :  function(e) {
				e.preventDefault();
				// AJAX call to /admin/ajax/tag/select
				// send: id of selected tag
				// server: 
				//  link tag to blog
				// client:
				//  close autocomplete dropdown
				//  clear new tag text field
				//  add tag entry to HTMl tag list

				var tagId = $(this).attr('data-tag-id');
				var tagText = $(this).text();

				zebra.lrvlAjax({
					"url" : "/admin/ajax/tag/select",
					"data" : {
						"id" : tagId,
						"blog" : blogID,
						"mode" : newBlogMode
					},
					"action" : function(text) {
						var data = $.parseJSON(text);
						// check for successful link
						if (data.status !== 1) {
							// add item to taglist
							zebra.buildTag(tagId, tagText);
							// new blog entry, add new id to hiden tag id list 
							zebra.addNewTags(tagId,tagText);
						}
						// clear dd
						var tagDD = $('ul#tagdd');
						tagDD.empty();
					}
				});
			}

			// new blog: store tag id/text in hidden field
			,addNewTags :  function(id,text) {
				if( newBlogMode === 1 ) {
					var newtags = $('#newtags').val();

					if (newtags.length > 0) {
						var nta = $.parseJSON(newtags)
					} else {
						nta = {"items":[]};
					}
					
					nta.items.push({
						"id" : String(id),
						"text" : String(text)
					});

					$('#newtags').val(JSON.stringify(nta));
				}
			}

			,removeNewTags :  function(id) {
				if( newBlogMode === 1 ) {
					var newtags = $('#newtags').val();
					var tagData = $.parseJSON(newtags);

					for( var i = 0; i < tagData.items.length; i++){ 
						if ( tagData.items[i].id === id) {
							console.log('match');
							tagData.items.splice(i, 1); 
						}
					}
					$('#newtags').val(JSON.stringify(tagData));
				}
			}

			,buildTags : function( tagIDs ) {
				var tagData = $.parseJSON(tagIDs);
				tagData.items.map(function(tag){
					console.log(tag.id,tag.text);
					zebra.buildTag(tag.id,tag.text);
				});
			}

			,buildTag : function(id,text) {
				var tagBox = $('.tagBox');
				var nt = $('<li></li>').attr({"data-tag-id" : id});
				var ns1 = $('<span></span>').addClass('text').text(text);
				var ns2 = $('<span></span>').text('X').addClass('remove').on('click',zebra.removeTag);
				nt.append(ns1).append(ns2);
				tagBox.append(nt);
			}

			/* Related Links */
			,editRlink : function(trigger){
				console.log('edit related link');
				// copy data from selected link into the link editor fields
				// may need to update the title/caption field ckedit instance

				var item = trigger.parents('li.link').eq(0);
				var id = item.attr('data-linkid');
				var title = item.find('p').html();
				var url = item.attr('data-url');
				var external = item.attr('data-external');
				var thumbnail = trigger.parents('li.link').eq(0).attr('data-thumbnail');

				CKEDITOR.instances['rlink-title'].setData( title );
				$('#rlink-url').val( url );
				$('#rlink-thumb').val( thumbnail );
				
				switch( external ) {
					case "0" : {
						$('#external-0').prop('checked', true);
						$('#external-1').prop('checked', false);
					} break;
					case "1" : {
						$('#external-0').prop('checked', false);
						$('#external-1').prop('checked', true);
					} break;
				}
				
				$('#updateLink').val( id );
				$('.actions').addClass('editMode');
			}

			,deleteRlink : function(trigger){
				console.log('delete related link');
				// confirm modal
				// if delete is confirmed
				// unlink :
				// AJAX call
				// create blog: remove entry from temp field #rlink_meta / meta:'rlinks'
				//   optional: delete link from links table?
				// edit blog: remove link entry in blog_links table on blog_id,link_id
				// remove HTML item from links list

				var linkItem = trigger.parents('li.link').eq(0);
				var linkid = linkItem.attr('data-linkid');

				var rlinkMeta = $('#rlink_meta');
				var id = linkItem.attr('data-linkid');
				var title = linkItem.find('p').html();

				var c = confirm("do you want to delete the link '" + title + "'");
				if( c ) {
					// AJAX unlink 
					zebra.lrvlAjax({
						"url" : "/admin/ajax/link/remove",
						"data" : {
							"id" : id,
							"blog" : blogID,
							"mode" : newBlogMode
						},
						"action" : function(text) {
							var data = $.parseJSON(text);
							// check for successful link
							if (data.status !== 1) {
								// remove link list item
								linkItem.remove();

								relatedLinkCount--;

								if ( relatedLinkCount === 0 ) {
									$('.rlw').addClass('nolinks');
								} else {
									$('.rlw').removeClass('nolinks');
								}

								if ( newBlogMode === 1 ) {
									// copy to metadata field '#rlink_meta'
									// find link in relatedLinks
									var index = 0;
									do {
										var thisLink = relatedLinks[index];

										if(parseInt(id,10) === thisLink.id)
											break;
										index++;
									} while (index < relatedLinks.length)

									// array splice relatedLinks at index to remove
									relatedLinks.splice(index, 1); 

									// update metadata field
									$('#rlink_meta').val(JSON.stringify(relatedLinks));
								}
							}
						}
					});

				}
			}
			,addRlink : function(){
				console.log('add related link');

				var rlinkMeta = $('#rlink_meta');

				// var title = $('#rlink-title').text();
				var title = CKEDITOR.instances['rlink-title'].getData();
				var url = $('#rlink-url').val();
				var thumbnail = $('#rlink-thumb').val();
				var external = $("input[name=rlink-external]:checked").val();

				// do basic validation
				// required fields: title,url
				// thumbnail optional

				$('.error').removeClass('error');
				var errors = [];

				if ( blankRegEx.test(title) ){
					errors.push('#rlink-title');
				}

				if ( blankRegEx.test(url) ){
					errors.push('#rlink-url');
				}

				if ( errors.length === 0 ) {
					// valid (not empry title,url fields)
					// create new entry in linklist
					//  AJAX call to server to see if link URL already exists in links table
					//    either get existing link ID and override specified title/thumbnail
					//    or create new link record with new title,url, image (*multiple url entries in links table) and return new link id

					zebra.lrvlAjax({
						"url" : "/admin/ajax/link/add",
						"data" : {
							"title" : title,
							"url" : url,
							"external" : external,
							"thumbnail" : thumbnail,
							"blog" : blogID,
							"mode" : newBlogMode
						},
						"action" : function(text) {
							var data = $.parseJSON(text);
							// check for successful link
							if (data.status !== 1) {
								// create blog -> insert new link data in metadata field #rlink_meta / meta:'rlinks'
								// edit blog -> create new link in blog_links with blog_id,link_id

								relatedLinkCount++;

								$('.rlw').removeClass('nolinks');

								zebra.buildRlink({
									"id" : data.newid,
									"title" : title,
									"url" : url,
									"external" : external,
									"thumbnail" : thumbnail
								});

								if ( newBlogMode === 1 ) {
									// insert new related link
									relatedLinks.push({
										"id" : parseInt(data.newid,10),
										"title" : title,
										"url" : url,
										"external" : external,
										"thumbnail" : thumbnail
									});

									// update metadata field
									$('#rlink_meta').val(JSON.stringify(relatedLinks));
								}
							}
						}
					});

				} else {
					// one or both of title,url is blank
					// display error message / highlight empty field
					errors.map(function(field){
						console.log("error %s", field);
						$(field).addClass('error');
					}, this);
				}
			}

			// copy values from link editor back into relevant link lisk item
			,updateRlink : function(){
				var rlinkMeta = $('#rlink_meta');
				var id = $('#updateLink').val();

				if ( id > 0 ) {

					// get data from link editor panel
					var title = CKEDITOR.instances['rlink-title'].getData();
					var url = $('#rlink-url').val();
					var thumbnail = $('#rlink-thumb').val();
					var external = $("input[name=rlink-external]:checked").val();

					// AJAX update link
					zebra.lrvlAjax({
						"url" : "/admin/ajax/link/update",
						"data" : {
							"id" : id,
							"title" : title,
							"url" : url,
							"external" : external,
							"thumbnail" : thumbnail,
							"blog" : blogID,
							"mode" : newBlogMode
						},
						"action" : function(text) {
							var data = $.parseJSON(text);
							// check for successful link
							if (data.status !== 1) {
								// create blog -> insert new link data in metadata field #rlink_meta / meta:'rlinks'
								// edit blog -> create new link in blog_links with blog_id,link_id

								// find item in linklist to update
								var item = $('li#link' + id);

								if ( !blankRegEx.test(thumbnail) && thumbnail !== '-' ) {
									if (thumbnail !== item.attr('data-thumbnail')) {
										item.find('.frame').eq(0).remove(0);
										var frame = $('<div></div>').addClass('frame');
										var preview = $('<img></img').attr({"src" : thumbnail });
										frame.append(preview);
										item.prepend(frame);
									}
								} else {
									item.find('.frame').eq(0).remove(0);
								}

								// update item
								item.find('p').eq(0).html(title);
								item.attr({
									"data-url" : url,
									"data-thumbnail" : thumbnail,
									"data-external" : external
								});

								if ( newBlogMode === 1 ) {

									// update metatdata
									var index = 0;
									do {
										var thisLink = relatedLinks[index];

										if(parseInt(id,10) === thisLink.id)
											break;
										index++;
									} while (index < relatedLinks.length)

									// update relatedLinks
									relatedLinks[index] = {
										"id" : parseInt(id,10),
										"title" : title,
										"url" : url,
										"external" : external,
										"thumbnail" : thumbnail
									};
									// update metadata field
									$('#rlink_meta').val(JSON.stringify(relatedLinks));
								}
							}
						}
					});
				}
			}

			// switch from edit mode to add mode in link editor
			,resetRlink : function(){
				// clear form fields
				CKEDITOR.instances['rlink-title'].setData( '' );
				$('#rlink-url').val( '' );
				$('#rlink-thumb').val( '' );
				
				// set buttons state
				$('.actions').removeClass('editMode');

				// clear link id metadat
				$('#updateLink').val(0);
			}

			,buildRlink : function(linkdata){
				// linkdata.id
				// linkdata.title
				// linkdata.url
				// linkdata.external
				// linkdata.thumbnail

				var linklist = $('#rlinkList');

				var item = $('<li></li>').attr({
					"id" : "link" + linkdata.id,
					"data-linkid" : linkdata.id,
					"data-url" : linkdata.url,
					"data-external" : linkdata.external,
					"data-thumbnail" : linkdata.thumbnail
				}).addClass('link');

				if ( !blankRegEx.test(linkdata.thumbnail) && linkdata.thumbnail !== '-' ) {
					var frame = $('<div></div>').addClass('frame');
					var preview = $('<img></img').attr({"src" : linkdata.thumbnail });
					frame.append(preview);
					item.append(frame);
				}

				var itemTitle = $('<p></p>').html(linkdata.title);
				var itemControls = $('<ul></ul>').addClass('controls');

				// <button class="button success small editRlink" type="button">Edit</button>
				var itemEdit = $('<li></li>').addClass('edit');
				var itemEditButton = $('<button></button>').addClass('button success small editRlink').attr({"type" : "button"}).text('Edit').on('click',function(e){
					e.preventDefault();
					zebra.editRlink($(this));
				});
				itemEdit.append(itemEditButton);

				var itemDelete = $('<li></li>').addClass('delete');
				var itemDeleteButton = $('<button></button>').addClass('button success small deleteRlink').attr({"type" : "button"}).text('Delete').on('click',function(e){
					e.preventDefault();
					zebra.deleteRlink($(this));
				});
				itemDelete.append(itemDeleteButton);

				itemControls
				.append(itemEdit)
				.append(itemDelete);

				item
				.append(itemTitle)
				.append(itemControls);

				linklist.append(item);
			}


			/*
			,replaceImageThumb: function(e) {
				e.preventDefault();
				$("#imgPreviewThumb").addClass("hidden");
				$("#imgUploadThumb").removeClass("hidden");
				$('html,body').animate({scrollTop: 0},'slow');
			}

			,replaceImage: function(e) {
				e.preventDefault();
				$("#imgPreview").addClass("hidden");
				$("#imgUpload").removeClass("hidden");
				$('html,body').animate({scrollTop: 0},'slow');
				
			}

			,removeImageThumb: function(e) {
				e.preventDefault();
				$("#imgPreviewThumb").addClass("hidden");
				$("#imgUploadThumb").removeClass("hidden");
				$('html,body').animate({scrollTop: 0},'slow');
				$("#clear_thumb").val("1");
			}

			,removeImage: function(e) {
				e.preventDefault();
				$("#imgPreview").addClass("hidden");
				$("#imgUpload").removeClass("hidden");
				$('html,body').animate({scrollTop: 0},'slow');
				$("#clear_image").val("1");
			}
			
			,selectGallery: function(e) {
				//e.preventDefault();
				if ($(this).val() == '0') {
					$("#galleryPreview").addClass("hidden");
				} else {
					$("#galleryPreview").removeClass("hidden");
				}
			}
			*/
		});
		
		return PageView;
	}
);