/* ------------------------------------------------ */
// PanelBackgroundVideo
/* ------------------------------------------------ */

function PanelBackgroundVideo(parameters) {
	this.parameters = parameters;

	this.index = parameters.index;
	this.panel = parameters.panel;
	this.videoTitle = parameters.video;

	this.isLoaded = false;
	this.isVisible = false;
	this.hasBeenTriggered = false;
	this.isPlaying = false;
	this.isPaused = false;

	this.videos = [];
	this.videoTag = null;

	this.videoPath = 'dist/video/home/panels/';
	this.videoSource = {
		"mp4" : this.videoPath + this.videoTitle + '.mp4',
		"webm" : this.videoPath + this.videoTitle + '.webm',
		"ogv" : this.videoPath + this.videoTitle + '.ogv'
	}

	this.init();
}

PanelBackgroundVideo.prototype = {
	"constructor" : PanelBackgroundVideo,
	"template" : function () {let that = this; },
	
	"init" : function () {
		let that = this;
		console.log("PanelBackgroundVideo.init index: %s video: %s", this.index, this.videoTitle);
	},

	"insertVideo" : function(callback) {
		let that = this;
		// console.log("insertVideo.init video: %s", this.videoTitle);

		// let newVideoTag = $('<video preload="metadata" nocontrols autoplay playsinline muted loop></video>');  // muted needs to be in the tag when it is generated
		let newVideoTag = $('<video preload="metadata" nocontrols playsinline muted autoplay loop></video>');  // muted needs to be in the tag when it is generated
		newVideoTag
		.attr({
			"id" : "panel-" + this.videoTitle
		});

		// attach mp4 source
		if ( this.videoSource.mp4 ) {
			let newVideoSourceMP4 = $('<source></source>').attr({
				"type" : "video/mp4",
				"src" : this.videoSource.mp4
			});
			newVideoTag.append(newVideoSourceMP4);
		}

		/* */
		newVideoTag.on('loadedmetadata',function(){
			that.isLoaded = true;
			let video = $(this).get(0),
			readyInfo = video.readyState;
			video.play();

			// send message to queue, video loaded, move onto next video
			callback.act.bind(callback.queue)();
			// $(this).removeClass('waiting');
		});
		/* */

		// loop video when it ends
		newVideoTag.on('ended',function(){
			$(this).get(0).currentTime = 0;
			$(this).get(0).play();
		});

		this.videoTag = newVideoTag;
		this.panel.append(newVideoTag);
	},

	"startVideo" : function() {
		let that = this;
		// console.log("startVideo.init index: %s", this.index);

		if (this.isLoaded) {
			let video = this.videoTag.get(0);
			video.play();

			this.setIsPlaying(true);
		}
	},
	"stopVideo" : function() {
		let that = this;
		// console.log("stopVideo.init index: %s", this.index);

		if (this.isLoaded) {
			let video = this.videoTag.get(0);
			video.pause();

			this.setIsPlaying(false);
		}
	},
	"setIsVisible" : function(isVisible) {
		this.isVisible = isVisible;
	},
	"getIsVisible" : function() {
		return this.isVisible;
	},
	"setHasBeenTriggered" : function(hasBeenTriggered) {
		this.hasBeenTriggered = hasBeenTriggered;
	},
	"getHasBeenTriggered" : function() {
		return this.hasBeenTriggered;
	},
	"setIsPlaying" : function(isPlaying) {
		this.isPlaying = isPlaying;
	},
	"getIsPlaying" : function() {
		return this.isPlaying;
	},
	"setIsPaused" : function(isPaused) {
		this.isPaused = isPaused;
	},
	"getIsPaused" : function() {
		return this.isPaused;
	}
}

export default PanelBackgroundVideo;