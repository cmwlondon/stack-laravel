function QueueSingle(parameters) {
	this.parameters = parameters;

	this.actions = parameters.actions;
	this.settings = parameters.settings;

	this.workers = [];
	this.workerIndex = 0;
	this.workerCount = 0;
	this.running = false;

	this.init();
}

QueueSingle.prototype = {
	"constructor" : QueueSingle,
	"template" : function () {var that = this; },
	
	"init" : function () {
		var that = this;

		// console.log('Queue.init');
		// console.log(this.settings);
	},
	"addItem" : function(item) {
		var that = this;
		this.workers.push(item);
		this.workerCount = this.workers.length;

		return this.workerCount;
	},
	"startQueue" : function() {
		var that = this;

		this.actions.queueStart(this);

		this.startWorker();
		this.running = true;
	},
	"startWorker" : function() {
		var that = this;

		if (this.workerIndex < this.workerCount) {
			this.actions.itemStart(this,this.workers[this.workerIndex]);
		} else {
			this.queueComplete();
		}
		
	},
	"workerComplete" : function() {
		var that = this;

		this.actions.itemComplete(this,this.workers[this.workerIndex]);

		this.workerIndex++;	
		this.startWorker();
	},
	"queueComplete" : function() {
		var that = this;

		this.actions.queueComplete(this);
		this.running = false;
	}
}
export default QueueSingle;
