/* ------------------------------------------------ */
// ParallaxScroll
/* ------------------------------------------------ */

function ParallaxScroll(parameters) {
	this.parameters = parameters;

	this.modules = parameters.modules // thisScrollModules.moduleOffsets
	/*
	this.modules.index
	this.modules.id
	this.modules.min
	this.modules.max
	this.modules.height
	this.modules.width
	this.modules.middle
	this.modules.colour
	*/

	this.images = parameters.images;
	this.loopPresent = parameters.loopPresent;
	this.loopModule = parameters.loopModule;
	this.loopDuration = parameters.loopDuration;
	this.isIos = parameters.isIOS;
	this.loopItems;
	this.loopItemCount = 0
	this.loopItemIndex = 0;
	this.loopRunning = false;

	this.picBoxes = [];
	this.mirrors = [];
	this.mirrorLoopImages = [];

	this.init();
}

ParallaxScroll.prototype = {
	"constructor" : ParallaxScroll,
	"template" : function () {var that = this; },
	
	"init" : function () {
		var that = this;
		console.log('ParallaxScroll.init()');

		this.isMobile = mobileWidth;
		this.context = (this.isMobile) ? 'mobile' : 'desktop'; 

		// set correct desktop/mobile images in mirror elements
		this.build();

		if ( this.loopPresent ) {
			this.startLoop();
		}

		// onscroll
		$(window).scroll(function(){
			// var throttle = window.setTimeout(function(){
				that.scrollUpdate( $(window).scrollTop() );
			// }, 10);
		});

		// onresize
		// fired from ScrollModules.init();
	},

	"indexLoop" : function() {},

	"build" : function() {
		var that = this;

		if (this.isIos) {
			// ios, look for .picbox
			this.buildPicBoxes();
		} else {
			// other devices, look for .mirror
			this.buildMirrors();
		}

		$('div.boundary').removeClass('contentLoading');			
	},

	"buildPicBoxes" : function() {
		var that = this,
			module,
			target;

		this.images.map(function(image){
			module = $('#' + image.module);

			if (image.context == that.context) {
				target = module.find('.picbox').eq(0);
				target.css({"background-image" : "url(" + image.src + ")"});
			}
		});
	},

	"buildMirrors" : function() {
		var that = this,
			target,
			loopcount;

		this.indexLoop();

		this.images.map(function(image, index){
			if (image.context == that.context && image.index == 0) {
				target = $('#mirror_' + image.module);

				that.buildMirror(target,image, index);
				// position mirror for initial page state, scrollTop = 0
				that.updateMirror(target,0);
				
			}
		});

	},

	"buildMirror" : function(target, image, index) {
		var that = this,
			newIMg,
			moduleIndex,
			imgCSS;

		newIMg = $('<img></img>')
		.addClass('mirrorImage')
		.attr({
			"src" : image.src,
		})

		moduleIndex = $('article.module').index($('#' + image.module));
		target
		.attr({ "data-image-index" : index, "data-module-index" : moduleIndex })
		.css({"width" : windowSize.w + "px", "height" : windowSize.h + "px" });

		imgCSS = this.scaleImage(target, index);
		newIMg.css(imgCSS);

		target
		.append(newIMg);
	},

	"scrollUpdate" : function( scrollTop ) {
		var that = this;

		if (!this.isIos) {

			// skip first mirror element if a video is set in the first module
			$('.mirror').not('#mirror_module1').each(function(index){

			// $('.mirror').each(function(index){				
				that.updateMirror($(this), scrollTop);
			});
		}
	},

	"updateMirror" : function(mirror, scrolltop) {
		var that = this;

		// console.log("ParallaxScroll.updateMirror");
		// console.log(mirror);

		var imageIndex = mirror.attr('data-image-index');

		// console.log("imageIndex: %s", imageIndex);

		var imgData = that.findImageData(imageIndex);

		// console.log("imgData: %s", imgData);

		var	moduleIndex = mirror.attr('data-module-index'),
			moduleData = this.modules[moduleIndex],
			thistop = moduleData.min,
			thisHeight = moduleData.height,
			percent = ( 100 * ( ( scrolltop - thistop ) / thisHeight ) ),
			img,
			imageH = 0,
			theta,
			psi;

		// $('div.test').append( $('<p></p>').text("index;" + i + " id:'" + $(this).attr('id') + "' st:" + st + " thistop:" + thistop + " thisHeight:" + thisHeight + " st-thistop:" + ( st - thistop )  + " percent:" + percent ) );
		// select visible modules
		if ( percent > -100.5 && percent < 100.5) {
			mirror.css({"opacity" : "1", "top" : -( scrolltop - thistop ) + "px"});
			img = mirror.find('img').eq(0);

			if (!isIOS) {
			/* */
				theta = 0; // equivalent to image sitting in normal page flow, scrolls at same speed as content
				theta = 1; // image behaves as if set to position:fixed
				theta = 0.6;

				imageH = imgData.nh;
				// imageH = img.attr('data-nh');
				psi = (thisHeight / imageH) * theta;

				// for image height = module height
				img.css({"top" : psi * (imageH * (percent /100)) + "px"});
			/* */
			}
		} else {
			mirror.css({"opacity" : "0"});
		}

	},

	"resizer" : function(scrollTop) {
		var that = this,
			target,
			images,
			imageIndex,
			imgIndices,
			imgCSS;

		var rx = 0, ry = 0, rw = windowSize.w, rh = (windowSize.h > moduleMinHeight) ? windowSize.h : moduleMinHeight, percent, img;

		if (!isIOS) {
			// skip first mirror element if a video is set in the first module
			$('.mirror').not('#mirror_module1').each(function(i){

			// $('.mirror').each(function(i){
				target = $(this);

				ry = (4 - i) * rh;

				target
				.css({
					"width" : rw + "px",
					"height" : rh + "px"
				});

				percent = ( 100 * ( ( scrollTop - ry ) / rh ) );

				if ( percent > -100.5 && percent < 100.5) {
					$(this).css({"opacity" : "1", "top" : -( scrollTop - ry ) + "px"});
				} else {
					$(this).css({"opacity" : "0", "top" : -( scrollTop - ry ) + "px"});
				}		

				imageIndex = $(this).attr('data-image-index');
				images = target.find('img').eq(0);
				imgCSS = that.scaleImage(target, imageIndex);
				images.css(imgCSS);
			});
		}

		// resize mirror elements and switch between desktop/mobile images
		if ( this.isMobile == !mobileWidth ) {
			this.isMobile = mobileWidth;
			this.context = (this.isMobile) ? 'mobile' : 'desktop'; 

			if (isIOS) {
				this.buildPicBoxes();
			} else {
				// load mobile/desktop images
				$('div.boundary').addClass('contentLoading');
				$('.mirror').each(function(i){
					$(this).empty();
				});
				this.buildMirrors();

				this.scrollUpdate( $(window).scrollTop() );

				$('div.boundary').removeClass('contentLoading');			
			}
		}

	},

	"findImageData" : function(index) {
		var that = this;

		return this.images[index];
	},

	"scaleImage" : function(target, index) {
		var that = this,
			thisImageData = that.findImageData(index),
			mirror = {
				"w" : target.width(),
				"h" : target.height()
			},
			scale, newW, newH, ol, ot, vw, vw2, iw, iw2, edgeOffset, a, b, theta, gamma;

		/*
		thisImageData.mw = mirror.w;
		thisImageData.mh = mirror.h;
		thisImageData.ma = mirror.w / mirror.h;
		*/
		mirror.a = mirror.w / mirror.h;

		thisImageData.xc = parseFloat(target.attr('data-xc'));

		if ( thisImageData.a > mirror.a ) {
			// scale image by height
			scale = mirror.h / thisImageData.h;
		} else {
			// scale image by width
			scale = mirror.w / thisImageData.w;
		}

		newW = (thisImageData.w * scale) * 1;
		newH = (thisImageData.h * scale) * 1;

		// calculate left and top margins
		vw = mirror.w;
		vw2 = vw / 2;
		iw = newW;
		iw2 = iw /2;
		edgeOffset = iw2 - vw2;
		theta = Math.abs(thisImageData.xc);
		theta = 0;

		a = theta * iw2;
		b = (1 - theta) * iw2;

		if ( thisImageData.xc >= 0 ) {
			// +Ve 
			if ( b > vw2 ) {
				gamma = -(iw2 + a); // place centroid of image at centre of veiwport
			} else {
				gamma = -(iw2 + edgeOffset); // gamma -> margin-left offset sufficient to pin right hand edge of image to right hand edge of viewport
			}
		} else {
			// -Ve
			if ( b > vw2 ) {
				gamma = -(iw2 - a); // place centroid of image at centre of veiwport
			} else {
				gamma = -iw2;
			}
		}

		ol = newW / 2; // centre align image 
		ot = 0;

		thisImageData.nw = newW;
		thisImageData.nh = newH;
		thisImageData.zeta = -(newH - mirror.h);

		return {
			"top" : ot + "px",
			"left" : "50%",
			"marginLeft" : gamma + "px",
			"width" : newW + "px",
			"height" : newH + "px"
		};
	}

	/* --------------------------------------------------------------------------- */

}

window.ParallaxScroll = ParallaxScroll;
export default ParallaxScroll;