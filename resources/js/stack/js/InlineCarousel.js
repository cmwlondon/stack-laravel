/* ------------------------------------------------ */
// InlineCarousel
/* ------------------------------------------------ */

// case study page image carousel
// CSS: /src/sass/casetudy.scss
/*
	/casestudy/white-oak.html
	<div class="multiImage carousel" data-speed="2500">
		<div class="inline on"><img src="../dist/img/casestudy/white-oak/pic1.jpg" alt=""></div>
		<div class="inline"><img src="../dist/img/casestudy/white-oak/pic2.jpg" alt=""></div>
		<div class="inline"><img src="../dist/img/casestudy/white-oak/pic3.jpg" alt=""></div>
		<div class="inline"><img src="../dist/img/casestudy/white-oak/pic4.jpg" alt=""></div>
	</div>
*/

function InlineCarousel(parameters) {
	this.parameters = parameters;

	this.slides = parameters.slides;
	this.speed = parameters.speed;

	this.index = 0;
	this.oldIndex;
	this.slideCount = 0;
	this.slideAnimation;

	this.init();
}

InlineCarousel.prototype = {
	"constructor" : InlineCarousel,
	"template" : function () {var that = this; },
	
	"init" : function () {
		let that = this;
		console.log('InlineCarousel.init()');

		this.slideCount = this.slides.length;

		this.start();
	},

	"start" : function () {
		let that = this;
		this.slideAnimation = window.setInterval(function() {
			that.transition();
		}, this.speed);

		// reset position of slide which has just been moved out of focus			
		this.slides.on('transitionend webkitTransitionEnd oTransitionEnd', function () {
			if (that.slides.index($(this)) === that.oldIndex) {
				that.slides.eq(that.oldIndex).removeClass('out');	
			}
		});
	},

	"transition" : function () {
		var that = this;

		// slides move from right to left
		this.slides.eq(this.index).removeClass('on').addClass('out');

		this.oldIndex = this.index;
		/*
		let slideReturn1 = window.setTimeout(function(){
			that.slides.eq(that.oldIndex).removeClass('out');
		}, 600);
		*/
		this.index = this.nextSlide(this.index);

		this.slides.eq(this.index).addClass('on');
	},

	"nextSlide" : function (slide) {
		let nextSlide = slide + 1;

		if ( nextSlide == this.slideCount) {
			nextSlide = 0;
		}

		return nextSlide;
	}
}

window.InlineCarousel = InlineCarousel;
export default InlineCarousel;