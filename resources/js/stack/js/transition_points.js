// /src/system.js
// case study transition points

$(document).ready(function () {
	function transitionPoints() {
		let points = $('.point');
		
		// stagger points, delay between transitions in milliseconds -> setting.delay
		let pointQueue = new QueueSingle({
			"settings" : {
				"delay" : 250
			},
			"actions" : {
				"queueStart" : function(queue){},
				"itemStart" : function(queue,worker){
					$('#' + worker.id).addClass('go');

					worker.delay = window.setTimeout(function(){
						queue.workerComplete();	
					}, queue.settings.delay);
					
				},
				"itemComplete" : function(queue,worker){},
				"queueComplete" : function(queue){
					pointsDone = true;
				}
			}
		});

		let p1 = [];
		$('.point').each(function(i){
			pointQueue.addItem({"id" : $(this).attr('id')});
		});
		pointQueue.startQueue();

	};

	if ($('.points').length > 0) {
		// animated impact points
		let pointsTriggerScroll = $('#pointTrigger').position().top;
		window.pointsDone = false;
		let scrollY = $(window).scrollTop();

		// trigger transition if 'Impact' section is visible on page load
		let triggerPoint1 = windowSize.h;
		if ( scrollY > (pointsTriggerScroll - triggerPoint1) ) {

			if ( !pointsDone ) {
				transitionPoints();
			}

		}

		$(window).scroll(function(){
			let scrollY = $(window).scrollTop();

			// trigger transition when 'IMPACT' becomes visible
			let triggerPoint2 = windowSize.h * 0.75;

			if ( scrollY > (pointsTriggerScroll - triggerPoint2) ) {

				if ( !pointsDone ) {
					transitionPoints();
				}
			}
		});
	}
}
