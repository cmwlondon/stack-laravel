/* ------------------------------------------------ */
// PhraseChanger
/* ------------------------------------------------ */

function PhraseChanger(parameters) {
	this.parameters = parameters;

	this.container = parameters.container;
	this.speed = parameters.speed;

	this.phrases;
	this.index = 0;
	this.count = 0;

	this.intervalTimer;

	this.init();
}

PhraseChanger.prototype = {
	"constructor" : PhraseChanger,
	"template" : function () {var that = this; },
	
	"init" : function () {
		var that = this;

		this.phrases = this.container.find('span');
		this.count = this.phrases.length;

		this.intervalTimer = window.setInterval(function(){
			that.action();
		}, this.speed);
	},

	/* --------------------------------------------------------------------------- */

	"action" : function () {
		var that = this;

		this.phrases.eq(this.index).removeClass('on');
		this.loop();
		this.phrases.eq(this.index).addClass('on');
	},

	/* --------------------------------------------------------------------------- */

	"loop" : function () {
		var that = this;

		this.index++;

		if (this.index == this.count) {
			this.index = 0;
		}
	}

	/* --------------------------------------------------------------------------- */

}

// window.PhraseChanger = PhraseChanger;
export default PhraseChanger;
