import '../../sass/stack/styles.scss'
// import Bootstrap from './js/bootstrap'

import Utility from './js/Utility.js'
import Queue from './js/Queue.js'
import QueueSingle from './js/QueueSingle.js'
// import ScrollModules from './js/ScrollModules.js'
// import PhraseChanger from './js/PhraseChanger.js'
// import ParallaxScroll from './js/ParallaxScroll.js'
import InlineCarousel from './js/InlineCarousel.js'
import HomeVideo from './js/HomeVideo.js'
import PanelBackgroundVideo from './js/PanelBackgroundVideo.js'

import ScrollMonitor from './js/ScrollMonitor.js'
import stickybits from 'stickybits'

function remap(percent,range) {
	let percentP = (percent > (1 - range)) ? (percent - (1 - range)) / range: 0;
	return percentP;
}

window.mobileWidth = false;
window.windowSize = 0;
window.isIOS = null;
window.thisParallaxScroll = null;
window.moduleMinHeight = 320;
window.pageHeight = 0;
window.slider2Height = 0;
window.debugScale = false;

window.supportsCSSTransitions = false;
window.supportsCSSAnimation = false;
window.supportsSVG = false;
window.supportsWebWorker = false;
window.documentFonts = false;
window.ishighdpi = false;
window.isIE = false;
window.dpi = 0;
window.landscapeAspect = false;
window.shortLandscape = false;
window.doVisualUpdates = false;
var isIE10 = false;
var isIE11 = !!navigator.userAgent.match(/Trident\/7\./);

var modules,
	mirrors,
	menuModule,
	thisScrollModules,
	phraseChangers = [],
	thisUtility = new Utility({}),
	imgprefix = 'dist/img/modules/',
	cacheQueue, moduleID, loop, di, mi,xc, moduleImages = [],
	loopPresent = false,
	loopModule,
	loopWorker,
	peopleGridWidth;


window.requestAnimFrame = (function(callback) {
	return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
	function(callback) {
		window.setTimeout(callback, 1000 / 60);
	};
})();

if (!window.console) {
	window.console = {
		"log" : function(){}
	}
}

Modernizr.addTest('isios', function() {
    return (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) ? true : false;
});

/*
https://www.paulirish.com/2009/throttled-smartresize-jquery-event-handler/
Debounced Resize() jQuery Plugin
August 11th 2009
*/
(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize 
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

// usage:
$(window).smartresize(function(){
  // code that takes it easy...
});

// contact page google map module
function initmap(){
	let stackoffice = new google.maps.LatLng( 51.5218891, -0.1359421 ); // 90 tottenham court road lat & lng

	let map = new google.maps.Map(document.getElementById("stackmap"), {
		"zoom" : 17,
		"center" : {
		  "lat" : 51.5218891,
		  "lng" : -0.1359421
		},
		"panControl" : true,
		"zoomControl" : true,
		"mapTypeControl" : true,
		"streetViewControl" : true
	});


	// tilesloaded
	map.addListener('tilesloaded', function(me) {
	    // let delay = window.setTimeout(function(){
	    	$('#stackmap').addClass('ready');

	    	// define custom icon
			let stackIcon = {
				"url" : "/img/drop_pin_02.png",
				"anchor" : new google.maps.Point(33,80),
				"size" : new google.maps.Size(331,400),
				"scaledSize" : new google.maps.Size(66,80)
			};

			let newMarker = new google.maps.Marker({
		      "position" : stackoffice,
		      "icon" : stackIcon,
		      // "animation" : google.maps.Animation.DROP
		      "map" : map,
		    });

		    newMarker.addListener('click', function(poiMarker) {
		    	console.log('STACK');
		    	window.open('https://www.google.co.uk/maps/place/Tottenham+Court+Rd,+Fitzrovia,+London+W1T+4TJ/@51.5220482,-0.1381659,17z/data=!3m1!4b1!4m5!3m4!1s0x48761b293a4be25f:0xa8c2275ab2d6d797!8m2!3d51.5219813!4d-0.1360347');
		    	// window.location.href = 'https://www.google.co.uk/maps/place/Tottenham+Court+Rd,+Fitzrovia,+London+W1T+4TJ/@51.5220482,-0.1381659,17z/data=!3m1!4b1!4m5!3m4!1s0x48761b293a4be25f:0xa8c2275ab2d6d797!8m2!3d51.5219813!4d-0.1360347';
		    });
	    // }, 10);
	});
}
window.initmap = initmap;

function windowDimensions() {
	windowSize = {
		"w" : $(window).width(),
		"h" : $(window).height(),
		"a" : $(window).width() / $(window).height()
	};

	if (windowSize.a > 1) {
		$('html').removeClass('portraitAspectRatio');
		$('html').addClass('landscapeAspectRatio');
		landscapeAspect = true;

		if (landscapeAspect && windowSize.h < 500) {
			shortLandscape = true;
			$('html').addClass('shortLandscape');
		} else {
			shortLandscape = false;
			$('html').removeClass('shortLandscape');
		}
	} else {
		$('html').removeClass('landscapeAspectRatio');
		$('html').removeClass('shortLandscape');
		$('html').addClass('portraitAspectRatio');
		landscapeAspect = false;
	}
}

// insert auto play video into page
// used in: citroen 3 aircross (casetudy/citroen-c3-aircross.html), evaluate professionally curious (evaluate2.html)
function insertAutoVideo(videoSource) {
	let newVideoTag = $('<video muted></video>'); // muted needs to be in the tag when it is generated

	// you can't add the 'muted' attribute after the tag has been created
	let settings = {
		"preload" : "metadata",
		"nocontrols" : "",
		"playsinline" : "",
		"loop" : ""
	};

	if (videoSource.poster) {
		settings.poster = videoSource.poster;
	}

	console.log(videoSource);
	console.log(settings);

	newVideoTag
	.attr(settings)
	.addClass('waiting');

	// attach mp4 source
	if ( videoSource.mp4 ) {
		let newVideoSourceMP4 = $('<source></source>').attr({
			"type" : "video/mp4",
			"src" : videoSource.mp4
		});
		newVideoTag.append(newVideoSourceMP4);
	}

	// attach webm source
	if ( videoSource.webm ) {
		let newVideoSourceWEBM = $('<source></source>').attr({
			"type" : "video/webm",
			"src" : videoSource.webm
		});
		newVideoTag.append(newVideoSourceWEBM);
	}

	// attach ogv source
	if ( videoSource.ogv ) {
		let newVideoSourceOGV = $('<source></source>').attr({
			"type" : "video/ogg",
			"src" : videoSource.ogv
		});
		newVideoTag.append(newVideoSourceOGV);
	}

	// start video once it is ready to go
	newVideoTag.on('loadedmetadata',function(){
		var video = $(this).get(0),
		readyInfo = video.readyState;
		video.play();
		$(this).removeClass('waiting');
	});

	// loop video
	newVideoTag.on('ended',function(){
		console.log('video ended');
		$(this).get(0).currentTime = 0;
		$(this).get(0).play();
	});

	return newVideoTag;
}


/*
	the bulk of the javascript behaviours for the site pages is defined here
	homepage
	casestudy/*
	blog landing page
	blog item
	people
	clients
	specific case study items: carousel, autostarting inline videos, evaluate animated HTML header
*/

function pageGo() {
		$('body').addClass('gill-sans-font-ready');

		/* ---------------------------------------------------------------------------- */
		// homepage
		// dependent: /index.html

		if ( $('body').hasClass('homepage') ) {

			// scroll down buttons
			$('.scrollDown').on('click',function(e){
				e.preventDefault();
				let target = $(this).attr('href');

				$(window).scrollTo(target,{
					"duration" : 1000
				});
			});

			// home page first panel 'stack' video
			let thisHomeVideo = new HomeVideo({
				"destination" : "#hv",
				"videotag" : '<video id="homevideo" preload="metadata" muted nocontrols playsinline loop data-xephon="8817"></video>',
				"sources" : {
					"poster" : "/img/stack-poster.png",
					"mp4" : "/video/home/stack_web_intro-1kbps.mp4",
					"webm" : "",
					"ogv" : ""
				},
				"delay" : 2000
			});

			let panelVideo = new PanelBackgroundVideo({
				"panel" : $('#panelVideoBox'),
				"videoSource" : {
					"mp4" : "/video/home/stack_bg_vidv2.mp4",
					"webm" : "",
					"ogv" : ""
				}
			});
			panelVideo.insertVideo();

			// navigation dots plus panel visibility animation
			let homeScrollMonitor = new ScrollMonitor({
				"dotsSelector" : "ul.position li",
				"moduleClass" : ".module"
			});

			/* new popup video element 'continuous customer acquisition' */
			let popupVideoTag = null;
			$('.videotrigger').on('click', function(e){
				e.preventDefault();

				let videoMP4 = $(this).attr('data-video');
				let videoPoster = $(this).attr('data-poster');
				let videoCaption = $(this).attr('data-caption');

				// examine window dimensions / aspect ratio and video dimensions
				// scale to 100% width for portrait or approx 90% height for landscape window/device

				// has the popup been populated yet?
				if ( !$('.popup .content').hasClass('video-is-in') ) {
					// no, populate now

					// update popup header
					$('.popup .header h2').html(videoCaption);

					// insert video tag with autoplay and loop
					popupVideoTag = $('<video></video>');

					// you can't add the 'muted' attribute after the tag has been created
					popupVideoTag
					.attr({
						"preload" : "metadata",
						"controls" : "",
						"playsinline" : "",
						"noloop" : ""
					})
					.addClass('waiting');

					// attach mp4 source
					let newVideoSourceMP4 = $('<source></source>').attr({
						"type" : "video/mp4",
						"src" : videoMP4
					});
					popupVideoTag.append(newVideoSourceMP4);

					// start video once it is ready to go
					popupVideoTag.on('loadedmetadata',function(){
						var video = $(this).get(0),
						readyInfo = video.readyState;
						video.play();
						$(this).removeClass('waiting');
					});

					/*
					// loop video
					popupVideoTag.on('ended',function(){
						console.log('video ended');
						$(this).get(0).currentTime = 0;
						$(this).get(0).play();
					});
					*/

					// mark popup as being populated
					$('.popup .content')
					.append(popupVideoTag)
					.addClass('video-is-in');
				} else {
					// start video
					popupVideoTag.get(0).currentTime = 0;
					popupVideoTag.get(0).play();
				}

				$('body').addClass('popup-open');
			});

			$('.black').on('click',function(e){
				e.preventDefault();

				// stop video
				popupVideoTag.get(0).pause();
				popupVideoTag.get(0).currentTime = 0;

				$('body').removeClass('popup-open');
			});
			$('.popup .header a').on('click',function(e){
				e.preventDefault();

				// stop video
				popupVideoTag.get(0).pause();
				popupVideoTag.get(0).currentTime = 0;

				$('body').removeClass('popup-open');
			});
		}

		/* ---------------------------------------------------------------------------- */

		/* ---------------------------------------------------------------------------- */
		// casestudy
		// dependent: /casetudy/*.html

		if ( $('body').hasClass('casestudy') ) {

			// fullspan video play buttons
			var videos = [];

			$('.video').each(function(i) {

				$(this).find('a.playButton').eq(0).on('click',function(e){
					e.preventDefault();

					let videoBox = $(this).parents('div.video').eq(0), videoID = videoBox.attr('id');

					/*
					toggle playing videos if there are more than one in the page and one is already running
					*/

					// are there more than one videos in the page
					if ( videos.length > 1 ) {
						// yes, multiple videos,
						let tvi = 0, tv;
						do {
							tv = videos[tvi];

							if( !tv.playing && tv.id === videoID ) {
								tv.playing = true;
								let videoItem = videoBox.find('video').eq(0);
								videoItem.get(0).currentTime = 0;
								videoItem.get(0).play();
								videoBox.addClass('started');
							} else {
								tv.playing = false;
								let otherBox = $('#' + tv.id), otherVideo = otherBox.find('video').eq(0);
								otherBox.removeClass('started');
								otherVideo.get(0).pause();
							}
							tvi++;
						} while (tvi < videos.length)

					} else {
						// no, start single video
						let videoItem = videoBox.find('video').eq(0);
						videoItem.get(0).play();
						videos[0].playing = true;
						videoBox.addClass('started');
					}

				});

				videos.push({
					"id" : $(this).attr('id'),
					"playing" : false
				});
			});

			$('header.work').addClass('go');

			// transition points
			/*
			*/

			/*
			control aspect ratio of video element in solace-hidden-abuse case study
			1:1 (square video element)
				in landscape based on height of viewport
				in portrait based on width of viewport (90%)
			*/
			if( $('#solace-hidden-abuse').length > 0 ) {

				let r = 965.66 / 609;
				// r = h / w
				// w' = h' / r
				// h' = w' * r

				// let vbox = $('#solace-hidden-abuse,#solace-hidden-abuse video');
				let vbox = $('#solace-hidden-abuse');

				// set state of video box on load
				let newH = windowSize.h * 0.6;
				let newW = newH / r;

				vbox.css({
					"width" : newW + "px",
					"height" : newH + "px"
				});

				// set state on window resize
				$(window).smartresize(function(){

					console.log("window.height: %s", windowSize.h);

					let newH = windowSize.h * 0.6;
					let newW = newH / r;

					vbox.css({
						"width" : newW + "px",
						"height" : newH + "px"
					});
				});

				window.onorientationchange = function() { 
					console.log("window.height: %s", windowSize.h);

					let newH = windowSize.h * 0.6;
					let newW = newH / r;

					vbox.css({
						"width" : newW + "px",
						"height" : newH + "px"
					});
				};

			}
		}

		/* ---------------------------------------------------------------------------- */
		// blog index
		// dependent: /blog/

		if ( $('body').hasClass('blogindex') ) {
			console.log('blogindex');

			// catalog blog posts in index page
			let posts = [];
			$('article.post').each(function(index){
				let frame = $(this).find('div.frame').eq(0);
				posts.push({
					"i" : index,
					"id" : $(this).attr('id'),
					"category" : $(this).attr('data-category'),
					"thumb" : frame.attr('data-thumb'),
					"image" : frame.attr('data-image')

				});
			});
			// $('article.post').removeClass('loading');

			let postQueue = new QueueSingle({
				"settings" : {
					"delay" : 250 // quarter second delay between each person fades in
				},
				"actions" : {
					"queueStart" : function(queue){},
					"itemStart" : function(queue,worker){
						let workerNode = $('article#' + worker.id);

						let portraitImageFull = $('<img></img>').attr({
							"alt" : worker.id,
							"src" : worker.image
						})
						.addClass('full')
						.on('load',function(e){

							let portraitImageThumb = $('<img></img>').attr({
								"alt" : worker.id,
								"src" : worker.thumb
							})
							.addClass('thumb')
							.on('load',function(e){
								// stagger image loading
								worker.delay = window.setTimeout(function(){

									queue.workerComplete();	

								}, queue.settings.delay);

							});
							workerNode.find('.frame a').append( portraitImageThumb )

						});
						workerNode.find('.frame a').append( portraitImageFull )

					},
					"itemComplete" : function(queue,worker){
						let workerNode = $('article#' + worker.id);
						workerNode.removeClass('loading');	
					},
					"queueComplete" : function(queue){
					}
				}
			});

			posts.map(function(post){
				postQueue.addItem({
					"id" : post.id,
					"thumb" : post.thumb,
					"image" : post.image
				});
			});

			postQueue.startQueue();
		}

		/* ---------------------------------------------------------------------------- */
		// blog post
		// dependent: /blog/*

		if ( $('body').hasClass('blogpost') ) {
			// $('header.work').addClass('go');
		
			// fullspan video play buttons
			var videos = [];

			$('.video').each(function(i) {

				$(this).find('a.playButton').eq(0).on('click',function(e){
					e.preventDefault();

					let videoBox = $(this).parents('div.video').eq(0), videoID = videoBox.attr('id');

					/*
					toggle playing videos if there are more than one in the page and one is already running
					*/

					// are there more than one videos in the page
					if ( videos.length > 1 ) {
						// yes, multiple videos,
						let tvi = 0, tv;
						do {
							tv = videos[tvi];

							if( !tv.playing && tv.id === videoID ) {
								tv.playing = true;
								let videoItem = videoBox.find('video').eq(0);
								videoItem.get(0).currentTime = 0;
								videoItem.get(0).play();
								videoBox.addClass('started');
							} else {
								tv.playing = false;
								let otherBox = $('#' + tv.id), otherVideo = otherBox.find('video').eq(0);
								otherBox.removeClass('started');
								otherVideo.get(0).pause();
							}
							tvi++;
						} while (tvi < videos.length)

					} else {
						// no, start single video
						let videoItem = videoBox.find('video').eq(0);
						videoItem.get(0).play();
						videos[0].playing = true;
						videoBox.addClass('started');
					}

				});

				videos.push({
					"id" : $(this).attr('id'),
					"playing" : false
				});
			});

		}
		
		/* ---------------------------------------------------------------------------- */
		// people
		// dependent: /people.html

		// people page bio overlay/insert
		if ( $('body').hasClass('people') ) {

			// event: user clicks on person photo to open/toggle BIO panel
			// START
			function openBio(person,personIndex) {
				let lastRow = false;

				if( !person.hasClass('centred') ) {
					centredItem.addClass('ncentre');	
				} else {
					lastRow = true;
				}

				let metadata = person.find('div.metadata');
				openBioIndex = personIndex; // store bio index
				person.addClass('openPerson');
				let bioInsertIndex = personIndex + peopleGridWidth - 1;

				let biobox = $('<div></div>');
				biobox.addClass('biobox');

				if (lastRow && peopleGridWidth === 2) {
					biobox.addClass('lastBioBox');
				}

				let bioboxoptions = ['','one','two','three', 'four'];
				let bioboxwidth = bioboxoptions[peopleGridWidth];
				biobox.addClass(bioboxwidth);
				biobox.append( metadata.clone() );

				if ( peopleGridWidth > 1) {

					if ( bioInsertIndex > ( visiblePeopleCount - 1 ) ) {
						let extraCount = bioInsertIndex - visiblePeopleCount;

						switch( extraCount ){
							case 2 : {
								peopleGrid.append($('<div></div>').addClass('biobox').addClass('spacerbox'));
								peopleGrid.append($('<div></div>').addClass('biobox').addClass('spacerbox'));
								peopleGrid.append($('<div></div>').addClass('biobox').addClass('spacerbox'));
							} break;
							case 1 : {
								peopleGrid.append($('<div></div>').addClass('biobox').addClass('spacerbox'));
								peopleGrid.append($('<div></div>').addClass('biobox').addClass('spacerbox'));
							} break;
							case 0 : {
								if (!(lastRow && peopleGridWidth === 2)) {
									peopleGrid.append($('<div></div>').addClass('biobox').addClass('spacerbox'));
								}
							} break;
						}

						peopleGrid.append(biobox);	
					} else {
						visiblePeople.eq(bioInsertIndex).after(biobox);	
					}	
					
				} else {
					visiblePeople.eq(personIndex).after(biobox);
				} 
			}
			// END

			let peopleGrid = $('.peopleGrid');
			let visiblePeople = $('article.person').not('.hide');
			let openBioIndex = -1;
			let visiblePeopleCount = visiblePeople.length;

			let centredItem = $('.centred');

			if ( windowSize.w >= 1024 ) {
				peopleGrid.addClass('mode4');
				peopleGridWidth = 4;
			} else if ( windowSize.w >= 768 && windowSize.w < 1024) {
				peopleGrid.addClass('mode3');
				peopleGridWidth = 3;
			} else if ( windowSize.w >= 480  && windowSize.w < 768) {
				peopleGrid.addClass('mode2');
				peopleGridWidth = 2;
			} else {
				peopleGrid.addClass('mode1');
				peopleGridWidth = 1;
			}

			let lastRowSize = visiblePeopleCount % peopleGridWidth;

			$('article.person .frame img').remove();

			// queue: load all people images then fade in page elements
			let personQueue = new QueueSingle({
				"settings" : {
					"delay" : 250 // quarter second delay between each person fades in
				},
				"actions" : {
					"queueStart" : function(queue){},
					"itemStart" : function(queue,worker){

						let portraitFile = worker.name;
						let PersonName = worker.node.find('.info h2').text();
						let lastInRow = null;

						let portraitImage = $('<img></img>').attr({
							"alt" : PersonName,
							"src" : portraitFile
						}).on('load',function(e){
							worker.node.addClass('ready');	
							worker.delay = window.setTimeout(function(){
								queue.workerComplete();	
							}, queue.settings.delay);
						});

						// need to deal with missing/broken images
						// add error/timeout handling to image loader queue
						// load placeholder image if main image is not available

						worker.node.find('.frame').append(portraitImage);

						worker.node.find('a.triggerbio').on('click', function(e){
							e.preventDefault();

							// copy details from metatdata into overlay
							let person = $(this).parents('article.person').eq(0);
							let personIndex = visiblePeople.index(person);

							/**/
							if ( peopleGrid.hasClass('openbio') ) {
								// a bio is open -> openBioIndex
								if ( openBioIndex === personIndex ) {
									// clicked on open bio, close bio
									peopleGrid.find('.biobox').remove();
									peopleGrid.removeClass('openbio');
									person.removeClass('openPerson');
									openBioIndex = -1;

									centredItem.removeClass('ncentre');
								} else {
									// clicked on another person, close open bio (openBioIndex) and open new bio (personIndex)
									centredItem.removeClass('ncentre');
									visiblePeople.eq(openBioIndex).removeClass('openPerson');
									peopleGrid.find('.biobox').remove();
									openBio(person,personIndex);
								}

							} else {
								// no bios open, open new bio -> personIndex
								peopleGrid.addClass('openbio');
								openBio(person,personIndex);
							}
							/**/

						})
					},
					"itemComplete" : function(queue,worker){},
					"queueComplete" : function(queue){
					}
				}
			});

			$('article.person').each(function(i){
				if ( !$(this).hasClass('hide') ) {
					personQueue.addItem({
						"node" : $(this),
						"name" : $(this).attr('data-portrait')
					});
				}
			});

			personQueue.startQueue();

			$(window).smartresize(function(){

				let bioboxGo = $('.biobox')
				windowSize = {
					"w" : $(window).width(),
					"h" : $(window).height(),
					"a" : $(window).width() / $(window).height()
				};

				if ( windowSize.w >= 1024 ) {

					peopleGrid.removeClass('mode1 mode2 mode3');
					peopleGrid.addClass('mode4');
					peopleGridWidth = 4;

					bioboxGo.removeClass('one two three');
					bioboxGo.addClass('four');
				} else if ( windowSize.w >= 768 ) {

					peopleGrid.removeClass('mode1 mode2 mode4');
					peopleGrid.addClass('mode3');
					peopleGridWidth = 3;

					bioboxGo.removeClass('one two four');
					bioboxGo.addClass('three');
				} else if ( windowSize.w >= 480 ) {
					peopleGrid.removeClass('mode1 mode3 mode4');
					peopleGrid.addClass('mode2');
					peopleGridWidth = 2;

					bioboxGo.removeClass('one three four');
					bioboxGo.addClass('two');
				} else {
					peopleGrid.removeClass('mode2 mode3 mode4');
					peopleGrid.addClass('mode1');
					peopleGridWidth = 1;

					bioboxGo.removeClass('two three four');
					bioboxGo.addClass('one');
				}
			});

			/*
			$('section.personOverlay').on('click',function(e){
				e.preventDefault();
				// copy details from metatdata into overlay
				$('section.personOverlay').removeClass('open');
				console.log('x');
				centredItem.removeClass('ncentre');
			});
			*/		
		}

		/* ---------------------------------------------------------------------------- */
		// clients page
		// dependent: /clients.html

		if ( $('body').hasClass('clients') ) {
			// add touch events to show client info overlays

			// queue clients logos for staggered load
			/*

			let logo = $('article div.logo').attr(data-logo)
			$('article div.logo').css({ "background-image" : "url('dist/img/clients/" + logo + ".png');" });
			*/

			// queue: load all people images then fade in page elements
			let logoQueue = new QueueSingle({
				"settings" : {
					"delay" : 250, // quarter second delay between each person fades in
					// "logopath" : "/uploads/assets/images/clients/"
					"logopath" : ""
				},
				"actions" : {
					"queueStart" : function(queue){},
					"itemStart" : function(queue,worker){

						let logoFile = queue.settings.logopath + worker.logo;

						// load image into hidden image cache
						let logoImage = $('<img></img>').attr({
							"alt" : worker.title,
							"src" : logoFile
						})
						.on('load',function(e){
							worker.delay = window.setTimeout(function(){
								queue.workerComplete();	
							}, queue.settings.delay);
						})
					    .on('error', function(e) {
							// image did not load
							worker.client.addClass('brokenImage');
							worker.delay = window.setTimeout(function(){
								queue.workerComplete();	
							}, queue.settings.delay);
					    });

						$('.moduleCache').append(logoImage);
					},
					"itemComplete" : function(queue,worker){
						worker.client.addClass('ready');
						worker.logobox.css({ "background-image" : "url('" + queue.settings.logopath + worker.logo + "')" });
					},
					"queueComplete" : function(queue){
						// temporarily disable focus states for client logos until all copy is in place
						/*
						$('.clientlist article a.focus').each(function(i){
							$(this).on('click', function(e){
								e.preventDefault();
								let thisItem = $(this).parents('article').eq(0);
								// let thisInfo = thisItem.find('.info');
								if ( thisItem.hasClass('active') ) {
									thisItem.removeClass('active');
								} else {
									thisItem.addClass('active');
								}

							});
						});
						*/
					}
				}
			});

			$('section.clientlist article').each(function(i){
				let logoBox = $(this).find('div.logo').eq(0);
				let logoTitle = $(this).find('div.info h2').eq(0).html();
				logoQueue.addItem({
					"title" : logoTitle,
					"client" : $(this),
					"logobox" : logoBox,
					"logo" : logoBox.attr('data-logo')
				});
			});

			logoQueue.startQueue();

		}

		/* ---------------------------------------------------------------------------- */
		// case study page
		// image carousel

		if ( $('.carousel').length > 0 ) {

			console.log('carousel');
			let CasestudyCarousel = new InlineCarousel({
				"slides" : $('.carousel .inline'),
				"speed" : $('.carousel').attr('data-speed')
			});
		}

		/* ---------------------------------------------------------------------------- */

		// Citroen C3 Aircross case study: /casestudy/citroen-c3-aircross.html
		let flipperBox = document.querySelector('div#flipperBox');
		if ( flipperBox !== null ) {
			let src_mp4 = flipperBox.getAttribute('data-mpv')

			let newVideoTag = insertAutoVideo({
				"mp4" : src_mp4,
				"ogv" : false,
				"webm" : false, 
				"poster" : false
			});

			$('#flipperBox').prepend(newVideoTag);

		}

		/* ---------------------------------------------------------------------------- */

		// autovideo player
		if ( $('.autovideo').length > 0 ) {
			$('.autovideo').each(function(){

				let newVideoTag = insertAutoVideo({
					"mp4" : ($(this).attr('data-mpv') === undefined) ? false : $(this).attr('data-mpv'),
					"webm" : ($(this).attr('data-webm') === undefined) ? false : $(this).attr('data-webm'),
					"ogv" : ($(this).attr('data-ogv') === undefined) ? false : $(this).attr('data-ogv'),
					"poster" : ($(this).attr('data-poster') === undefined) ? false : $(this).attr('data-poster')
				});

				$(this).append(newVideoTag);

			});
		}

		/*
		if ( $('.autovideoflex').length > 0 ) {
			$('.autovideoflex').each(function(){
				$(this).append(insertAutoVideo({
					"mp4" : ($(this).attr('data-mpv') === undefined) ? false : $(this).attr('data-mpv'),
					"webm" : ($(this).attr('data-webm') === undefined) ? false : $(this).attr('data-webm'),
					"ogv" : ($(this).attr('data-ogv') === undefined) ? false : $(this).attr('data-ogv'),
					"poster" : ($(this).attr('data-poster') === undefined) ? false : $(this).attr('data-poster')
				}));
			});
		}
		*/

		/* ---------------------------------------------------------------------------- */

		// evaluate case study header animation 
		// /casestudy/evaluate2.html
		if ( $('.ev2header').length > 0 ) {

			let gaps = $('.ev2header span.gaps');
			let cursor = $('.ev2header img');
			let underscore = $('.ev2header .underscore');
			let start = 'gaps';
			let end = '____';
			let blinkDelay = 150;
			let cursorOffsets = [38,42.85,46,50.25,53.5];
			let underscoreWidth = [14,10,6.5,2.75,0];
			window.gi = 0;

			function reset(){
				gi = 0;
				gaps.text( start.slice(0,gi) );
				underscore.css({"width" : underscoreWidth[gi] + '%'});
				cursor.css({"left" : cursorOffsets[gi] + '%'});

				cursorBlink(gapTimer);
			}

			// should replace this with a CSS animation at some point
			function cursorBlink(callback){
				// callback temple of doom
				// look out Indy!
				cursor.css({ "opacity": 1 }).animate({"opacity" : 0}, blinkDelay, function(){
					cursor.animate({"opacity" : 1}, blinkDelay, function(){
						cursor.animate({"opacity" : 0}, blinkDelay, function(){
							cursor.animate({"opacity" : 1}, blinkDelay, function(){
								cursor.animate({"opacity" : 0}, blinkDelay, function(){
									cursor.animate({"opacity" : 1}, blinkDelay, function(){
										cursor.animate({"opacity" : 0}, blinkDelay, function(){
											cursor.animate({"opacity" : 1}, blinkDelay, function(){
												cursor.animate({"opacity" : 0}, blinkDelay, function(){
													cursor.animate({"opacity" : 1}, blinkDelay, function(){
														cursor.animate({"opacity" : 0}, blinkDelay, function(){
															cursor.animate({"opacity" : 1}, blinkDelay, function(){
																cursor.animate({"opacity" : 0}, blinkDelay, function(){
																	cursor.animate({"opacity" : 1}, blinkDelay, function(){
																		if ( callback !== null) {
																			callback();	
																		}
																	});
																});
															});
														});
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});
			}

			function gapTimer () {
				gaps.text( start.slice(0,gi) );
				underscore.css({"width" : underscoreWidth[gi] + '%'});
				cursor.css({"left" : cursorOffsets[gi] + '%'});

				gi++;

				if ( gi <= start.length) {
					window.setTimeout(function(){
						gapTimer();
					}, 150);
				} else {
					cursorBlink(reset);
				}
			}

			// start animation
			cursorBlink(gapTimer);
		}

		/* ---------------------------------------------------------------------------- */
}




$(document).ready(function () {

	supportsCSSAnimation = $('html').hasClass('cssanimations');
	supportsCSSTransitions = $('html').hasClass('csstransitions');
	supportsSVG = $('html').hasClass('csstransitions');
	documentFonts = document.fonts;
	isIE = $('html').hasClass('ie') || isIE11 || isIE10;

	isIOS = Modernizr.isios;
	// id device supports devicePixelRatio check for retina display, otherwise assume standard res (IE 9/10) 
	if (typeof window.devicePixelRatio != 'undefined' ) {
		ishighdpi = ( window.devicePixelRatio > 1);	
		dpi = window.devicePixelRatio;
	} else {
		ishighdpi = false;
		dpi = 1;
	}

	if ( location.href.indexOf('iostest=1') !== -1 ) { isIOS = true; }

	// test for iOS devices - relevant to background image attachment/device pixel ratio
	if ( isIOS ) { $('html').addClass('ios'); } else { $('html').addClass('not-ios'); } 

	$('html').addClass('dpr' + dpi);

	windowDimensions();

	$(window).smartresize(function(){
		windowDimensions();
	});

	window.onorientationchange = function() { 
		windowDimensions();
	}

	// mobileWidth = (windowSize.w < 768) || (windowSize.a < 1);
	mobileWidth = (windowSize.w < 1024) && (windowSize.a < 1);


	document.addEventListener('visibilitychange', function(){
		doVisualUpdates = !document.hidden;
		/*
		if (doVisualUpdates) {
			console.log('visible');
		} else {
			console.log('hidden');
		}
		*/
	});

	/*
	if (window.Worker) {
		console.log("web worker supported");
		global.gridWorker = new Worker('dist/grid_worker.js');
	}
	*/

	/* ---------------------------------------------------------------------------- */

	// useful stuff goes here

	// navigation menu trigger
	$('.trigger').on('click',function(e){
		e.preventDefault();

		let hp = $('header.page');

		if (hp.hasClass('open')) {
			hp.removeClass('open');
		} else {
			hp.addClass('open');
		}
	});

	/* ---------------------------------------------------------------------------- */

	if ( documentFonts ) {
		// set up promise for supported browsers
		// hide page content till the main body font has loaded
		document.fonts.ready.then(function() {
			pageGo();
		})
	} else {
		// document.fonts not supported (Microsoft IE/Edge), display the page content whether or not the fonts have loaded
		pageGo();
	}

	/* ---------------------------------------------------------------------------- */

})
