@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<h2>Case studies: {{ $casestudycount }}</h2>
		<h2>Blog posts: {{ $blogcount }}</h2>
		{{--
		<ul>
			@foreach ( $blog AS $blogItem)
			<li>
				<p>{{ $blogItem['slug'] }}-{{ $blogItem['id'] }}</p>
				<img alt="" src="{{ $blogItem['image'] }}">
				<img alt="" src="{{ $blogItem['thumb'] }}">
			</li>
			@endforeach
		</ul>
		--}}
		<h2>People: {{ $peoplecount }}</h2>
		<h2>Clients: {{ $clientcount }}</h2>
	</div>
@endsection
