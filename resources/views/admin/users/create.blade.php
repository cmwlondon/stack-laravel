@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li>{!! link_to_route('admin.users.index', 'Users') !!}</li>
			  <li class="current">Create User</li>
			</ul>

			{!! Form::open(array('route' => ['admin.users.store'], 'method' => 'POST') )!!}
				<div class="row">
					<div class="small-12 columns mt1">
						@include('admin.users.partials._form')
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						{!! Form::submit('Create', array('class' => 'button success small ff')) !!}
						{!! link_to_route('admin.users.index', 'Cancel', [], ['class' => 'small button alert ff']) !!}
					</div>
				</div>
			{!! Form::close() !!}
		</div>		
	</div>
@stop
