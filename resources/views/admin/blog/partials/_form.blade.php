{!! Form::hidden('id', $id, ['id' => 'id']) !!}
{!! Form::hidden('tags', null, ['id' => 'tags']) !!}
{!! Form::hidden('rlinks', null, ['id' => 'rlinks']) !!}

{{-- <pre>{{ print_r($test, true) }}</pre> --}}
<div class="row">


<div class="small-12 medium-12 large-6 columns">
	{!! Form::label('thumb','Thumbnail Image') !!}
	<div id="previewThumb" class="imagePreview">@if (isset($post) && $post->thumb != '')<img alt="" src="{{{ $post->thumb }}}">@endif</div>
	<p class="button tiny" id="ckfinder-popup-1">Select</p>
	{!! Form::text('thumb', null, ['id' => 'thumb']) !!}
	{!! $errors->first('thumb', '<small class="error">:message</small>') !!}
</div>

<div class="small-12 medium-12 large-6 columns">
	{!! Form::label('image','Wide Image') !!}
	<div id="previewImage" class="imagePreview">@if (isset($post) && $post->image != '')<img alt="" src="{{{ $post->image }}}">@endif</div>
	<p class="button tiny" id="ckfinder-popup-2">Select</p>
	{!! Form::text('image', null, ['id' => 'image']) !!}
	{!! $errors->first('image', '<small class="error">:message</small>') !!}
</div>

	<div class="small-12 columns">
		{!! Form::label('title', 'Title') !!}
		{!! Form::text('title',(isset($post) ? $post->title : null),['placeholder' => '', 'id' => 'title']) !!}
		{!! $errors->first('title', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 columns">
		{!! Form::label('slug', 'Slug') !!}
		{!! Form::text('slug',(isset($post) ? $post->slug : null),['placeholder' => '', 'id' => 'slug']) !!}
		{!! $errors->first('slug', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 columns mb1">
		{!! Form::label('synopsis', 'Synopsis') !!}
		{!! Form::textarea('synopsis',(isset($post) ? $post->synopsis : null),['placeholder' => '', 'id' => 'synopsis', 'rows' => '2']) !!}
		{!! $errors->first('synopsis', '<small class="error">:message</small>') !!}
	</div>


	<div class="small-12 medium-12 large-12 columns">
		<div class="row ml0">
		{!! Form::label('category', 'Category') !!}
		{!! Form::select('category', $categoryList, (isset($category) ? $category->id : null)) !!}
		{!! $errors->first('category', '<small class="error">:message</small>') !!}
		</div>
	</div>

	<div class="small-12 columns mt1">
		{!! Form::label('authors', 'Author(s)') !!}
		{!! Form::select('authors[]', $authorList, (isset($authors) ? $authors : null), [ 'multiple' => true]) !!}
		{!! $errors->first('authors', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 medium-12 large-12 columns">
		<div class="row ml0">
			{!! Form::label('tags', 'Tags') !!}
			{!! Form::hidden('newtags', $newtags, ['id' => 'newtags']) !!}
			<ul class="tagBox">
				@foreach( $tags AS $tag)
					<li data-tag-id="{{ $tag->id }}"><span class="text">{{ $tag->text }}</span><span class="remove">X</span></li>
				@endforeach
			</ul>
			{!! Form::text('newtag',null,['placeholder' => '', 'id' => 'newtag']) !!}
			<ul id="tagdd"></ul>
			{!! Form::button('Add new tag', ['class' => 'button success small','id' => 'newTagAdd']) !!}
		</div>
	</div>

	<div class="small-12 columns mb1">
		{!! Form::label('body', 'Body') !!}
		{!! Form::textarea('body',(isset($post) ? $post->body : null),['placeholder' => '', 'id' => 'body']) !!}
		{!! $errors->first('body', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 medium-12 large-12 columns rLinks">
		<div class="row ml0">

			<div class="wrapper rlw @if( count($links) === 0 ) nolinks @endif">
				<h2>Related links:</h2>
				{!! Form::hidden('rlink_meta',null,['placeholder' => '', 'id' => 'rlink_meta']) !!}
				<ul id="rlinkList">
					@if( count($links) > 0 )
					
					@foreach( $links AS $link )
					<li id="link{{ $link->id }}" data-linkid="{{ $link->id }}" data-url="{{ $link->url }}" data-thumbnail="{{ $link->thumbnail }}" data-external="{{ $link->external }}" class="link">
						@if( $link->thumbnail !== '-' )<div class="frame"><img alt="" src="{{ $link->thumbnail }}"></div>@endif
						<p>{!! $link->title !!}</p>
						<ul class="controls">
							<li class="edit">{!! Form::button('Edit', ['class' => 'button success small editRlink']) !!}</li>
							<li class="delete">{!! Form::button('Delete', ['class' => 'button success small deleteRlink']) !!}</li>
						</ul>
					</il>
					@endforeach
					@endif
				</ul>
				<p class="nl">No related links set</p>
			</div>
			<div id="rlinkEdit">
				<div class="wrapper">
					<h2>Related link editor</h2>
					{!! Form::hidden('updateLink', 0, ['id' => 'updateLink']) !!}
					{!! Form::label('rlink-title', 'Title / Caption') !!}
					{!! Form::textarea('rlink-title',null,['placeholder' => '', 'id' => 'rlink-title']) !!}

					{!! Form::label('rlink-url', 'URL') !!}
					{!! Form::text('rlink-url',null,['placeholder' => '', 'id' => 'rlink-url']) !!}

					<formfield>
						<legend>External link (opens in new window/tab)</legend>
						{!! Form::radio('rlink-external', '1', false, ['id' => 'external-1'] ) !!} {!! Form::label('external-1', 'Yes') !!} 
						{!! Form::radio('rlink-external', '0', true, ['id' => 'external-0'] )  !!} {!! Form::label('external-0', 'No') !!}
						{!! $errors->first('rlink-external', '<p>:message</p>') !!}
					</formfield>

					{!! Form::label('rlink-thumb', 'thumbnail image') !!}
					<div>
					{!! Form::text('rlink-thumb',null,['placeholder' => '', 'id' => 'rlink-thumb']) !!}
					{!! Form::button('Select thumbnail', ['class' => 'button success small','id' => 'selectRlinkThumb']) !!}
					</div>
				</div>
				<div class="actions">
					{!! Form::button('Add Link', ['class' => 'button success small','id' => 'addRlink']) !!}
					{!! Form::button('Update Link', ['class' => 'button success small','id' => 'updateRlink']) !!}
					{!! Form::button('Reset Link editor', ['class' => 'button success small','id' => 'resetRlink']) !!}
				</div>
			</div>
		</div>
	</div>

	<div class="small-12 columns">
		<ul class="accordion" data-accordion data-allow-all-closed="true">
		  <li class="accordion-item">
		    <a href="#" class="accordion-title">Search Engine Optimisation</a>
		    <div class="accordion-content" data-tab-content>
		    	<div class="row">
			    	<div class="small-12 columns">
						{!! Form::label('meta_title', 'Page Title') !!}
						{!! Form::text('meta_title',(isset($post) ? $post->meta_title : null),['placeholder' => '']) !!}
						{!! $errors->first('meta_title', '<small class="error">:message</small>') !!}
					</div>

					<div class="small-12 columns">
						{!! Form::label('meta_description', 'Page Description') !!}
						{!! Form::text('meta_description',(isset($post) ? $post->meta_description : null),['placeholder' => '']) !!}
						{!! $errors->first('meta_description', '<small class="error">:message</small>') !!}
					</div>
				</div>
		    </div>
		  </li>
		</ul>
	</div>
	
	<div class="small-6 after-6 columns">
		{!! Form::label('created_at', 'Date') !!}
		{!! Form::date('created_at',(isset($post) ? $post->created_at : $now),['placeholder' => '', 'id' => 'created_at']) !!}
		{!! $errors->first('created_at', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 columns mt1">
		<h5>Post Status</h5>
		<div class="panel radius">
		  <div class="row">
		  	<div class="columns small-4">{!! Form::radio('status','draft',(isset($post) ? $post->status : null), ['class' => 'mb0']) !!} Draft</div>
			<div class="columns small-4">{!! Form::radio('status','published',(isset($post) ? $post->status : null), ['class' => 'mb0']) !!} Published</div>
			<div class="columns small-4">{!! Form::radio('status','archived',(isset($post) ? $post->status : null), ['class' => 'mb0']) !!} Archived</div>
		  </div>
		</div>
		{!! $errors->first('status', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 medium-12 large-12 columns" style="display:none;">
		<h5>Related Links</h5>
		<div class="row ml0">
			<ul>
				<li id="l1">Post: POST TITLE <button class="linkRemove">REMOVE LINK</button></li>
				<li id="l2">Link: Title/URL  <button class="linkRemove">REMOVE LINK</button></li>
			</ul>

			<p>
				checkbox: post<br>
				select: post title<br><br>
				checkbox: link<br>
				text field: Title<br>
				test field: URL<br>
				[thumnbail image select]<br><br>
				<button class="linkAdd">Add LINK</button>
			</p>
		</div>
	</div>

</div>