@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li class="current">Blog Posts</li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="medium-12 columns">
			@if (isset($blog) && count($blog) != 0)
				<table style="width:100%;" id="sortTable" class="display">
				  <thead>
				    <tr>
				      <th scope="column">Post</th>
				      <th scope="column" width="150">Category</th>
				      <th scope="column" width="150">Status</th>
				      <th scope="column" width="200">Date</th>
				      <th width="115"scope="column">Actions</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach ($blog as $post)
						<tr class="{{ $post['status'] }} @if($featured == $post['id']) featured @endif">
					      <td class="table-middle"><a href="" class="fancybox">{!! link_to('/admin/blog/edit/'.$post['id'], $post['title']) !!}</a></td>
					      <td class="table-middle">{{{ ucfirst($post['category']['title']) }}}</td>
					      <td class="table-middle">{{{ ucfirst($post['status']) }}}</td>
					      <td class="table-middle">{{{ $post['created'] }}}</td>
					      <td class="table-buttons">
					      	<button href="#" data-toggle="drop_{{{ $post['id'] }}}" aria-controls="drop_{{{ $post['id'] }}}" aria-expanded="false" class="button small dropdown mb0">Options</button>
							<ul class="dropdown-pane" id="drop_{{{ $post['id'] }}}" data-dropdown data-auto-focus="true">
								<li>{!! link_to('/admin/blog/edit/'.$post['id'], 'Edit') !!}</li>
								<li>{!! link_to('/blog/'.$post['slug'] .'?preview=true', 'Preview', ['target' => '_blank']) !!}</li>

								<li>{!! Form::model($post, ['url' => 'admin/blog/destroy/'.$post['id'], 'method' => 'delete', 'id' => 'form_delete_'.$post['id']]) !!}
								  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this post?"); if (r == true) {document.getElementById("form_delete_'.$post['id'].'").submit();} return false;']) !!}
								  	{!! Form::close() !!}
								</li>
							</ul>

						  </td>
					    </tr>
					@endforeach

				  </tbody>
				</table>

			@else
				<p>No posts yet</p>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="medium-12 columns">
			{!! Form::open(array('url' => ['admin/blog/setfeatured'], 'method' => 'POST', 'id' => 'featuredForm') )!!}
			{!! Form::label('featured', 'Featured post') !!}
			{!! Form::select('featured', $featuredPostList, (isset($featured) ? $featured : null)) !!}
			{!! Form::submit('Set featured post', array('class' => 'button success small')) !!}
			{!! Form::close() !!}
		</div>
	</div>

@endsection
