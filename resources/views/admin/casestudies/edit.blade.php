@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li><a href="/admin/casestudies">Case studies</a></li>
			  <li class="current">Edit Case study</li>
			</ul>

			{!! Form::model($casestudy, array('url' => ['admin/casestudies/update/'.$casestudy->id], 'method' => 'PUT', 'files' => true) )!!}
				<div class="row">
					<div class="small-12 columns mt1">
						@include('admin.casestudies.partials._form')
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns mt1">
						{!! Form::submit('Update', array('class' => 'button success small')) !!}
						{!! link_to('admin/casestudies', 'Cancel', ['class' => 'small button alert']) !!}

						
					</div>
				</div>
				<input type="hidden" name="update" value="1"/>
			{!! Form::close() !!}
		</div>
	</div>
@stop
