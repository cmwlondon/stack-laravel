@extends('admin.casestudies.front-end-editor')

@section('content')
<header class="work">
	<h2>Work</h2>        
</header>

{{-- <pre>{{ print_r($moduleList,true) }}</pre> --}}

<section class="frontEndEditor">
	<header>
		<h2>Category:</h2>  <ul><li @if ( $category === 'business' ) class="active" @endif>business</li> <li @if ( $category === 'consumer' ) class="active" @endif>consumer</li> </p>
	</header>

	@foreach( $modules as $module)
	<section class="feModule" data-module-id="{{ $module['module-id'] }}" data-module-type="{{ $module['type'] }}">
		<header>
			<p>module type: '{{ $module['type'] }}'</p>
		</header>

		@include('Main.Work.Modules.'.$module['type'])

		<footer>
		</footer>
	</section>

	<section class="addModule">
		<p>Add new module</p>	
		<ul>
			@foreach( $moduleList as $moduleKey => $moduleName)
			<li data-module-type="{{ $moduleKey }}">{{ $moduleName }}</li>
			@endforeach
		</ul>
	</section>
	@endforeach

	<div class="buttonBox">
		<a href="/{{ $category }}" id="casestudyBackLink">BACK TO {{ strtoupper($category) }} STACK</a>
	</div>
</section>

<hr>
@stop
