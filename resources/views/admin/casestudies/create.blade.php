@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li><a href="/admin/casestudies">Case studies</a></li>
			  <li class="current">Create Case study</li>
			</ul>

			{!! Form::open(array('url' => ['admin/casestudies/store'], 'method' => 'POST', 'id' => 'clientForm', 'files' => true) )!!}

			@include('admin.casestudies.partials._form', ['type' => 'create', 'hidden' => 'hidden'])

			<div class="row">
				
				<div class="small-12 columns">
					{!! Form::submit('Create', array('class' => 'button success small', 'id' => 'bSubmit')) !!}
					{!! link_to('admin/casestudies', 'Cancel', ['class' => 'small button alert']) !!}
				</div>
			</div>
			{!! Form::close() !!}

		</div>		
	</div>	
@stop
