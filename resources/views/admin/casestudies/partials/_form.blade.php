{!! Form::hidden('id', $id, ['id' => 'id']) !!}
{!! Form::hidden('resub', '0', ['id' => 'resub']) !!}

@php
	if ( isset($casestudy) ) {
		$br = $casestudy->branch;
		$ti = $casestudy->thumb_image;
		$tc = $casestudy->thumb_colour;
		$pc = $casestudy->page_colour;
	} else {
		$ti = old('thumb_image');
		$tc = old('thumb_colour');
		$pc = old('page_colour');
		$br = old('branch');
	}
@endphp

{{--
<pre>{{ print_r($test, true) }}</pre>
--}}

<div class="row">

	<div class="small-6 columns">
		{!! Form::label('title', 'Title') !!}
		{!! Form::text('title',(isset($casestudy) ? $casestudy->title : null),['placeholder' => '', 'id' => 'title']) !!}
		{!! $errors->first('title', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-6 columns">
		{!! Form::label('slug', 'URL') !!}
		{!! Form::text('slug',(isset($casestudy) ? $casestudy->slug : null),['placeholder' => '', 'id' => 'slug']) !!}
		{!! $errors->first('slug', '<small class="error">:message</small>') !!}
	</div>
</div>
<div class="row">
	<div class="small-12 columns">
		{!! Form::label('branch', 'Category') !!}
		<select name="branch" id="branch">
			@foreach ( $branchList AS $index => $item)
			<option value="{{$item['value']}}" @if ( $br === $item['value'] ) selected="selected" @endif>{{ $item['caption'] }}</option>
			@endforeach
		</select>

		{!! $errors->first('branch', '<small class="error">:message</small>') !!}
	</div>
</div>

<div class="row">
	<div class="small-6 columns">
		{!! Form::label('thumb_main', 'Index Title') !!}
		{!! Form::text('thumb_main',(isset($casestudy) ? $casestudy->thumb_main : null),['placeholder' => '', 'id' => 'thumb_main']) !!}
		{!! $errors->first('thumb_main', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-6 columns">
		{!! Form::label('thumb_sub', 'Index subtitle') !!}
		{!! Form::text('thumb_sub',(isset($casestudy) ? $casestudy->thumb_sub : null),['placeholder' => '', 'id' => 'thumb_sub']) !!}
		{!! $errors->first('thumb_sub', '<small class="error">:message</small>') !!}
	</div>
</div>


<div class="row">
	<div class="small-6 columns">
		{!! Form::label('thumb_image','Thumbnail') !!}
		<div id="previewCSThumb" class="csthumbPreview">
			@if ( $ti !== '' )<img alt="" src="{{ $ti }}">@endif
		</div>
		<p class="button tiny imagepicker" data-preview="previewCSThumb" data-field="thumb_image">Select</p>
		{!! Form::hidden('thumb_image', (isset($ti) ? $ti : null), ['id' => 'thumb_image']) !!}
		<div class="rows">
		{!! $errors->first('thumb_image', '<small class="error">:message</small>') !!}
		</div>
	</div>


	<div class="small-6 columns">
		<div class="row">
			<div class="small-12 columns">
				{!! Form::label('thumb_colour', 'Thumbnail underline colour') !!}
				{!! Form::hidden('thumb_colour',(isset($casestudy) ? $casestudy->thumb_colour : null),['placeholder' => '', 'id' => 'thumb_colour']) !!}
				<ul class="colourPicker" data-field="thumb_colour">
					@foreach ( $thumbColourList AS $index => $item)
					<li data-value="{{ $item['value'] }}" style="background-color:{{ $item['colour'] }};" @if ( $tc == $item['value'] ) class="selected" @endif>{{ $item['caption'] }}</li>
					@endforeach
				</ul>
				{!! $errors->first('thumb_colour', '<small class="error">:message</small>') !!}
			</div>

			<div class="small-12 columns">
				{!! Form::label('meta_title', 'Meta page title') !!}
				{!! Form::text('meta_title',(isset($casestudy) ? $casestudy->meta_title : null),['placeholder' => '', 'id' => 'meta_title']) !!}
				{!! $errors->first('meta_title', '<small class="error">:message</small>') !!}
			</div>

			<div class="small-12 columns">
				{!! Form::label('meta_desc', 'Meta description') !!}
				{!! Form::text('meta_desc',(isset($casestudy) ? $casestudy->meta_desc : null),['placeholder' => '', 'id' => 'meta_desc']) !!}
				{!! $errors->first('meta_desc', '<small class="error">:message</small>') !!}
			</div>

			<div class="small-12 columns">
				{!! Form::label('meta_keywords', 'Meta keywords') !!}
				{!! Form::text('meta_keywords',(isset($casestudy) ? $casestudy->meta_keywords : null),['placeholder' => '', 'id' => 'meta_keywords']) !!}
				{!! $errors->first('meta_keywords', '<small class="error">:message</small>') !!}
			</div>

			<div class="small-12 columns">
				{!! Form::label('page_colour', 'Page colour scheme') !!}
				{!! Form::hidden('page_colour',(isset($casestudy) ? $casestudy->page_colour : null),['placeholder' => '', 'id' => 'page_colour']) !!}
				<ul class="colourPicker" data-field="page_colour">
					@foreach ( $pageColourList AS $index => $item)
					<li data-value="{{ $item['value'] }}" style="background-color:{{ $item['colour'] }};" @if ( $pc == $item['value'] ) class="selected" @endif>{{ $item['caption'] }}</li>
					@endforeach
				</ul>
				{!! $errors->first('page_colour', '<small class="error">:message</small>') !!}
			</div>
		</div>
	<br style="clear:both;float:none;">
</div>
@php
	$maxKey = (isset($modulesData)) ? count($modulesData) : 0;
	$maxMetaKey = 0;
	$extendBaseKey = $maxKey;
	$metakey = -1;
@endphp
<div class="row">

	<div class="moduleSection">
		<p>Modules</p>
		{!! Form::hidden('modules', null, ['id' => 'modules']) !!}
		{!! Form::hidden('modulesFieldList', (isset($modulesFieldList) ? $modulesFieldList : null), ['id' => 'modulesFieldList']) !!}
		{!! Form::hidden('modulesMeta', (isset($modulesMeta) ? $modulesMeta : null), ['id' => 'modulesMeta']) !!}
		{!! $errors->first('modules', '<small class="error">:message</small>') !!}

		<div id="moduleBox" data-nextmoduleid="{{ $nextId }}">
		@if (isset($modulesData) && count($modulesData) != 0)
			@foreach( $modulesData AS $key => $moduleData)
				@include('admin.casestudies.partials.modules.module-container')
			@endforeach
		@endif

		@php
		// get metadata and convert to array to build module fields
		$modulesMeta = old('modulesMeta');
		$moduleMetaArray = json_decode($modulesMeta);

		@endphp

		{{--
		build dynamic field based on value of 'modulesMeta' field
		these fields have been added dynamically, and are pending validation and insertion into the db
		--}}

		@if ((old('modulesMeta') !== null) && old('modulesMeta') !== '')
			@foreach( $moduleMetaArray AS $metaIndex => $moduleDataP)
				@php
				$key = $metaIndex + $extendBaseKey;
				$metakey = $metaIndex;
				$maxKey++;
				$maxMetaKey++;
				@endphp

				@php
					$moduleData = [
						'suffix' => $moduleDataP->suffix,
						'type' => $moduleDataP->type,
						'module-id' => $moduleDataP->id
					];
				@endphp
				@include('admin.casestudies.partials.modules.module-container')
			@endforeach
		@endif

		</div>
		
		<div class="row twoColumn">
			<div class="column">		
				{!! Form::select('newModuleSelector', $moduleList, null,['id' => 'newModuleSelector']) !!}
			</div>
			<div class="column">		
				<p class="button actionButton" data-action="add" data-nextmodule="{{ $nextId }}">Add New Module</p>
			</div>
		</div>
	</div>

	<h5>Active</h5>
	<div class="row twoColumn">
		<div class="column">{!! Form::radio('active','1',(isset($casestudy) ? $casestudy->active : null), ['id' => 'active1', 'class' => 'mb0']) !!} {!! Form::label('active1', 'Yes') !!}</div>
		<div class="column">{!! Form::radio('active','0',(isset($casestudy) ? $casestudy->active : null), ['id' => 'active0', 'class' => 'mb0']) !!} {!! Form::label('active0', 'No') !!}</div>
	</div>
	{!! $errors->first('active', '<small class="error">:message</small>') !!}

</div>
