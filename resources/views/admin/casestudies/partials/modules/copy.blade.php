
	<div class="row">
		{!! Form::label('html'.$moduleData['suffix'], 'Body') !!}
		{!! Form::textarea('html'.$moduleData['suffix'],(isset($moduleData['meta']) ? $moduleData['meta']['html'] : null),['placeholder' => '', 'id' => 'html'.$moduleData['suffix']]) !!}
		{!! $errors->first('html'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
