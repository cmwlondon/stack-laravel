
@php
	$_old = old('iimage'.$moduleData['suffix']);

	if ( isset($moduleData['meta']['image']) ) {
		$_image = $moduleData['meta']['image'];

		// check to see if old value is set
		if( $_old !== NULL ) {
			// check to see if old and moduledata match, i.e. has the field been updated or not
			if ( $_image === $_old  )
			{
				$ti = $_image;
			} else {
				$ti = $_old;
			}
		} else {
			$ti = $_image;
		}
	} else {
		$ti = $_old;
	}
@endphp

	<div class="row">
		{!! Form::label('iimage'.$moduleData['suffix'],'Image') !!}
		<div id="previewIImage{{ $moduleData['suffix'] }}" class="imagePreview">@if ($ti != '')<img alt="" src="{{{ $ti }}}">@endif</div>
		<p class="button tiny imagepicker" id="ckft_inline{{ $moduleData['suffix'] }}" data-preview="previewIImage{{ $moduleData['suffix'] }}" data-field="iimage{{$moduleData['suffix']}}">Select</p>
		{!! Form::text('iimage'.$moduleData['suffix'], (isset($ti) ? $ti : null), ['id' => 'iimage'.$moduleData['suffix']]) !!}
		{!! $errors->first('iimage'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>

	<div class="row">
		{!! Form::label('ialt'.$moduleData['suffix'], 'Alt Text') !!}
		{!! Form::text('ialt'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['alt'] : null),['placeholder' => '', 'id' => 'ialt'.$moduleData['suffix']]) !!}
		{!! $errors->first('ialt'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
