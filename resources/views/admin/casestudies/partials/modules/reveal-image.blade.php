@php
	$_old = old('rimage'.$moduleData['suffix']);

	if ( isset($moduleData['meta']['image']) ) {
		$_image = $moduleData['meta']['image'];

		// check to see if old value is set
		if( $_old !== NULL ) {
			// check to see if old and moduledata match, i.e. has the field been updated or not
			if ( $_image === $_old  )
			{
				$ti = $_image;
			} else {
				$ti = $_old;
			}
		} else {
			$ti = $_image;
		}
	} else {
		$ti = $_old;
	}
@endphp
	<div class="row">
		{!! Form::label('rimage'.$moduleData['suffix'],'Image') !!}
		<div id="previewRImage{{ $moduleData['suffix'] }}" class="imagePreview">@if ($ti != '')<img alt="" src="{{{ $ti }}}">@endif</div>
		<p class="button tiny imagepicker" id="ckft_reveal{{ $moduleData['suffix'] }}" data-preview="previewRImage{{ $moduleData['suffix'] }}" data-field="rimage{{$moduleData['suffix']}}">Select</p>
		{!! Form::text('rimage'.$moduleData['suffix'], (isset($ti) ? $ti : null), ['id' => 'rimage'.$moduleData['suffix']]) !!}
		{!! $errors->first('rimage'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>

	<div class="row">
		{!! Form::label('ralt'.$moduleData['suffix'], 'Alt Text') !!}
		{!! Form::text('ralt'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['alt'] : null),['placeholder' => '', 'id' => 'ralt'.$moduleData['suffix']]) !!}
		{!! $errors->first('ralt'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
