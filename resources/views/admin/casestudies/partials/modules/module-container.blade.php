<div class="module" id="module{{$moduleData['suffix']}}" data-suffix="{{$moduleData['suffix']}}" data-module="{{ $moduleData['module-id'] }}">
	<div class="row twoColumn">
		<div class="column">
			{!! Form::select('moduleSelector'.$moduleData['module-id'], $moduleList, $moduleData['type'],['id' => 'moduleSelector'.$moduleData['module-id']]) !!}
		</div>
		<div class="column">
			<p class="button actionButton" data-action="change" data-suffix="{{$moduleData['suffix']}}" data-module="{{ $moduleData['module-id'] }}" data-current="{{ $moduleData['type'] }}">Change module type</p>
		</div>
	</div>

	<div class="moduleTarget" id="moduleTarget{{$moduleData['suffix']}}">
		@include('admin.casestudies.partials.modules.'.$moduleData['type'])
	</div>

	<div class="row">
		<p class="button actionButton" data-action="remove" data-suffix="{{$moduleData['suffix']}}" data-module="{{ $moduleData['module-id'] }}">Remove this module</p>
	</div>
</div>
