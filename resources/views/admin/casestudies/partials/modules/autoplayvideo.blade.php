@php
	$_old = old('poster'.$moduleData['suffix']);

	if ( isset($moduleData['meta']['poster'])) {
		$_image = $moduleData['meta']['poster'];

		// check to see if old value is set
		if( $_old !== NULL ) {
			// check to see if old and moduledata match, i.e. has the field been updated or not
			if ( $_image === $_old  )
			{
				$ti = $_image;
			} else {
				$ti = $_old;
			}
		} else {
			$ti = $_image;
		}
	} else {
		$ti = $_old;
	}
@endphp
	<div class="row">
		{!! Form::label('poster'.$moduleData['suffix'],'Poster') !!}
		<div id="previewPoster{{ $moduleData['suffix'] }}" class="imagePreview">@if ($ti != '')<img alt="" src="{{{ $ti }}}">@endif</div>
		<p class="button tiny imagepicker" id="ckft_poster{{ $moduleData['suffix'] }}" data-preview="previewPoster{{ $moduleData['suffix'] }}" data-field="poster{{$moduleData['suffix']}}">Select</p>
		{!! Form::text('poster'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['poster'] : null), ['id' => 'poster'.$moduleData['suffix']]) !!}
		{!! $errors->first('poster'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>

<div class="row twoColumn">
	<div class="column">
		{!! Form::label('mp4'.$moduleData['suffix'], 'mp4') !!}
		{!! Form::text('mp4'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['mp4'] : null),['placeholder' => '', 'id' => 'mp4'.$moduleData['suffix']]) !!}
		<p class="button tiny videopicker" id="ckft_mp4{{ $moduleData['suffix'] }}">Select</p>
		{!! $errors->first('mp4'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
	<div class="column">
		{!! Form::label('webm'.$moduleData['suffix'], 'webm') !!}
		{!! Form::text('webm'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['webm'] : null),['placeholder' => '', 'id' => 'webm'.$moduleData['suffix']]) !!}
		<p class="button tiny videopicker" id="ckft_webm{{ $moduleData['suffix'] }}">Select</p>
		{!! $errors->first('webm'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
</div>
<div class="row twoColumn">
	<div class="column">
		{!! Form::label('ogv'.$moduleData['suffix'], 'ogv') !!}
		{!! Form::text('ogv'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['ogv'] : null),['placeholder' => '', 'id' => 'ogv'.$moduleData['suffix']]) !!}
		<p class="button tiny videopicker" id="ckft_ogv{{ $moduleData['suffix'] }}">Select</p>
		{!! $errors->first('ogv'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
</div>

<div class="row twoColumn">
	<div class="column">
		{!! Form::label('class'.$moduleData['suffix'], 'Class') !!}
		{!! Form::text('class'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['class'] : null),['placeholder' => '', 'id' => 'class'.$moduleData['suffix']]) !!}
		{!! $errors->first('class'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
	<div class="column">
		{!! Form::label('vid'.$moduleData['suffix'], 'Video ID') !!}
		{!! Form::text('vid'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['vid'] : null),['placeholder' => '', 'id' => 'vid'.$moduleData['suffix']]) !!}
		{!! $errors->first('vid'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
</div>
