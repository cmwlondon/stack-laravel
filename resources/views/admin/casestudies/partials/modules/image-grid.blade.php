{{--
    [meta] => Array
        (
            [images] => Array
                (
                    [0] => /img/casestudy/white-oak/pic1.jpg
                    [1] => /img/casestudy/white-oak/pic2.jpg
                    [2] => /img/casestudy/white-oak/pic3.jpg
                    [3] => /img/casestudy/white-oak/pic4.jpg
                )

            [alt] => WHITE OAK UK - POINTLESS PAPERWORK
        )
	$moduleData[
		'suffix' => STRING,
		'meta' => ARRAY
	]
--}}
@php
	$ti = [];
	$_old = [
		old('gimg1'.$moduleData['suffix']),
		old('gimg2'.$moduleData['suffix']),
		old('gimg3'.$moduleData['suffix']),
		old('gimg4'.$moduleData['suffix'])
	];

	if ( isset($moduleData['meta']['images']) ) {
		$_image = [
			$moduleData['meta']['images'][0],
			$moduleData['meta']['images'][1],
			$moduleData['meta']['images'][2],
			$moduleData['meta']['images'][3]
		];

		// check to see if old value is set
		foreach( $_old AS $index => $_oldItem )
		{
			if( $_oldItem !== NULL ) {
				// check to see if old and moduledata match, i.e. has the field been updated or not
				if ( $_image[$index] === $_oldItem  )
				{
					$ti[$index] = $_image[$index];
				} else {
					$ti[$index] = $_oldItem;
				}
			} else {
				$ti[$index] = $_image[$index];
			}
		}
	} else {
		$ti = $_old;
	}

@endphp
<div class="row twoColumn">
	<div class="column">
		{!! Form::label('gimg1'.$moduleData['suffix'],'Image 1') !!}
		<div id="previewImg1{{ $moduleData['suffix'] }}" class="imagePreview">@if ($ti[0] != '')<img alt="" src="{{{ $ti[0] }}}">@endif</div>
		<p class="button tiny imagepicker" id="ckft_grid1{{ $moduleData['suffix'] }}" data-preview="previewImg1{{ $moduleData['suffix'] }}" data-field="gimg1{{$moduleData['suffix']}}">Select</p>
		{!! Form::text('gimg1'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['images'][0] : null), ['id' => 'gimg1'.$moduleData['suffix']]) !!}
		{!! $errors->first('gimg1'.$moduleData['suffix'], '<small class="error">:message</small>') !!}

	</div>
	<div class="column">
		{!! Form::label('gimg2'.$moduleData['suffix'],'Image 2') !!}
		<div id="previewImg2{{ $moduleData['suffix'] }}" class="imagePreview">@if ($ti[1] != '')<img alt="" src="{{{ $ti[1] }}}">@endif</div>
		<p class="button tiny imagepicker" id="ckft_grid2{{ $moduleData['suffix'] }}" data-preview="previewImg2{{ $moduleData['suffix'] }}" data-field="gimg2{{$moduleData['suffix']}}">Select</p>
		{!! Form::text('gimg2'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['images'][1] : null), ['id' => 'gimg2'.$moduleData['suffix']]) !!}
		{!! $errors->first('gimg2'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
</div>
<div class="row twoColumn">
	<div class="column">
		{!! Form::label('gimg3'.$moduleData['suffix'],'Image 3') !!}
		<div id="previewImg3{{ $moduleData['suffix'] }}" class="imagePreview">@if ($ti[2] != '')<img alt="" src="{{{ $ti[2] }}}">@endif</div>
		<p class="button tiny imagepicker" id="ckft_grid3{{ $moduleData['suffix'] }}" data-preview="previewImg3{{ $moduleData['suffix'] }}" data-field="gimg3{{$moduleData['suffix']}}">Select</p>
		{!! Form::text('gimg3'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['images'][2] : null), ['id' => 'gimg3'.$moduleData['suffix']]) !!}
		{!! $errors->first('gimg3'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
	<div class="column">
		{!! Form::label('gimg4'.$moduleData['suffix'],'Image 4') !!}
		<div id="previewImg4{{ $moduleData['suffix'] }}" class="imagePreview">@if ($ti[3] != '')<img alt="" src="{{{ $ti[3] }}}">@endif</div>
		<p class="button tiny imagepicker" id="ckft_grid4{{ $moduleData['suffix'] }}" data-preview="previewImg4{{ $moduleData['suffix'] }}" data-field="gimg4{{$moduleData['suffix']}}">Select</p>
		{!! Form::text('gimg4'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['images'][3] : null), ['id' => 'gimg4'.$moduleData['suffix']]) !!}
		{!! $errors->first('gimg4'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
	</div>
</div>
<div class="row">
	{!! Form::label('galt'.$moduleData['suffix'], 'Alt Text') !!}
	{!! Form::text('galt'.$moduleData['suffix'], (isset($moduleData['meta']) ? $moduleData['meta']['alt'] : null),['placeholder' => '', 'id' => 'galt'.$moduleData['suffix']]) !!}
	{!! $errors->first('galt'.$moduleData['suffix'], '<small class="error">:message</small>') !!}
</div>
