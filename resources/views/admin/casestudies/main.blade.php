@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li class="current">Case Studies</li>
			</ul>
		</div>
	</div>
	{{--
	<pre>{{ print_r($test ,true) }}</pre>
	<p>{{ serialize($test['opg']) }}</p>
	--}}
	<div class="row">
		<div class="medium-12 columns">
			@if (isset($casestudies) && count($casestudies) != 0)
			<p>Total case studies: {{ count($casestudies) }}<br>
			Business case studies: {{ count($businessCasestudies) }}<br>
			Consumer case studies: {{ count($consumerCasestudies) }}</p>

			<div class="tabWrapper">
				<div class="tabBox" id="main">
					<div class="content">
						<table>
							<tr>
								<th>Title</th>
								<th>category</th>
								<th>active</th>
							</tr>
							@foreach ($casestudies as $casestudy)
							<tr class="{!! ($casestudy['active'] === 1) ? 'active' : 'inactive' !!}" id="casestudy_{{ $casestudy->id }}" data-id="{{ $casestudy->id }}" data-category="{{$casestudy->branch}}" >
								<td class="title_column">{!! $casestudy['title'] !!}</td>
								<td class="branch_column">{!! $casestudy['branch'] !!}</td>
								<td class="active_column">{!! ($casestudy['active'] === 1) ?'yes' : 'no' !!}</td>
								<td><a href="/admin/casestudies/edit/{{ $casestudy->id }}" class="action edit">Edit</a></td>
								<td><a href="/admin/casestudies/destroy/{{ $casestudy->id }}" class="action delete">Delete</a></td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>

				<div class="content">
					<div class="tabBox" id="grid">
					</div>
				</div>
				
				<div class="tabBox" id="businessOrder">
					<h2>Business casestudies</h2>
					@if (isset($businessCasestudies) && count($businessCasestudies) != 0)
					<ul class="caseStudyIndexGrid" data-category="business" data-order="{{ $businessOrder }}">

						@foreach ($businessCasestudies as $casestudy)
						<li class="thumbnail {{$casestudy->thumb_colour}}" data-id="{{$casestudy->id}}" style="background-image:url('{{ $casestudy->thumb_image }}');">
						</li>
						@endforeach
					</ul>
					@else
						<p>No Case Studies yet</p>
					@endif
				</div>
				<div class="tabBox" id="consumerOrder">
					<h2>Consumer casestudies</h2>
					@if (isset($consumerCasestudies) && count($consumerCasestudies) != 0)
					<ul class="caseStudyIndexGrid"  data-category="consumer"  data-order="{{ $consumerOrder }}">
						@foreach ($consumerCasestudies as $casestudy)
						<li class="thumbnail {{$casestudy->thumb_colour}}" data-id="{{$casestudy->id}}" style="background-image:url('{{ $casestudy->thumb_image }}');">
						</li>
						@endforeach
					</ul>
					@else
						<p>No Case Studies yet</p>
					@endif
				</div>
			</div>
			@else
				<p>No Case Studies yet</p>
			@endif

		</div>


	</div>


@endsection
