@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li class="current">People</li>
			</ul>
		</div>
	</div>

	{{-- <pre>{{ print_r($test,true) }}</pre> --}}
	<div class="row peopleList">
		<div class="medium-12 columns">
			@if (isset($people) && count($people) != 0)
				<p>People page layout/order</p>

				<ul id="peopleSorter" class="peopleGallery" data-order="{{ $peopleOrder }}">
					{{--<li class="item-placeholder"></li>--}}
					@foreach ($people as $person)
					<li class="person {{ ($person->active === 1) ? '' : 'inactive' }}" data-id="{{ $person->id }}">
						<div class="frame"><img alt="{!! $person->name !!}" src="{!! $person->portrait !!}"></div>
						<div class="controls">
							<ul>
								<li class="edit"><a href="/admin/people/edit/{{ $person->id }}">Edit</a></li>
								<li class="delete"><a href="/admin/people/destroy/{{ $person->id }}">Delete</a></li>
							</ul>
						</div>
					</li>
					@endforeach
				</ul>

				<table style="width:100%;" id="sortTable" class="display">
				  <thead>
				    <tr>
				      <th scope="column">Person</th>
				      <th scope="column">Show in People page</th>
				      <th scope="column">Author</th>
				      <th scope="column">Active</th>
				      <th scope="column" width="115">Actions</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach ($all as $person)
						<tr>
					      <td class="table-middle"><a href="" class="fancybox">{!! link_to('/admin/people/edit/'.$person['id'], $person['name']) !!}</a></td>
					      <td class="table-middle">{{ ($person['showInPeoplePage'] === 1 ) ? 'yes' :'no' }}</td>
					      <td class="table-middle">{{ ($person['author'] === 1 ) ? 'yes' :'no' }}</td>
					      <td class="table-middle">{{ ($person['active'] === 1 ) ? 'yes' :'no' }}</td>
					      <td class="table-buttons">
					      	<button href="#" data-toggle="drop_{{{ $person['id'] }}}" aria-controls="drop_{{{ $person['id'] }}}" aria-expanded="false" class="button small dropdown mb0">Options</button>
							<ul class="dropdown-pane" id="drop_{{{ $person['id'] }}}" data-dropdown data-auto-focus="true">
								<li>{!! link_to('/admin/people/edit/'.$person['id'], 'Edit') !!}</li>

								<li>{!! Form::model($person, ['url' => 'admin/people/destroy/'.$person['id'], 'method' => 'delete', 'id' => 'form_delete_'.$person['id']]) !!}
								  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this post?"); if (r == true) {document.getElementById("form_delete_'.$person['id'].'").submit();} return false;']) !!}
								  	{!! Form::close() !!}
								</li>
							</ul>

						  </td>
					    </tr>
					@endforeach

				  </tbody>
				</table>


			@else
				<p>No People yet</p>
			@endif

		</div>
	</div>

@endsection
