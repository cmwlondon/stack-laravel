			<div class="formfield">
				{!! Form::label('name', 'Name') !!}
				{!! Form::text('name',(isset($person) ? $person->name : null),['placeholder' => '', 'id' => 'name']) !!}
				{!! $errors->first('name', '<p>:message</p>') !!}
			</div>
			<div class="formfield">
				{!! Form::label('slug', 'Slug') !!}
				{!! Form::text('slug',(isset($person) ? $person->slug : null),['placeholder' => '', 'id' => 'slug']) !!}
			</div>
			<div class="formfield">
				{!! Form::label('role', 'Role') !!}
				{!! Form::text('role',(isset($person) ? $person->role : null),['placeholder' => '', 'id' => 'role']) !!}
				{!! $errors->first('role', '<p>:message</p>') !!}
			</div>
			<div class="formfield ckeditorTarget">
				{!! Form::label('bio', 'Bio') !!}
				{!! Form::textarea('bio',(isset($person) ? $person->bio : null),['placeholder' => '', 'id' => 'bio']) !!}
				{!! $errors->first('bio', '<p>:message</p>') !!}
			</div>

			<div class="formfield media image">
				{!! Form::label('portrait','Portrait') !!}
				<div id="previewPortrait" class="imagePreview">@if (isset($person) && $person->portrait != '')<img alt="" src="{{{ $person->portrait }}}">@endif</div>
				<p class="button tiny" id="portraitTrigger">Select</p>
				{!! Form::text('portrait', (isset($person) && $person->portrait != '') ? $person->portrait : null, ['id' => 'portrait']) !!}
				{!! $errors->first('portrait', '<small class="error">:message</small>') !!}
			</div>

			{{--
			{!! Form::hidden('portraitChange' ) !!}
			<div class="formfield media image">
				<div class="portraitState @if (isset($person) && $person->portrait !== '') thumb @else file @endif ">
					@if (isset($person) && $person->portrait !== '')
					<div class="portraitThumbnail">
						<img alt="" src="/uploads/assets/images/people/{{ $person->portrait }}">
						<a href="" class="replaceImage">Replace Image</a>
					</div>
					@endif 
					<div class="portraitSelector">
						{!! Form::label('portrait', 'Portrait image') !!}
						{!! Form::file('portrait', null, ['id' => 'portrait']) !!}
						{!! $errors->first('portrait', '<p>:message</p>') !!}
					</div>
				</div>

			</div>
			--}}
			

			<div class="formfield social">
				{!! Form::label('twitter', 'Twitter') !!}
				{!! Form::text('twitter',(isset($person) ? $person->twitter : null),['placeholder' => '', 'id' => 'twitter']) !!}
			</div>
			<div class="formfield social">
				{!! Form::label('linkedin', 'Linkedin') !!}
				{!! Form::text('linkedin',(isset($person) ? $person->linkedin : null),['placeholder' => '', 'id' => 'linkedin']) !!}
			</div>
			<div class="formfield social">
				{!! Form::label('telephone', 'Telephone') !!}
				{!! Form::text('telephone',(isset($person) ? $person->telephone : null),['placeholder' => '', 'id' => 'telephone']) !!}
			</div>
			<div class="formfield social">
				{!! Form::label('email', 'Email') !!}
				{!! Form::text('email',(isset($person) ? $person->email : null),['placeholder' => '', 'id' => 'email']) !!}
			</div>

			{{-- macro selectPeopleColour -> app/Services/Macros.php --}}
			<div class="formfield">
				{!! Form::label('colour', 'Underline Colour') !!}
				{!! Form::selectPeopleColour('colour', (isset($person) ? $person->colour : null)) !!}
			</div>
			
			<formfield>
				<legend>Show this person in the 'People' page</legend>
				{!! Form::radio('showInPeoplePage', '1', (isset($person) && $person->ashowInPeoplePage === 1), ['id' => 'show-1'] ) !!} {!! Form::label('show-1', 'Yes') !!} 
				{!! Form::radio('showInPeoplePage', '0', (isset($person) && $person->ashowInPeoplePage === 0), ['id' => 'show-0'] )  !!} {!! Form::label('show-0', 'No') !!}
				{!! $errors->first('showInPeoplePage', '<p>:message</p>') !!}
			</formfield>

			<formfield>
				<legend>Is an author</legend>
				{!! Form::radio('author', '1', (isset($person) && $person->author === 1), ['id' => 'author-1'] ) !!} {!! Form::label('author-1', 'Yes') !!} 
				{!! Form::radio('author', '0', (isset($person) && $person->author === 0), ['id' => 'author-0'] )  !!} {!! Form::label('author-0', 'No') !!}
				{!! $errors->first('author', '<p>:message</p>') !!}
			</formfield>

			<formfield>
				<legend>Status</legend>
				{!! Form::radio('active', '1', (isset($person) && $person->active === 1), ['id' => 'active-1'] ) !!} {!! Form::label('active-1', 'Active') !!} 
				{!! Form::radio('active', '0', (isset($person) && $person->active === 0), ['id' => 'active-0'] )  !!} {!! Form::label('active-0', 'Inactive') !!}
				{!! $errors->first('active', '<p>:message</p>') !!}
			</formfield>
