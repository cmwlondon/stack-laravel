@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li class="current">People</li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="small-12 columns">
			{!! Form::open(array('url' => ['admin/people/store'], 'method' => 'POST', 'id' => 'peopleForm', 'files' => true) )!!}
				@include('admin.people.partials._form')
				<div class="formfield">
					{!! Form::submit('Create', array('class' => 'button success small')) !!}
					{!! link_to('admin/people', 'Cancel', ['class' => 'small button alert']) !!}
				</div>
			{{ Form::close() }}
		</div>

	</div>
@endsection
