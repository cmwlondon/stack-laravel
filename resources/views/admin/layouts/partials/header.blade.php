	<div class="top-bar">
	<div class="top-bar-left">
		<ul class="dropdown menu" data-dropdown-menu>
			<li class="menu-text">Stack Admin</li>
			<li>{!! Html::link('/admin/summary', 'Summary', ['class'=>'']) !!}</li>
			<li>
				{!! Html::link('#', 'Blog', ['class'=>'']) !!}
				<ul class="menu vertical">
					<li>{!! Html::link('/admin/blog', 'List Posts', ['class'=>'']) !!}</li>
					<li>{!! Html::link('/admin/blog/create', 'Create Post', ['class'=>'']) !!}</li>
				</ul>
			</li>

			<li>
				{!! Html::link('#', 'Casestudies', ['class'=>'']) !!}
				<ul class="menu vertical">
					<li>{!! Html::link('/admin/casestudies', 'List Casestudies', ['class'=>'']) !!}</li>
					<li>{!! Html::link('/admin/casestudies/create', 'Create Casestudy', ['class'=>'']) !!}</li>
				</ul>
			</li>
			<li>
				{!! Html::link('#', 'Clients', ['class'=>'']) !!}
				<ul class="menu vertical">
					<li>{!! Html::link('/admin/clients', 'List Clients', ['class'=>'']) !!}</li>
					<li>{!! Html::link('/admin/clients/create', 'Create Client', ['class'=>'']) !!}</li>
				</ul>
			</li>
			<li>
				{!! Html::link('#', 'People', ['class'=>'']) !!}
				<ul class="menu vertical">
					<li>{!! Html::link('/admin/people', 'List People', ['class'=>'']) !!}</li>
					<li>{!! Html::link('/admin/people/create', 'Create Person', ['class'=>'']) !!}</li>
				</ul>
			</li>
			
			{{-- @if (Admin::checkPermission('menu_users')) --}}
			<li>
				{!! Html::link('#', 'Users', ['class'=>'']) !!}
				<ul class="menu vertical">
					<li>{!! Html::link('/admin/users', 'List Users', ['class'=>'']) !!}</li>
					@if (Admin::checkPermission('create_user'))
						<li>{!! Html::link('/admin/users/create', 'Create User', ['class'=>'']) !!}</li>
					@endif
				</ul>
			</li>
			{{-- @endif --}}

		</ul>
	</div>

	<div class="top-bar-right">
		<ul class="dropdown menu" data-dropdown-menu>
			<li>
				{!! Html::link('#', Admin::user('name'), ['class'=>'']) !!}
				<ul class="menu vertical">
					{{-- <li>{!! link_to_route('admin.users.edit', 'My Account', [Admin::user('id')], null) !!}</li> --}}
					<li class="divider"></li>
					<li class="alert">{!! link_to('/logout', 'Logout', [], null) !!}</li>
				</ul>
			</li>
		</ul>
	</div>
</div>
<br/>

{{--
	<ul>
@foreach (Admin::showPermission('menu_users') as $item)
    <li>[{{ $item->id }}] {{ $item->name }}</li>
@endforeach
</ul>
--}}

@include('admin.layouts.partials.responses')
