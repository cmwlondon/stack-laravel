@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li><a href="/admin/clients">Clients</a></li>
			  <li class="current">Create Client Logo</li>
			</ul>

			{!! Form::open(array('url' => ['admin/clients/store'], 'method' => 'POST', 'id' => 'clientForm', 'files' => true) )!!}

			@include('admin.clients.partials._form', ['type' => 'create', 'hidden' => 'hidden'])

			<div class="row">
				
				<div class="small-12 columns">
					{!! Form::submit('Create', array('class' => 'button success small', 'id' => 'bSubmit')) !!}
					{!! link_to('admin/clients', 'Cancel', ['class' => 'small button alert']) !!}
				</div>
			</div>
			{!! Form::close() !!}

		</div>		
	</div>	
@stop
