{{--
clients.id
clients.title
clients.text
clients.logo
clients.active
--}}

{!! Form::hidden('id', $id, ['id' => 'id']) !!}

{{-- <pre>{{ print_r($test, true) }}</pre> --}}
<div class="row">

	<div class="small-12 columns">
		{!! Form::label('title', 'Title') !!}
		{!! Form::text('title',(isset($client) ? $client->title : null),['placeholder' => '', 'id' => 'title']) !!}
		{!! $errors->first('title', '<small class="error">:message</small>') !!}
	</div>
	<div class="small-12 columns mb1">
		{!! Form::label('text', 'Text') !!}
		{!! Form::textarea('text',(isset($client) ? $client->text : null),['placeholder' => '', 'id' => 'text', 'rows' => '2']) !!}
		{!! $errors->first('text', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 columns mb1">
		{!! Form::label('logo','Logo') !!}
		<div id="previewLogo" class="imagePreview">@if (isset($client) && $client->logo != '')<img alt="" src="{{{ $client->logo }}}">@endif</div>
		<p class="button tiny" id="logoTrigger">Select</p>
		{!! Form::text('logo', null, ['id' => 'logo']) !!}
		{!! $errors->first('logo', '<small class="error">:message</small>') !!}
	</div>

	{{--
	<div class="small-12 columns mt1">
		<h5>Active</h5>
		<div class="panel radius">
		  <div class="row">
		  	<div class="columns small-4">{!! Form::radio('active','1',(isset($client) ? $client->active : null), ['class' => 'mb0']) !!} Yes</div>
			<div class="columns small-4">{!! Form::radio('active','0',(isset($client) ? $client->active : null), ['class' => 'mb0']) !!} No</div>
		  </div>
		</div>
		{!! $errors->first('status', '<small class="error">:message</small>') !!}
	</div>
	--}}


</div>