@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li><a href="/admin/clients">Clients</a></li>
			  <li class="current">Edit Client Logo</li>
			</ul>

			{!! Form::model($client, array('url' => ['admin/clients/update/'.$client->id], 'method' => 'PUT', 'files' => true) )!!}
				<div class="row">
					<div class="small-12 columns mt1">
						@include('admin.clients.partials._form')
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns mt1">
						{!! Form::submit('Update', array('class' => 'button success small')) !!}
						{!! link_to('admin/clients', 'Cancel', ['class' => 'small button alert']) !!}

						
					</div>
				</div>
				<input type="hidden" name="update" value="1"/>
			{!! Form::close() !!}
		</div>
		<div class="small-12 columns">
			{!! Form::open(array('url' => 'admin/clients/destroy/'.$client->id, 'method' => 'delete', 'id' => 'del_'.$client->id)) !!}
		  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this post?"); if (r == true) {document.getElementById("del_'.$client->id.'").submit();} return false;', 'class' => 'small button secondary rf']) !!}
		  	{!! Form::close() !!}
		</div>	
	</div>
@stop
