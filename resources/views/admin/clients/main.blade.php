@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li class="current">Clients</li>
			</ul>
		</div>
	</div>

	<div class="row clientList">
		<div class="medium-12 columns">
			<p>Active</p>
			@if (isset($clients) && count($clients) != 0)
			<ul id="clientSorter" data-order="{{ $clientOrder }}">

				@foreach ($clients as $client)
				<li data-id="{!! $client['id'] !!}" title="{!! $client['title'] !!}" class="item">
					<div class="frame"><img alt="{!! $client['title'] !!}" src="{{ $client['logo'] }}"></div>
					<div class="controls">
						<ul>
							<li class="edit"><a href="/admin/clients/edit/{{ $client->id }}">Edit</a></li>
							<li class="delete"><a href="/admin/clients/destroy/{{ $client->id }}">Delete</a></li>
						</ul>
					</div>
				</li>
				@endforeach

			</ul>
			@endif

			<p>Inactive</p>
			<ul id="inactiveClients">

				<li class="item-placeholder @if( count($inactiveClients) != 0) hidden @endif"></li>

				@foreach ($inactiveClients as $client)
				<li data-id="{!! $client['id'] !!}" title="{!! $client['title'] !!}" class="item">
					<div class="frame"><img alt="{!! $client['title'] !!}" src="{{ $client['logo'] }}"></div>
					<div class="controls">
						<ul>
							<li class="edit"><a href="/admin/clients/edit/{{ $client->id }}">Edit</a></li>
							<li class="delete"><a href="/admin/clients/destroy/{{ $client->id }}">Delete</a></li>
						</ul>
					</div>
				</li>
				@endforeach
			
			</ul>

		</div>


	</div>


@endsection
