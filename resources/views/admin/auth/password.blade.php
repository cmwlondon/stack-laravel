@extends('admin.layouts.main')

@section('content')

<div class="row pt2">
	<div class="small-12 columns">
		<div class="row">
			<div class="panel medium-6 medium-offset-3 small-10 small-offset-1 columns">
				<h2>Reset Password</h2>


				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif

				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif

				<form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/password/email') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<label>E-Mail Address</label>
					<input type="email" class="form-control" name="email" value="{{ old('email') }}">

					<button type="submit" class="button success small">Send Password Reset Link</button>
				</form>

			</div>
		</div>
	</div>
</div>

@endsection
