@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')
      <header class="work">
        <!--<h2>{!! $post->title !!}</h2>-->
      </header>

      {{-- <pre>{{ print_r($test,true) }}</pre> --}}

        <article class="stackBlogPost" id="post{{ $id }}">
          <header @if ( $links->count() === 0) class="noAside" @endif>
            <h1>{!! $post->title !!}</h1>
            {{-- <p>{{ $postDate }}</p> --}}
          </header>

          <div class="container">
            <div class="main">
              <div class="inline"><img src="{{ $post->image }}" alt="{!! $post->title !!}"></div>

              <div class="tx">
                {!! $post->body !!}
              </div>

              @include('Main.Blog.layouts.authors')

            </div>

            @include('Main.Blog.layouts.related-links')
          </div>

        </article>
        {{-- @include('Main.Blog.layouts.paging') --}}

      <hr>
      
@endsection

@section('components')
  
@endsection


