@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')

      <header class="lead">
        <h1>Our blog</h1>
      </header>

      {{-- <pre>{{ print_r($test,true) }}</pre> --}}
      <div class="categoryFilter">
        <ul>
          @foreach( $filters AS $filterItem)
            <li @if ( $filter === $filterItem['value'] ) class="selected" @endif><a href="/blog/{{ $filterItem['value'] }}">{{$filterItem['caption']}}</a></li>
          @endforeach
        </ul>
      </div>
      <section class="blogPosts">
      @foreach( $blog as $index => $post )
      <article id="post{{ $post['id'] }}" data-category="{{ $post['category'] }}" class="post loading {{ $post['category'] }} @if ($post['featured']) featured @endif">
        <div class="frame" data-image="{{ $post['image'] }}" data-thumb="{{ $post['thumb'] }}">
          <a href="/blog/{{ $post['slug'] }}" title="{!! $post['title'] !!}" >
          <!-- <img alt="{!! $post['title'] !!}" src="/uploads/assets/images/blog/{{ $post['image'] }}" class="full">
          <img alt="{!! $post['title'] !!}" src="/uploads/assets/images/blog/thumbs/{{ $post['thumb'] }}" class="thumb"> -->
          </a>
        </div>
        <h2>{{ $post['categoryTitle'] }}</h2>
        <h1>{!! $post['title'] !!}</h1>
        <p>{!! $post['synopsis'] !!}</p>
        <a href="/blog/{{ $post['slug'] }}" class="readmore">Read More</a>
        
      </article>
      @endforeach
      </section>
@endsection

@section('components')
  
@endsection


