              <div class="authors">
                <ul>
                  @foreach( $authors AS $author )
                  <li>
                    <div class="frame"><img alt="{!! $author->name !!}" src="{{ $author->portrait }}"></div>
                    <div class="info">
                      <h3>{!! $author->name !!}</h3>
                      <h4>{!! $author->role !!}</h4>
                      <ul class="socialLinks">
                        @if ($author->email && $author->email !== '')
                          <li class="email">email: <a href="mailto:{{ $author->email }}">{{ $author->email }}</a></li>
                        @endif
                        @if ($author->telephone && $author->telephone !== '')
                        <li class="phone"><a href="tel:{{ $author->telephone }}">{{$author->telephone}}</a></li>
                        @endif
                        @if ($author->linkedin && $author->linkedin !== '')
                        <li class="linkedin"><a href="https://www.linkedin.com/company/stackagency" target="_blank" title="linkedin" class="linkedin"  style="background-image:url('/img/linkedin.png');"><span>linkedin</span></a></li>
                        @endif
                        @if ($author->twitter && $author->twitter !== '')
                        <li class="twitter"><a href="https://twitter.com/stackagency" target="_blank" title="twitter" class="twitter" style="background-image:url('/img/twitter.png');"><span>twitter</span></a></li>
                        @endif
                      </ul>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
