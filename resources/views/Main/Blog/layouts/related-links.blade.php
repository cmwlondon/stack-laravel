
@if ( $links->count() > 0) 
<aside>
  <h2>MORE ARTICLES</h2>
  <ul>
  	@foreach( $links AS $link ) 
    <li>
    	@if($link->thumbnail !== '-')<a href="{{ $link->url }}" @if($link->external == 1) target="_blanK" @endif title="{!! $link->title !!}" class="thumb"><img alt="{!! $link->title !!}" src="{{ $link->thumbnail }}"></a>@endif
    	<a href="{{ $link->url }}" @if($link->external == 1 ) target="_blanK" @endif>{!! $link->title !!}</a>
    </li>
    @endforeach
  </ul>
</aside>
@endif