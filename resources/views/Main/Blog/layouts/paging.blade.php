      <div class="paging">
        <div class="p">
          @if( $previous )
        <a href="/blog/{{ $previous['slug'] }}" class="next">&lt; {{ $previous['title'] }}</a>
        @else
        &nbsp;
        @endif
        </div>

        <div class="b"><a href="/blog" class="index">Back to Blogs</a></div>

        <div class="n">
        @if( $next )
        <a href="/blog/{{ $next['slug'] }}" class="next">{{ $next['title'] }} &gt;</a>
        @else
        &nbsp;
        @endif
        </div>
      </div>
