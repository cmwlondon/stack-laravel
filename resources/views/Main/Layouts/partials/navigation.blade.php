		<header class="page">
      @if ( !$isHomePage )
      <a href="/" class="logo" title="STACK"><image alt="STACK" src="/img/stack-splash.png" class="svgimg"></a>
      @endif
      <div class="trigger"><div class="button"><div></div></div></div>
      <div class="menu">
        <div class="logobox">
          @if ( $isHomePage )
          <a href="/" class="logo" title="STACK"><image alt="STACK" src="/img/stack-splash.png" class="svgimg"></a>
          @endif
          
          <div class="address">
            <h2>GET IN TOUCH<br>
            <span>T</span>: 020 7927 3600<br>
            <span>E</span>: <a href="mailto:hello@stackworks.com">HELLO@STACKWORKS.COM</a> 
            </h2>
            <address>90 Tottenham Court Road, London W1T 4TJ</address>
            <p>Part of <a href="http://www.msqpartners.com/" target="_blank">MSQ Partners</a> / &copy; STACK 2018</p>
          </div>
        </div>
        <nav>
          <div class="group fl">
            <a href="/">HOME</a>
            <a href="/consumer">CONSUMER STACK</a>
            <a href="/business">BUSINESS STACK</a>
          </div>
          <div class="group fr">
            <a href="/clients">CLIENTS</a>
            <a href="/people">PEOPLE</a>
            <a href="/blog">BLOG</a>
            <a href="/contact">CONTACT</a>
          </div>
        </nav>
        <aside class="socialmedia">
          <a href="https://www.instagram.com/stacklondon/" target="_blank" title="instagram" class="instagram" style="background-image:url('/img/instagram.png');"><span>instagram</span></a>
          <a href="https://www.linkedin.com/company/stackagency" target="_blank" title="linkedin" class="linkedin"  style="background-image:url('/img/linkedin.png');"><span>linkedin</span></a>
          <a href="https://twitter.com/stackagency" target="_blank" title="twitter" class="twitter" style="background-image:url('/img/twitter.png');"><span>twitter</span></a>
        </aside>
      </div>
		</header>
