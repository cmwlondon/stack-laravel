		<footer class="clearfix">
        <h2>GET IN TOUCH</h2>

        <nav class="phoneAndEmail">  
          <p class="phone"><span>t</span>: <a href="tel:02079273600">020 7927 3600</a></p>
          <p class="email"><span>e</span>: <a href="mailto:hello@stackworks.com">hello@stackworks.com</a></p>
        </nav>

        <address>90 Tottenham Court Road, <br class="mbr">London W1T&nbsp;4TJ</address>

        <nav class="socialMedia">
            <a href="https://twitter.com/stackagency" target="_blank" title="twitter" class="twitter"><span>twitter</span></a>
            <a href="https://www.linkedin.com/company/stackagency" target="_blank" title="linkedin" class="linkedin"><span>linkedin</span></a>
            <a href="https://www.instagram.com/stacklondon/" target="_blank" title="instagram" class="instagram"><span>instagram</span></a>
        </nav>

        <div class="footerLogo">
          <a href="/" class="logo" title="STACK"><img alt="STACK" src="/img/stack-splash.png" class="svgimg"></a>
          <img class="msq" alt="an MSQ agency" src="/img/msq-logo1.png">
        </div>

        <p>&copy; STACK 2021</p>

        {{--
        <p>Part of <a href="http://www.msqpartners.com/" target="_blank">MSQ Partners</a> / &copy; STACK 2020</p>
        <p class="sublinks"><!-- <a href="terms.html">Terms &amp; Conditions</a> | --><a href="/privacy">Privacy Policy</a> | <a href="/sitemap">Sitemap</a></p>
        --}}
    </footer>	
