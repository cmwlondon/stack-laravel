<!DOCTYPE html>
<html class="no-js">
<head>
    <title>{!! $meta['title'] !!}</title>
    <meta charset="utf-8">
    <meta name="description" content="{!! $meta['desc'] !!}" >
    <meta name="keywords" content="{!! $meta['keywords'] !!}" >
    <meta name="author" content="{!! $meta['author'] !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="canonical" href="{{ $canonical }}" />
    @foreach ($meta['og'] AS $key => $value)
    <meta property="og:{{$key}}" content="{{$value}}" />
    @endforeach
    <link rel="icon" type="/image/png" href="/img/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:wght@400;500;700&display=swap" rel="stylesheet">
	<style type="text/css" media="all">
	@import '/css/stack.css?t={{ now()->timestamp }}';
	</style>			
</head>

<body class="{{ $pageClass }} {{ $colourScheme }}">
	<div class="boundary">
	    @include('Main.Layouts.partials.navigation')
	    <section class="main">
			@yield('content')
		</section>	
		@include('Main.Layouts.partials.footer')
	</div>

	<div class="moduleCache"></div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="/js/modernizr.js"></script>
	<script src="/js/stack.js"></script>
	@if ( $extra_js !== '' )
		{!! $extra_js !!}
	@endif
	@include('Main.Layouts.partials.google-analytics')
</body>
</html>
