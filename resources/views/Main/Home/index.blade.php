<!DOCTYPE html>
<html class="no-js">
<head>
    <title>{!! $meta['title'] !!}</title>
    <meta charset="utf-8">
    <meta name="description" content="{!! $meta['desc'] !!}" >
    <meta name="keywords" content="{!! $meta['keywords'] !!}" >
    <meta name="author" content="{!! $meta['author'] !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="canonical" href="{{ $canonical }}" />
    @foreach ($meta['og'] AS $key => $value)
    <meta property="og:{{$key}}" content="{{$value}}" />
    @endforeach
    <link rel="icon" type="image/png" href="/img/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:wght@400;500;700&display=swap" rel="stylesheet">
	<style type="text/css" media="all">
	@import '/css/stack.css?t={{ now()->timestamp }}';
	</style>			
</head>

<body class="homepage">

  <div class="boundary">

    <!-- /sass/stack/sass/_home-logo.scss -->
    <div class="logoAnimation"><a href="/" class="logo" title="STACK"><img alt="STACK" src="/img/stack-splash.png" class="svgimg"></a></div>

    <header class="page">
      <div class="trigger"><div class="button"><div></div></div></div>
      <div class="menu">
        <div class="logobox">
          <!-- <a href="index.html" class="logo" title="STACK"><img alt="STACK" src="/svg/stack2.svg" class="svgimg"></a> -->
          <div class="address">
            <h2>GET IN TOUCH<br>
            <span>T</span>: 020 7927 3600<br>
            <span>E</span>: <a href="mailto:hello@stackworks.com">HELLO@STACKWORKS.COM</a> 
            </h2>
            <address>90 Tottenham Court Road, London W1T 4TJ</address>
            <p>Part of <a href="http://www.msqpartners.com/" target="_blank">MSQ Partners</a> / &copy; STACK 2019</p>
          </div>
        </div>
        <nav>
          <div class="group fl">
            <a href="/">HOME</a>
            <a href="/consumer">CONSUMER STACK</a>
            <a href="/business">BUSINESS STACK</a>
          </div>
          <div class="group fr">
            <a href="/clients">CLIENTS</a>
            <a href="/people">PEOPLE</a>
            <a href="/blog">BLOG</a>
            <a href="/contact">CONTACT</a>
          </div>
        </nav>
        <aside class="socialmedia">
          <a href="https://www.instagram.com/stacklondon/" target="_blank" title="instagram" class="instagram" style="background-image:url('/img/instagram.png');"><span>instagram</span></a>
          <a href="https://www.linkedin.com/company/stackagency" target="_blank" title="linkedin" class="linkedin" style="background-image:url('/img/linkedin.png');"><span>linkedin</span></a>
          <a href="https://twitter.com/stackagency" target="_blank" title="twitter" class="twitter" style="background-image:url('/img/twitter.png');"><span>twitter</span></a>
        </aside>
      </div>
    </header>

    <section class="main">
      <!-- /sass/stack/sass/_home.scss -->

      <!-- /sass/stack/sass/_home-video.scss -->
      {{-- STACK banner video --}}
      <article id="module00" data-logo-color="#fff" class="videoModule module firstModule">
        <div class="posrel">
          <div class="placeholder">
            <img alt="STACK" src="/img/home/stack-placeholder.png">
          </div>
          <div class="vpos">
            <div class="vpos2">
              <div id="hv">
                <img class="msq" alt="an MSQ agency" src="/img/msq-logo1.png">
              </div>
            </div>
          </div>
        </div>
      </article>

      {{-- MBA announce --}}
      <!-- MBA acqusition notice 16/03/2021 -->
      <article class="module mba-announce" id="mba-announce">
        <div class="arrows">
          <h2>A new, nimble agency</h2>
           <p>We've joined forces with MBA to create a new agency designed for today’s fast-paced, digitally transforming and increasingly e&#8209;commerce driven&nbsp;world.</p>
          <a href="https://mbastack.com/" target="_blank">MBASTACK.COM</a>
        </div>
      </article>

      {{-- Everyone's a switcher video popup box --}}
      <article class="module everyones-a-switcher" id="switcher">
        <div class="leftSide">
          <div class="flat"></div>
          <div class="videoPanel"><img alt="" src="/img/home/switcher-video.jpg"></div>
          <h2>Continuous customer acquisition</h2>
          <a href="#" class="videotrigger" data-caption="a sample video" data-video="/video/casestudies/lechameau.mp4" data-poster="/img/casestudy/lechameau/pic3.jpg" title="PLAY"><span>PLAY</span></a>
        </div>
        <div class="rightSide">
          <h2>Everyone&rsquo;s a switcher&nbsp;now</h2>
          <p>Why wouldn&rsquo;t they be? <br>
          There are more new products than ever&nbsp;before.<br>
          More alternatives. An ever-expanding range of things to buy. With more and more consumer choice, why would anyone stick when they can keep twisting&nbsp;forever?</p>
        </div>
      </article>

      {{-- grid background video --}}
      <section class="module videogrid" id="grid1">
        <div class="videobg">
          <div class="fillScreenBox" id="panelVideoBox"></div>
        </div>

        <div class="cflex">
            <div class="b1">
              <div>
                <h3>So for modern brands there are no &lsquo;customers&rsquo; or &lsquo;non&#8209;customers&rsquo;</h3>
                <p>Only people who&rsquo;ve chosen you in the past (but there&rsquo;s no guarantee they&rsquo;ll come&nbsp;back). </p>
                <p>And those who&rsquo;ve never chosen you before (with no guarantee they ever&nbsp;will).</p>
              </div>          
            </div>
            <div class="b2">
              <div>
                <h3>What does this mean for&nbsp;you?</h3>
                <p>To succeed in this environment you must focus on each customer&rsquo;s next choice – not their last&nbsp;one.</p>
                <p>And you need more than just an ad campaign. You need a multichannel flow of ideas and content, supported and steered by data, expressing a single brand message in different ways to remain relevant and&nbsp;interesting.</p>          
              </div>
            </div>
        </div>
      </section>

  <article class="module text" id="text2">
    <div class="stickyBox">
      <div>
        <h2>See what Continuous Customer Acquisition looks like in&nbsp;action.</h2>
      </div>
      <a href="#work1" class="control scrollDown" style="background-image:url('/img/scrolldown.png');"></a>
    </div>
  </article>

  <!-- partials/work2 -->
  <!-- /sass/stack/sass/_home-work.scss -->
      <article class="module work" id="work1">
        <div class="branchButtonContainer">
          <div class="branch consumerBranch">
            <div class="pad">
              <div class="frame">
                <div class="zoomFrame" style="background-image:url('/img/home/consumer.jpg');">
                  <!-- <img alt="CONSUMER" src="/img/modules/m2019/consumer.jpg"> -->
                </div>
              </div>
              <div class="caption">
                  <h3>Consumer</h3>
                  <h4><img alt="STACK" src="/svg/stack2.svg" class="svgimg"></h4>
                  <p>VIEW WORK</p>
              </div>
              <a class="overlay" href="/consumer" title="CONSUMER STACK">Consumer</a>
            </div>
          </div>
          <div class="branch businessBranch">
            <div class="pad">
              <div class="frame">
                <div class="zoomFrame" style="background-image:url('/img/home/business.jpg');">
                  <!-- <img alt="BUSINESS" src="/img/modules/m2019/business.jpg"> -->
                </div>
              </div>
              <div class="caption">
                  <h3>Business</h3>
                  <h4><img alt="STACK" src="/svg/stack2.svg" class="svgimg"></h4>
                  <p>VIEW WORK</p>
              </div>
              <a class="overlay" href="/business" title="BUSINESS STACK">Business</a>
            </div>
          </div>
        </div>
      </article>

  <article class="module text fivesteps" id="fivesteps0">
      <h2>Continuous Customer Acquisition: <br>Five things you can’t afford.</h2>
      <p>Most brands don&rsquo;t have the gigantic budgets of market&nbsp;leaders.<br>
      That doesn&rsquo;t mean you can&rsquo;t afford to acquire new&nbsp;customers.<br>
      But it does mean that there are five things you most definitely can&rsquo;t&nbsp;afford.</p>
  </article>

  <article class="module text fivesteps" id="fivestep1">
    <img alt="1" src="/img/numerals1.png">
    <h3>Acquisition can&rsquo;t afford <br class="mbr">to be&nbsp;wasteful</h3>
    <p>So we build everything on first-party data – which we connect, enrich and deploy to improve targeting and&nbsp;messaging.</p>
  </article>

  <article class="module text fivesteps" id="fivestep2">
          <img alt="2" src="/img/numerals2.png">
          <h3>Acquisition can&rsquo;t afford <br class="mbr">to be off&#8209;message</h3>
          <p>So we always produce a core narrative, translating your brand for your audiences,to keep multichannel campaigns on&nbsp;track.</p>
  </article>

  <article class="module text fivesteps" id="fivestep3">
          <img alt="3" src="/img/numerals3.png">
          <h3>Acquisition can&rsquo;t afford <br class="mbr">to be&nbsp;confusing</h3>
          <p>So we build all our work around a single, powerful idea that encapsulates what makes your brand compelling and&nbsp;unique.</p>
  </article>

  <article class="module text fivesteps" id="fivestep4">
          <img alt="4" src="/img/numerals4.png">
          <h3>Acquisition can&rsquo;t afford <br class="mbr">to be&nbsp;shallow</h3>
          <p>So we create fresh, varied content that helps your audiences experience your products and services before they experience them in real&nbsp;life.</p>
  </article>

  <article class="module text fivesteps" id="fivestep5">
          <img alt="5" src="/img/numerals5.png">
          <h3>Acquisition can&rsquo;t afford <br class="mbr">to be&nbsp;indiscriminate</h3>
          <p>So we build, test and optimise customer journeys to bring the right content to the right people, at the&nbsp;right&nbsp;time.</p>
  </article>

<article class="module journey" id="journey">
  <div class="pic"><img alt="" src="/img/home/journey.jpg"></div>
  <div class="flat"></div>
  <h2>&lsquo;Customer&rsquo; is a journey, not a destination</h2>
</article>

    <!-- position -->
      <ul class="position">
        <li class="on m0" data-anchor="#module0">1</li>
        <li class="m1" data-anchor="#switcher">2</li>
        <li class="m3" data-anchor="#grid">3</li>
        <li class="m4" data-anchor="#text2">4</li>
        <li class="m5" data-anchor="#work1">5</li>
        <li class="m6" data-anchor="#fivesteps0">6</li>
        <li class="m7" data-anchor="#fivesteps1">7</li>
        <li class="m8" data-anchor="#fivesteps2">8</li>
        <li class="m9" data-anchor="#fivesteps3">9</li>
        <li class="m10" data-anchor="#fivesteps4">10</li>
        <li class="m11" data-anchor="#fivesteps5">11</li>
        <li class="m12" data-anchor="#tailpic">12</li>
        <li class="m13" data-anchor="#footer">13</li>
        <!-- <li class="m8" data-anchor="#work1">7</li> -->
        <!--<li class="m9" data-anchor="#cap1">8</li> -->
      </ul>

    </section>

    <footer class="clearfix">
        <h2>GET IN TOUCH</h2>
        <nav class="phoneAndEmail">  
          <p class="phone"><span>t</span>: <a href="tel:02079273600">020 7927 3600</a></p>
          <p class="email"><span>e</span>: <a href="mailto:hello@stackworks.com">hello@stackworks.com</a></p>
        </nav>
        <address>90 Tottenham Court Road, <br class="mbr">London W1T&nbsp;4TJ</address>

        <nav class="socialMedia">
            <a href="https://twitter.com/stackagency" target="_blank" title="twitter" class="twitter" style="background-image:url('/img/footer/twitter.png');"><span>twitter</span></a>
            <a href="https://www.linkedin.com/company/stackagency" target="_blank" title="linkedin" class="linkedin"  style="background-image:url('/img/footer/linkedin.png');"><span>linkedin</span></a>
            <a href="https://www.instagram.com/stacklondon/" target="_blank" title="instagram" class="instagram" style="background-image:url('/img/footer/instagram.png');"><span>instagram</span></a>
        </nav>

        <div class="footerLogo">
          <a href="/" class="logo" title="STACK"><img alt="STACK" src="/img/stack-splash.png" class="svgimg"></a>
          <img class="msq" alt="an MSQ agency" src="/img/msq-logo1.png">
        </div>
        <p>&copy; STACK 2021</p>
        {{--
        <p>Part of <a href="http://www.msqpartners.com/" target="_blank">MSQ Partners</a> / &copy; STACK 2020</p>
        <p class="sublinks"><!-- <a href="terms.html">Terms &amp; Conditions</a> | --><a href="/privacy">Privacy Policy</a> | <a href="/sitemap">Sitemap</a></p>
        --}}
    </footer> 

  </div>

  <div class="popup">
    <div class="header"><h2>VIDEO</h2><a href="#"><span>close</span></a></div>
    <div class="content">
      
    </div>
  </div>
  <div class="black"></div>

  <div class="moduleCache"></div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/jquery.scrollto@2.1.2/jquery.scrollTo.min.js"></script>
	<script src="/js/modernizr.js"></script>
	<script src="js/stack.js"></script>

  @include('Main.Layouts.partials.google-analytics')
  </body>
</html>