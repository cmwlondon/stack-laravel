  <!-- /sass/stack/sass/_home-work.scss -->
      <article class="module work" id="work1">
        <div class="branchButtonContainer">
          <div class="branch consumerBranch">
            <div class="pad">
              <div class="frame">
                <div class="zoomFrame" style="background-image:url('/img/modules/m2019/consumer.jpg');">
                  <img alt="CONSUMER" src="/img/modules/m2019/consumer.jpg">
                </div>
              </div>
              <div class="caption">
                  <h3>CONSUMER</h3>
                  <h4><img alt="STACK" src="/svg/stack2.svg" class="svgimg"></h4>
                  <p>VIEW WORK</p>
              </div>
              <a class="overlay" href="/consumer" title="CONSUMER STACK">CONSUMER</a>
            </div>
          </div>
          <div class="branch businessBranch">
            <div class="pad">
              <div class="frame">
                <div class="zoomFrame" style="background-image:url('/img/modules/m2019/business.jpg');">
                  <img alt="BUSINESS" src="/img/modules/m2019/business.jpg">
                </div>
              </div>
              <div class="caption">
                  <h3>BUSINESS</h3>
                  <h4><img alt="STACK" src="/svg/stack2.svg" class="svgimg"></h4>
                  <p>VIEW WORK</p>
              </div>
              <a class="overlay" href="/business" title="BUSINESS STACK">BUSINESS</a>
            </div>
          </div>
        </div>
      </article>
