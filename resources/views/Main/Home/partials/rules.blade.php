<!-- rules START -->
<section class="rules">
  <div class="videobg">
    <div class="fillScreenBox" id="panelVideoBox">
      <!-- panel video inserted here -->
      <div class="newsprint"></div>
    </div>
  </div>

  <div class="rulesBox">
  
    <article class="module rule" id="rule1">
    <div class="stickyBox red">
    <div class="vm">
    <div class="numeral">
    <div class="numeralCLipper">
    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1080 1080" width="100" height="100">
    <defs>
    <style>
    .digit1-fill{fill:#5aa2e3;}
    </style>
    </defs
    ><title>1-2-3</title>
    <path class="digit1-fill" d="M339.79,299l96.27,46.67a2.16,2.16,0,0,1,1.22,2V841.94a2.16,2.16,0,0,1-.6,1.5L355.6,928.32a2.17,2.17,0,0,0,1.57,3.68H739.71a2.18,2.18,0,0,0,1.57-3.68l-82.41-86.21a2.22,2.22,0,0,1-.61-1.5V148.32a1.31,1.31,0,0,0-1.86-1.2l-316.58,148a2.17,2.17,0,0,0,0,3.92"/></svg>
    </div>
    </div>
    <div class="text">
    <h2 class="zerobm">We translate</h2>
    <h2>before we create.</h2>
    <div class="clipper"><div class="c2">
    <p>Effective communications turn what makes your brand and product extraordinary into language that makes people&nbsp;act.<br></br>So we begin with the real world detail that shows us how your business&nbsp;works.<br></br>Our strategy specialists interrogate you, and our data scientists interrogate your customers. Then we produce a clear, rich translation of your product experience to be the foundation of all of your acquisition&nbsp;communications.</p>
    </div></div>
    </div>
    </div>
    <a href="#rule2" class="control scrollDown" style="background-image:url('/img/scrolldown.png');"></a>
    </div>
    </article>

    <article class="module rule" id="rule2">
    <div class="stickyBox green">
    <div class="vm">
    <div class="numeral">
    <div class="numeralCLipper">
    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1080 1080"  width="100" height="100">
    <defs>
    <style>
    .digit2-fill1{fill:#faa800;}
    .digit2-fill2{fill:none;}
    </style>
    </defs>
    <title>1-2-3</title>
    <path class="digit2-fill1" d="M248.58,222.26,346.9,433a1.6,1.6,0,0,0,2.91,0L461.34,178.52a8.19,8.19,0,0,1,6.74-4.88c8.23-.77,24.85-1.92,32.14.51,9.9,3.3,75.26,14.52,75.26,166.36s-133,333.16-133,333.16l-174.15,227-22.26,27.53a2.34,2.34,0,0,0,1.82,3.81H780.82a4,4,0,0,0,3.89-3.21l50.8-260.17a1.73,1.73,0,0,0-2.89-1.59L748.44,746.5l-320.17.66.66-6.6,169-93.74S700.39,585.4,766.61,500.34c46.15-59.28,64.3-136.59,43.88-208.88-6.16-21.8-15.84-43.59-31-59.87-45.56-48.86-106.38-95.74-293.77-81.86-129.43,9.58-220.24,59.42-235.9,68.54a3.07,3.07,0,0,0-1.22,4"/><rect class="digit2-fill2" x="245.59" y="147.25" width="589.96" height="784.75"/></svg></div></div>
    <div class="text">
    <h2>We fill in the gaps.</h2>
    <div class="clipper"><div class="c2">
    <p>It&rsquo;s always a risk trying something you&rsquo;ve never tried&nbsp;before.</p><p>To your prospects, you&rsquo;re a risk. To their bank balance, their self-respect, their status, maybe their career. And the more questions you leave unanswered, the riskier you&nbsp;look.</p><p>That&rsquo;s why we add layers of fast-moving content strategy and production to traditional creative ideas. So people get to know your product, not just your&nbsp;ads.</p>
    </div></div>
    </div>
    </div>
    <a href="#rule3" class="control scrollDown" style="background-image:url('/img/scrolldown.png');"></a>
    </div>
    </article>

    <article class="module rule" id="rule3">
    <div class="stickyBox blue">
    <div class="vm">
    <div class="numeral">
    <div class="numeralCLipper">
    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1080 1080"  width="100" height="100">
    <defs>
    <style>
    .digit3-fill1{fill:none;}
    .digit3-fill2{clip-path:url(#clip-path);}
    .digit3-fill3{fill:#d485d8;}
    </style>
    <clipPath id="clip-path">
    <rect class="digit3-fill1" x="244.42" y="147.61" width="590.98" height="784.39"/>
    </clipPath>
    </defs>
    <title>1-2-3</title>
    <g class="digit3-fill2"><path class="digit3-fill3" d="M830,666.57C799.33,540.08,623.42,524,623.42,524v-4S838.36,494.13,808.71,311.3C782,146.48,568.12,146.31,526.25,147.87c-4.75.18-9.47.28-14.22.38-26.56.56-125.32,7.15-261.68,70.15l98.82,215.44L462.83,176.9s117.6-38.55,111.67,170c-3.71,130.21-77.92,166-133.15,174.54l-3.35,0,.11,6.66s155.16,2.6,155.16,182.46c0,112.53-41.58,161.43-72.71,182.26a92.18,92.18,0,0,1-50.17,15.11l-22.88.28-104.76-250L244.42,892.39S300.75,922,460.36,930.93c193,10.76,291.53-63.25,291.53-63.25S860.59,793.07,830,666.57"/></g></svg></div></div>
    <div class="text">
    <h2>We never stop. </h2>
    <div class="clipper"><div class="c2">
    <p>Customer acquisition doesn&rsquo;t stop with a sale – because you can never be sure your new customer will come back. In reality, your &lsquo;customers&rsquo; are just the people who chose you the last time they had to make a&nbsp;choice.</p><p>So we don&rsquo;t talk about loyalty. Just continuous acquisition and continuous&nbsp;engagement.</p><p>And we treat your customers just like your prospects. Encouraging them to spend their valuable time with you – not with your&nbsp;rivals.</p>
    </div></div>
    </div>
    </div>
    <a href="#work1" class="control scrollDown" style="background-image:url('/img/scrolldown.png');"></a>
    </div>
    </article>

  </div> 
</section>
<!-- rules END -->
