    <section class="work">
      <div class="branchButtonContainer">
        <div class="branch consumerBranch">
          <div class="pad">
            <div class="frame">
              <div class="zoomFrame" style="background-image:url('/img/branches/button_consumer.jpg');">
                <img alt="CONSUMER" src="/img/branches/button_consumer.jpg">
              </div>
            </div>
            <div class="caption">
                <h3>CONSUMER</h3>
                <h4><image alt="STACK" src="/svg/stack2.svg" class="svgimg"></h4>
                <p>VIEW</p>
            </div>
            <a class="overlay" href="/consumer" title="CONSUMER STACK">CONSUMER</a>
          </div>
        </div>
        <div class="branch businessBranch">
          <div class="pad">
            <div class="frame">
              <div class="zoomFrame" style="background-image:url('/img/branches/button_business.jpg');">
                <img alt="BUSINESS" src="/img/branches/button_business.jpg">
              </div>
            </div>
            <div class="caption">
                <h3>BUSINESS</h3>
                <h4><image alt="STACK" src="/svg/stack2.svg" class="svgimg"></h4>
                <p>VIEW</p>
            </div>
            <a class="overlay" href="/business" title="BUSINESS STACK">BUSINESS</a>
          </div>
        </div>
      </div>
    </section>
