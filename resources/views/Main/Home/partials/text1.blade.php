      <article class="module text" id="text1">
        <div class="stickyBox">
          <div class="vm">
            <h2>More customers. Less&nbsp;risk.</h2>
            <h3>Customer acquisition is a risky&nbsp;business.</h3>
            <div class="clipper"><div class="c2">
              <p>There’s the risk that your ads won’t&nbsp;work.</p>
              <p>The risk that your prospects won’t try your&nbsp;product.</p>
              <p>The risk that your customers will buy from someone else next&nbsp;time.</p>
              <p class="ptop">Agencies often talk about taking&nbsp;risks.</p>
              <p>But we think a good agency should talk about reducing risk&nbsp;first.</p>
              <p>That’s why we apply THREE RULES to reduce risk for our&nbsp;clients.</p>
            </div></div>
          </div>
          <a href="#rule1" class="control scrollDown" style="background-image:url('/img/scrolldown.png');"></a>
        </div>
      </article>
