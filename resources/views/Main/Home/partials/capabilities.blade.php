      <article class="module capabilities" id="cap1">
      <div class="textBlock">
        <h2>WHAT WE OFFER</h2>
      </div>
      <div class="accordions">
        <div class="accordion" id="data_science">
          <div class="control"></div>
          <h3>data science</h3>
          <ul>
<li>Experienced team of data scientists and data&nbsp;strategists</li>
<li>Expertise in omnichannel marketing, developing customer-driven strategies and connected&nbsp;journeys</li>
<li>Build and integrate customer data platforms and provide data-driven DMP&nbsp;optimisation</li>
           </ul>
        </div>
        <div class="accordion" id="strategy">
          <div class="control"></div>
          <h3>strategy</h3>
          <ul>
<li>Broad spectrum of strategic skills - from brand positioning to campaign&nbsp;messaging</li>
<li>Specialists in brand strategy, engagement strategy, content strategy and customer journey&nbsp;planning</li>
<li>Mixture of agency and client&#8209;side&nbsp;backgrounds</li>
            </ul>
        </div>
        <div class="accordion" id="creativity">
          <div class="control"></div>
          <h3>creativity</h3>
          <ul>
<li>Conceptual, writing and art direction, on and offline design, editing, After Effects and&nbsp;retouching</li>
<li>Experts in social and content creation</li>
<li>Streamlined, multi-skilled studio hub – ensures consistent delivery to time and&nbsp;budget</li>
           </ul>
        </div>
        <div class="accordion" id="technology">
          <div class="control"></div>
          <h3>technology</h3>
          <ul>
            
<li>A team of creative technologists and delivery&nbsp;experts</li>
<li>Web, apps, games, ecomms and customer journey concepting, coding and&nbsp;integration</li>
<li>Digital project management</li>
          </ul>
        </div>
        <div class="accordion" id="media">
          <div class="control"></div>
          <h3>media</h3>
          <ul>
            <li>We offer a full range of media planning and buying services through our sister MSQ agency, The&nbsp;Gate</li>
          </ul>
        </div>
      </div>
      </article>
