    <section class="main">
      <article id="module00" data-logo-color="#fff" class="videoModule module firstModule">
        <div class="posrel">
          <div class="placeholder">
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" x_viewBox="0 0 432.42 103.45" viewBox="-10 -10 450 120"><title>STACK</title><defs><filter id="dropshadow" height="130%"><feGaussianBlur in="SourceAlpha" stdDeviation="3" /><feOffset dx="2" dy="2" result="offsetblur" /><feMerge><feMergeNode/><feMergeNode in="SourceGraphic" /></feMerge></filter></defs><path d="M434.78,386.07a8.64,8.64,0,0,0-4.64-6.69,17.83,17.83,0,0,0-8.32-1.77c-7.1,0-10.24,2-10.24,5.6,0,14.74,56.5,5.6,56.5,41.35,0,22.79-19,33.85-46,33.85-25.93,0-42.85-15.15-43.54-32.89H409.4a11.2,11.2,0,0,0,5.32,7.78,17.66,17.66,0,0,0,9.14,2.46c8.05,0,13.37-2.59,13.37-7.23,0-14.87-56.5-4.64-56.5-42.17,0-20.88,18-31.39,42.58-31.39,27.29,0,39.71,13.92,42,31.11Z" transform="translate(-378.55 -354.95)" filter=""  /><path d="M460.9,357.55H551v28.56H522v69.7H489.83v-69.7H460.9Z" transform="translate(-378.55 -354.95)" filter="" /><path d="M525,455.81l35.21-98.26H593l35.21,98.26H596.12l-3.82-13H560.23l-3.68,13Zm51.31-68.65-10,35.21h20.33Z" transform="translate(-378.55 -354.95)" filter="" /><path d="M680.45,395.08c-1-10.24-7.37-16.1-17.47-16.1-11.33,0-18.15,9.69-18.15,28.66,0,13.78,3,26.75,19.38,26.75,10.37,0,15.28-7.23,16.51-16.65h31c-2.86,25.38-21,40.67-46.95,40.67-32.48,0-52.13-19.93-52.13-51.86s21.56-51.58,49-51.58c30,0,46.81,14.88,49.4,40.12Z" transform="translate(-378.55 -354.95)" filter="" /><path d="M707.74,356.19h32.21V388l28.25-31.8H806l-35.89,37,40.87,63H772.24l-24.65-40.92-7.64,8.05v33H707.74Z" transform="translate(-378.55 -354.95)" filter=""  /></svg>
          </div>
          <div class="vpos">
            <div class="vpos2">
              <div id="hv">
                <img class="msq" alt="an MSQ agency" src="/img/msq-logo1.png">
                
              </div>
            </div>
          </div>
        </div>
        <p>CONTINUOUS CUSTOMER ACQUISTION</p>
        <a href="#text1" class="control scrollDown" style="background-image:url('/img/scrolldown.png');"></a>
      </article>

  <article class="module text" id="text1">
    <div class="stickyBox">
      <div>
        <h2>Everyone&rsquo;s a switcher&nbsp;now.</h2>
        <p>Why wouldn&rsquo;t they be? There are more new products than ever&nbsp;before.<br>
        More alternatives. An ever-expanding range of things to buy. With more and more consumer choice, why would anyone stick when they can keep twisting&nbsp;forever?</p>
      </div>
      <a href="#grid1" class="control scrollDown" style="background-image:url('/img/scrolldown.png');"></a>
    </div>
  </article>

  <section class="module videogrid" id="grid1">
    <div class="videobg">
      <div class="fillScreenBox" id="panelVideoBox">
      </div>
    </div>

    <div class="columns">
      <div class="gridrow">
        <div class="column box1">
          <div>
            <h3>So for modern brands there are no &lsquo;customers&rsquo; or &lsquo;non&#8209;customers&rsquo;</h3>
            <p>Only people who&rsquo;ve chosen you in the past (but there&rsquo;s no guarantee they&rsquo;ll come&nbsp;back). </p>
            <p>And those who&rsquo;ve never chosen you before (with no guarantee they ever&nbsp;will).</p>
            </div>          
        </div>
      </div>
      <div class="gridrow">
        <div class="column box2">
          <div>
          <h3>What does this mean for&nbsp;you?</h3>
          <p>To succeed in this environment you must focus on each customer&rsquo;s next choice – not their last&nbsp;one.</p>
          <p>And you need more than just an ad campaign. You need a multichannel flow of ideas and content, supported and steered by data, expressing a single brand message in different ways to remain relevant and&nbsp;interesting.</p>          
        </div>
        </div>
      </div>
    </div>
    {{--<a href="#text2" class="control scrollDown" style="background-image:url('/img/scrolldown.png');"></a> --}}
  </section>

  <article class="module text" id="text2">
    <div class="stickyBox">
      <div>
        <h2>We call this Continuous Customer&nbsp;Acquisition.</h2>
      </div>
      <a href="#work1" class="control scrollDown" style="background-image:url('/img/scrolldown.png');"></a>
    </div>
  </article>

  <!-- partials/work2 -->
  <!-- /sass/stack/sass/_home-work.scss -->
      <article class="module work" id="work1">
        <div class="branchButtonContainer">
          <div class="branch consumerBranch">
            <div class="pad">
              <div class="frame">
                <div class="zoomFrame" style="background-image:url('/img/modules/m2019/consumer.jpg');">
                  <!-- <img alt="CONSUMER" src="/img/modules/m2019/consumer.jpg"> -->
                </div>
              </div>
              <div class="caption">
                  <h3>CONSUMER</h3>
                  <h4><img alt="STACK" src="/svg/stack2.svg" class="svgimg"></h4>
                  <p>VIEW WORK</p>
              </div>
              <a class="overlay" href="/consumer" title="CONSUMER STACK">CONSUMER</a>
            </div>
          </div>
          <div class="branch businessBranch">
            <div class="pad">
              <div class="frame">
                <div class="zoomFrame" style="background-image:url('/img/modules/m2019/business.jpg');">
                  <!-- <img alt="BUSINESS" src="/img/modules/m2019/business.jpg"> -->
                </div>
              </div>
              <div class="caption">
                  <h3>BUSINESS</h3>
                  <h4><img alt="STACK" src="/svg/stack2.svg" class="svgimg"></h4>
                  <p>VIEW WORK</p>
              </div>
              <a class="overlay" href="/business" title="BUSINESS STACK">BUSINESS</a>
            </div>
          </div>
        </div>
      </article>

  <article class="module text fivesteps" id="fivesteps0">
      <h2>Five things you can&rsquo;t afford in&nbsp;acquisition</h2>
      <p>Most brands don&rsquo;t have the gigantic budgets of market&nbsp;leaders.<br>
      That doesn&rsquo;t mean you can&rsquo;t afford to acquire new&nbsp;customers.<br>
      But it does mean that there are five things you most definitely can&rsquo;t&nbsp;afford.</p>
  </article>

  <article class="module text fivesteps" id="fivestep1">
    <img alt="1" src="/img/numerals1.png">
    <h3>Acquisition can&rsquo;t afford to be&nbsp;wasteful</h3>
    <p>So we build everything on first-party data – which we connect, enrich and deploy to improve targeting and&nbsp;messaging.</p>
  </article>

  <article class="module text fivesteps" id="fivestep2">
          <img alt="2" src="/img/numerals2.png">
          <h3>Acquisition can&rsquo;t afford to be off&#8209;message</h3>
          <p>So we always produce a core narrative, translating your brand for your audiences,to keep multichannel campaigns on&nbsp;track.</p>
  </article>

  <article class="module text fivesteps" id="fivestep3">
          <img alt="3" src="/img/numerals3.png">
          <h3>Acquisition can&rsquo;t afford to be&nbsp;confusing</h3>
          <p>So we build all our work around a single, powerful idea that encapsulates what makes your brand compelling and&nbsp;unique.</p>
  </article>

  <article class="module text fivesteps" id="fivestep4">
          <img alt="4" src="/img/numerals4.png">
          <h3>Acquisition can&rsquo;t afford to be&nbsp;shallow</h3>
          <p>So we create fresh, varied content that helps your audiences experience your products and services before they experience them in real&nbsp;life.</p>
  </article>

  <article class="module text fivesteps" id="fivestep5">
          <img alt="5" src="/img/numerals5.png">
          <h3>Acquisition can&rsquo;t afford to be&nbsp;indiscriminate</h3>
          <p>So we build, test and optimise customer journeys to bring the right content to the right people, at the&nbsp;right&nbsptime.</p>
  </article>

  <div class="module tailPic halftone" id="tailpic"><img alt="" src="/img/home/reveal.jpg"></div>

    <!-- position -->
      <ul class="position">
        <li class="on m0" data-anchor="#module0">1</li>
        <li class="m1" data-anchor="#text1">2</li>
        <li class="m3" data-anchor="#videogrid">3</li>
        <li class="m4" data-anchor="#text2">4</li>
        <li class="m5" data-anchor="#work1">5</li>
        <li class="m6" data-anchor="#fivesteps0">6</li>
        <li class="m7" data-anchor="#fivesteps1">7</li>
        <li class="m8" data-anchor="#fivesteps2">8</li>
        <li class="m9" data-anchor="#fivesteps3">9</li>
        <li class="m10" data-anchor="#fivesteps4">10</li>
        <li class="m11" data-anchor="#fivesteps5">11</li>
        <li class="m12" data-anchor="#tailpic">12</li>
        <li class="m13" data-anchor="#footer">13</li>
        <!-- <li class="m8" data-anchor="#work1">7</li> -->
        <!--<li class="m9" data-anchor="#cap1">8</li> -->
      </ul>
</section>