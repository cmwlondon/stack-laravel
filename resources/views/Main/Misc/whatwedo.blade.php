@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')

      <header class="generic">
        <h1>WHAT WE DO AND HOW WE DO&nbsp;IT</h1>        
      </header>

      <div class="inline"><img src="/img/branches/whatwedo/hero.jpg" alt="inline image"></div>

      <div class="textBlock intro">
        <p>We are a customer acquisition and engagement&nbsp;agency.</p>
        <p>We attract new customers to our clients. And we work hard to make them come back for&nbsp;more.</p>
        <p>We believe in a process of continuous acquisition, because your customers are really yesterday&rsquo;s customers, and there&rsquo;s no guarantee they&rsquo;ll come back tomorrow. And we believe in making data-led connections across multiple&nbsp;channels.</p>
        <p>At the heart of our business are four skills that make audiences want to spend time with your&nbsp;brand.</p>
      </div>

      <div class="textBlock titleBlock">
        <h3 class="colourScheme">Data science and implementation</h3>
        <p>Our experienced team of data scientists and strategists are experts in making the most of data-led omnichannel marketing. We have the skillset to build and integrate customer data platforms from scratch and provide data-driven DMP optimisation to increase the efficiency and effectiveness of all acquisition channels.</p>
      </div>

      <div class="textBlock titleBlock">
        <h3 class="colourScheme">Strategy</h3>
        <p>We have a deliberately broad spectrum of strategic skills to let us handle everything from brand positioning to campaign messaging, and provide a fully integrated strategic service. Our team contains specialists in brand strategy, engagement strategy, content strategy and customer journey planning.</p>
      </div>

      <div class="textBlock titleBlock">
        <h3 class="colourScheme">Technology</h3>
        <p>Our technology team combines creative technologists and delivery experts to offer concepting, coding, project management and integration for web, app, e-comms and customer journey projects.</p>
      </div>

      <div class="textBlock titleBlock">
        <h3 class="colourScheme">Creativity</h3>
        <p>Again, we offer a deliberately broad range of specialisms, including concept generation, writing and art direction, social and content creation, on- and offline design, after effects and retouching. Our central, multi-skilled studio hub ensures that creative is streamlined and delivered on time and on budget.</p>
      </div>

      @include('Main.Misc.partials.capabilities')
@endsection

@section('components')
  
@endsection


