      <div class="textBlock capabilities">
        <h2 class="colourScheme">OUR CAPABILITIES</h2>
        <div class="container">
          <div class="row">
            <div class="column">
              <h3>STRATEGY</h3>
              <ul>
                <li>Business transformation</li>
                <li>Brand definition</li>
                <li>Digital strategy</li>
              </ul>
            </div>
            <div class="column">
              <h3>CREATIVE</h3>
              <ul>
                <li>Concepting</li>
                <li>Copywriting &amp; Art Direction</li>
                <li>Multi-channel campaigns</li>
              </ul>
            </div>
            <div class="column">
              <h3>DESIGN</h3>
              <ul>
                <li>Graphic design</li>
                <li>Website design</li>
                <li>Interaction design</li>
              </ul>
            </div>
            <div class="column">
              <h3>TECHNOLOGY</h3>
              <ul>
                <li>Consulting</li>
                <li>Full Stack development</li>
                <li>Platform &amp; API integration</li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="column">
              <h3>DATA</h3>
              <ul>
                <li>Real time intelligence</li>
                <li>Consulting</li>
                <li>Analytics</li>
              </ul>
            </div>
            <div class="column">
              <h3>CONTENT</h3>
              <ul>
                <li>Concepting</li>
                <li>Strategy</li>
                <li>Curation</li>
              </ul>
            </div>
            <div class="column">
              <h3>SOCIAL</h3>
              <ul>
                <li>Strategy</li>
                <li>Influencers</li>
                <li>Copywriting &amp; Design</li>
              </ul>
            </div>
            <div class="column">
              <h3>MEDIA</h3>
              <ul>
                <li>Planning</li>
                <li>Buying</li>
                <li>Partnerships</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
ß