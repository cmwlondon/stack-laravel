       <section class="peopleGrid">
        <article class="person lineYellow" data-name="ben-stephens">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="BEN STEPHENS" src="/img/people/ben-stephens.jpg"></div></div>
            <div class="info"><div>
              <h2>BEN STEPHENS</h2>
              <h3>CHIEF EXECUTIVE OFFICER</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter="">
              <p>Before joining STACK, Ben founded Stephens Francis Whitson (SFW) and built a blue chip client roster over eight years, winning more than 50 awards for creativity and effectiveness. In 2014 SFW was sold to the VCCP Partnership, and renamed VCCPme. Prior to this Ben was MD of Rapier, for half of the time that Campaign awarded it ‘Direct Agency of the Decade' for the&nbsp;noughties.</p>
            </div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>
        <article class="person linePurple" data-name="iain-hunter">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="IAIN HUNTER" src="/img/people/iain-hunter.jpg"></div></div>
            <div class="info"><div>
              <h2>IAIN HUNTER</h2>
              <h3>EXECUTIVE CREATIVE DIRECTOR</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>Iain began his career as a creative writer at WCRS and then Burkitt DDB. He moved to the UK&rsquo;s most awarded agency HTW, and was then promoted to Creative Director at Wunderman running Orange, Land Rover and Microsoft. Iain is in charge of the overall creative output at STACK, producing award-winning campaigns for brands including Peugeot, NIVEA Men and&nbsp;Lotus.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>
        <article class="person lineBlue" data-name="james-champ">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="JAMES CHAMP" src="/img/people/james-champ.jpg"></div></div>
            <div class="info"><div>
              <h2>JAMES CHAMP</h2>
              <h3>CHIEF STRATEGY OFFICER</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>James began his career above the line, working at DFGW, Partners BDDH and AMV, before he decided to expand his horizons and experience other kinds of advertising at the wonderful Craik Jones Watson Mitchell Voelkel. From Craik Jones he moved to iris as Head of Planning, and four years later moved to The Communications Agency and then Rapier as Planning&nbsp;Director.</p><p>James has worked on brands including Land Rover, first direct, The Balvenie, NatWest, American Express, the National Trust, Utterly Butterly (twice) and the Cutty Sark. Highlights include a D&amp;AD Yellow Pencil, a DMA Grand Prix and a Highly Commended (aka Thank You For Turning Up) at the APG Creative Planning&nbsp;Awards.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>
        <article class="person linePurple" data-name="nicola-nimmo" data-portrait="">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="NICOLA NIMMO" src="/img/people/nicola-nimmo.jpg"></div></div>
            <div class="info"><div>
              <h2>NICOLA NIMMO</h2>
              <h3>CLIENT SERVICES DIRECTOR</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>Nicola started her agency career at iris working across a plethora of award winning integrated accounts from COI and Network Rail to Coca Cola. Since then Nicola has worked at a number of agencies including 23red, Publicis and The Communications Agency, building her experience on brands such as Bacardi, Johnson and Johnson, Sainsbury&rsquo;s and Argos - and working across the full mix of channels. She is also a mentor on NABS flagship training scheme Fast Forward, that looks to equip the industry&rsquo;s future leaders with requisite&nbsp;skills.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>
        <article class="person lineGreen" data-name="mark-king">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="MARK KING" src="/img/people/mark-king.jpg"></div></div>
            <div class="info"><div>
              <h2>MARK KING</h2>
              <h3>CHIEF DATA OFFICER</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>Mark brings 19 years&rsquo; experience working in top tier Data &amp; Tech and CRM agencies. Since 2008 he&rsquo;s been the lead data strategy consultant at MRM, Acxiom and Merkle, offering fresh innovative perspectives across many industries on the application of data to tackle business issues, achieving worthwhile results, industry recognition and growth for all involved.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>
        <article class="person lineYellow" data-name="matt-klippel">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="MATT KLIPPEL" src="/img/people/matt-klippel.jpg"></div></div>
            <div class="info"><div>
              <h2>MATT KLIPPEL</h2>
              <h3>HEAD OF TECHNOLOGY</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>Prior to joining STACK Matt was Head of Technology at Proximity London delivering digital technology solutions for some of the biggest brands around. He has also previously held senior technical positions at a number of pure play digital agencies in London and worked across a broad range of enterprise and open source technologies and&nbsp;platforms.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>
        <article class="person lineBlue" data-name="robin-murray">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="ROBIN MURRAY" src="/img/people/robin-murray.jpg"></div></div>
            <div class="info"><div>
              <h2>ROBIN MURRAY</h2>
              <h3>CREATIVE SERVICES DIRECTOR</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>Having previously worked at some of London's premier agencies (including Wunderman, Lowe Direct and VCCP), Robin brings over 20 years&rsquo; experience in creative services to STACK. His solution-driven, hands on approach has seen him deliver multi-channel campaigns around the globe, across pretty much every sector you can think&nbsp;of.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>
        <article class="person lineGreen" data-name="maxine-gregson">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="MAXINE GREGSON" src="/img/people/maxine-gregson1.jpg"></div></div>
            <div class="info"><div>
              <h2>MAXINE GREGSON</h2>
              <h3>DESIGN DIRECTOR</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>With 20 years&rsquo; design experience Maxine has created award-winning design in London and Holland for multiple clients and industry sectors. Her projects include Barclays&rsquo; first online banking platform; website and content development for Universal Music; Peugeot&rsquo;s first to market complete e-commerce experience and global brand launches for L&rsquo;Oreal. Maxine has led the design team at Stack for the last five years, winning a whole bunch of awards along the&nbsp;way.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>

        <article class="person lineYellow" data-name="ian-skinner">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="IAN SKINNER" src="/img/people/ian-skinner.jpg"></div></div>
            <div class="info"><div>
              <h2>IAN SKINNER</h2>
              <h3>HEAD OF DATA SCIENCE</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>Ian has honed his Analytical and Data Science capabilities in the world of Marketing Agencies for over 20 years. Previously heading up roles as Head of Data Science at Merkle and Head of Business Intelligence at MRM MCann, Ian has worked with data both Big and Small across many industries including Automotive, Technology, Publishing and Financial&nbsp;Services.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>

        <article class="person lineYellow" data-name="andy-obrien">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="" src="/img/people/andy-obrien.jpg"></div></div>
            <div class="info"><div>
              <h2>ANDY O&rsquo;BRIEN</h2>
              <h3>HEAD OF DATA PLANNING</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>Andy&rsquo;s experience spans major agencies including RAPP, DDB and Merkle - with specialist expertise in automotive, finance and retail. In recent years Andy has led the connected data charge with his clients giving them a competitive advantage by creating a culture that brings data talent, tools, and decision making together. Providing them with the control to drive their own marketing data destinies and deliver better results. His varied background traversing Customer Experience, Technology, Media and Creative, plays into STACK&rsquo;s channel-agnostic, customer-centric&nbsp;approach.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>

        <article class="person lineYellow" data-name="dan-plotkin">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="DAN PLOTKIN" src="/img/people/dan-plotkin.jpg"></div></div>
            <div class="info"><div>
              <h2>DAN PLOTKIN</h2>
              <h3>CREATIVE DIRECTOR</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>Dan is an award-winning art director and creative director with over 20 years&rsquo; experience creating multi-channel campaigns for clients across every sector. He has a passion for big ideas, a keen eye for detail and emphatically believes in creating work that works. At STACK he is charged with leading the creative output of our automotive brands, Peugeot, Citroen and DS&nbsp;Automobiles.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>
        <article class="person linePurple" data-name="caroline-deput">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="CAROLINE DEPUT" src="/img/people/caroline-deput.jpg"></div></div>
            <div class="info"><div>
              <h2>CAROLINE DEPUT</h2>
              <h3>PLANNING DIRECTOR</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>Caroline honed her strategy skills at the big marketing communications networks (Omnicom, Publicis and WPP). She has helped win creative and effectiveness awards for private and third sector clients, ranging from mobile telecoms to animal&nbsp;charities.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>

        <article class="person lineBlue centred" data-name="ash-shah">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="ASH SHAH" src="/img/people/ash-shah.jpg"></div></div>
            <div class="info"><div>
              <h2>ASH SHAH</h2>
              <h3>FINANCE DIRECTOR</h3>
            </div></div>
            <div class="metadata" data-tel="" data-email="" data-linked="" data-twitter=""><p>From Necker Island to sunny Tottenham Court Road, Ash&rsquo;s glittering career has seen him work for Virgin, Nobu – and, for the last 10 years, MSQ Partners. Since 2014 he&rsquo;s been taking very good care of finance here at&nbsp;STACK.</p></div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>

      </section>
