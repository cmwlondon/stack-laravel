@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')

      <header class="generic">
        <h1>SITEMAP</h1>        
      </header>

      <div class="textBlock">
        <ul class="taxonomy">
          <li class="section"><a href="/">Home</a></li>
          <li class="section">
            <a href="/business">Business Stack</a>
            <ul class="items">
              @foreach( $businessCaseStudies AS $casestudy )
              <li><a href="/casestudy/{{ $casestudy['slug'] }}">{!! $casestudy['title'] !!}</a>
              @endforeach
            </ul>
          </li>
          <li class="section">
            <a href="/consumer">Consumer Stack</a>
            <ul class="items">
              @foreach( $consumerCaseStudies AS $casestudy )
              <li><a href="/casestudy/{{ $casestudy['slug'] }}">{!! $casestudy['title'] !!}</a>
              @endforeach
            </ul>
          </li>

          <li class="section">
            <a href="/blog">Blog</a>
            <ul class="items">
              @foreach( $blogposts AS $blogpost)
              <li><a href="/blog/{{ $blogpost->slug }}">{!! $blogpost->title !!}</a></li>
              @endforeach
            </ul>
          </li>          

          <li class="section"><a href="/clients">Clients</a></li>
          <li class="section"><a href="/people">People</a></li>          
          <li class="section"><a href="/contact">Contact</a></li>          
        </ul>
      </div>


@endsection

@section('components')
  
@endsection


