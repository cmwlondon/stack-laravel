@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')

      <header class="generic">
        <h1>Business</h1>        
      </header>

      <div class="workButtonContainer">
        <div class="buttons">

          @foreach( $casestudies as $casestudy )
          <div class="workItem {{ $casestudy['thumb_colour'] }} wk-{{ $casestudy['slug'] }}" data-id="{{ $casestudy['id'] }}">
            <div class="pad">
              <div class="frame">
                <div class="zoomFrame" style="background-image:url('{{ $casestudy['thumb_image'] }}');">
                </div>
              </div>
              <div class="caption">
                <div class="c2">
                  <h3>{!! $casestudy['thumb_main'] !!}</h3>
                  <h4>{!! $casestudy['thumb_sub'] !!}</h4>
                </div>
              </div>
              <a class="overlay" href="casestudy/{{ $casestudy['slug'] }}" title="{!! $casestudy['title'] !!}">{!! $casestudy['title'] !!}</a>
            </div>
          </div>
          @endforeach

        </div>
      </div>

@endsection

@section('components')
  
@endsection


