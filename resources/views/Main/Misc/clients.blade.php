@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')

      <header class="generic">
        <h1>Clients</h1>        
      </header>

      <section class="clientlist">

        @foreach($clients as $client)
        <article class="pad">
          <div class="pad">
            <a href="" class="focus">view</a>
            <div class="logo" data-logo="{{ $client['logo'] }}" style="background-image:url('{{ $client['logo'] }}');">
            </div>
            <div class="info">
              <h2>{!! $client['title'] !!}</h2>
              <p>{!! $client['text'] !!}</p>
            </div>
          </div>
        </article>
        @endforeach

    	</section>
	
@endsection

@section('components')
  
@endsection


