@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')

      <header class="generic">
        <h1>PRIVACY POLICY</h1>        
      </header>


      <div class="textBlock privacyPolicy">

<h2>Introduction</h2>
<p>Our Privacy Policy will help you to understand what information we collect and process, how we protect and use it, and what choices you have about your personal data.</p>

<p>When we refer to &rdquo;Stack&rdquo; within this Privacy Policy, we are referring to Stack Works Limited of 90 Tottenham Court Road, London, W1T 4TJ, the organisation which provides this website, and any services or features which may be made available to you from this website.</p>

<h2>Data Protection Framework</h2>
<p>Stack is based within the United Kingdom, and as such is registered with the Information Commissioner’s Office (ICO) as a Data Controller under the UK Data Protection Act of 1998. We have also aligned our Privacy Policy with the EU General Data Protection Regulation (&rdquo;GDPR&rdquo;) which comes into effect on 25th May 2018, under the supervision of the ICO within the UK.</p>

<p>Stack has completed applicable Privacy Impact Assessments (also known as Data Protection Impact Assessments under GDPR) for activities related to this website, and these are available upon request.</p>

<h3 id="section1">1. Customer and Citizen&nbsp;Data</h3>
<p>You may decide to send us your personal information via this website if you are seeking more information, requesting to attend one of our events, or for other similar purposes. Your decision to disclose your personal data is entirely voluntary, and by doing so, you are taking an affirmative action by providing us with specific consent to use your personal data only for the purposes for which you have disclosed it to us.</p>

<p>Stack may access and use your personal data only for the purposes for which you have submitted it to us to (a) provide information to you, (b) make contact with you, (c) provide services to you, or (d) maintain the operations and security of the website and services we provide to you. We will not use your personal information for any other purposes, for example for the communication of marketing materials, unless we have your specific consent that permits us to do so.</p>

<p>We will at all times handle and store your personal data in accordance with industry best practice aligned with ISO27001, the international standard for information security. This includes the activities and procedures undertaken by our own personnel and the technical controls which we have implemented to prevent unauthorised access, compromise or theft of information from our applications, supporting computer systems and premises.</p>

<h3 id="section2">2. Sensitive Personal&nbsp;Data</h3>
<p>GDPR specifies a set of personal data categories which are &rdquo;sensitive&rdquo;, and which require special consideration by Data Controllers. This website, and any services available from this website, do not knowingly collect or process any sensitive personal data, and supporting Privacy Impact Assessments (also known as Data Protection Impact Assessments under GDPR) are available upon request (see <a href="#section8" title="Section 8. Contacting Stack" class="text-link">Section 8</a>).</p>

<h3 id="section3">3. Children&rsquo;s Personal&nbsp;Data</h3>
<p>This website, and any services available from this website, are not directed to children under the age of 13. If you learn that a child under the age of 13 has provided us with their personal information without having parental consent, please contact the Stack immediately so that we can take appropriate action.</p>

<h3 id="section4">4. Customer and Citizen Data&nbsp;Rights</h3>
<p>As prescribed within data protection regulations, you have specific rights connected to the provision of your personal data to Stack using this website. These include your rights to request we:</p>
<ul>
<li>confirm to you what personal data we may hold about you, if any, and for what&nbsp;purposes</li>
<li>change the consent which you have provided to us in relation to your personal&nbsp;data</li>
<li>correct any inaccurate or incomplete personal data which we may hold about&nbsp;you</li>
<li>provide you with a complete copy of your personal data for you to move&nbsp;elsewhere</li>
<li>stop the processing of your personal data, whilst an objection from you is being&nbsp;resolved</li>
<li>permanently erase all your personal data promptly, and confirm to you that this has been&nbsp;done</li>
</ul>

<p>(there may be reasons why we may be unable to do this)</p>

<p>To contact Stack, please see <a href="#section8" title="Section 8. Contacting Stack" class="text-link">Section 8</a> below.</p>

<p>If Stack does not address your request, or fails to provide you with a valid reason why we have been unable to do so, you have the right to contact the Information Commissioner’s Office to make a compliant. They can be contacted via their website (<a href="http://www.ico.org.uk" target="_blank" class="text-link">www.ico.org.uk</a>) or by telephone <a href="tel:0303 123 1113"  class="text-link">0303&nbsp;123&nbsp;1113</a>.</p>

<h3 id="section5">5. Website Cookies</h3>

<p>This website uses cookies to record log data. We use both session-based and persistent cookies, dependent upon how you use or interact with this website.</p>

<p>Cookies are small text files sent by us to your computer, and from your computer or mobile device to us each time you visit our website. They are unique to you or your web browser. Session-based cookies last only while your browser is open and are automatically deleted when you close your browser session. Persistent cookies last until you or your browser delete them, or until they expire.</p>

<p>We use cookies which allow us to undertake website analytics and customization, among other similar things. If you decide to disable some or all cookies, you may not be able to use some of the functions on our website. We may use third-party cookies, for example Google Analytics, and you may choose to opt-out of third party cookies by visiting their website.</p>

<h3 id="section6">6. External Links</h3>
<p>This website may include relevant hyperlinks to external websites not controlled by us. Whilst all reasonable care has been exercised in selecting and providing any such links, you are advised to exercise caution before clicking any external links. We cannot guarantee the ongoing suitability of external links, nor do we continually verify the safety or security of the contents which may be subsequently provided to you. You are advised, therefore, that your use of external links is at your own risk and we cannot be responsible for any damages or consequences from your use of them.</p>


<h3 id="section7">7. Changes to this Privacy&nbsp;Policy</h3>
<p>We may change this Privacy Policy from time to time, and if we do we will post any changes on this page. If you continue to access this website or use services available from this website after those changes have come into effect, you will have agreed to the revised policy.</p>

<p>This Privacy Policy is version 1.0, and was released on 25.05.18. You are advised to download or print a copy and retain it for your records.</p>

<h3 id="section8">8. Contacting Stack</h3>
<p>If you have any questions about this Privacy Policy, would like to exercise any of your statutory rights, or to make a complaint, please write to:</p>

<address>Stack Works Limited<br>
90 Tottenham Court Road, London, W1T 4TJ</address>

      </div>

@endsection

@section('components')
  
@endsection


