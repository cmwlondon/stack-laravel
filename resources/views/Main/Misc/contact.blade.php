@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')

      <header class="generic">
        <h1>Contact us</h1>        
      </header>

      <div class="textBox">
        <address><a href="https://www.google.co.uk/maps/place/Tottenham+Court+Rd,+Fitzrovia,+London+W1T+4TJ/@51.5220482,-0.1381659,17z/data=!3m1!4b1!4m5!3m4!1s0x48761b293a4be25f:0xa8c2275ab2d6d797!8m2!3d51.5219813!4d-0.1360347" target="_blank">90 Tottenham Court Road,<br>
          London W1T 4TJ</a></address>

        <div class="container">
          <div class="row">
            <div class="col general">
              <h3>FOR GENERAL ENQUIRIES:</h3>
              <a href="mailto:hello@stackworks.com">hello@stackworks.com</a>
              <a href="tel:02079273600">020 7927 3600</a>
              </div>
            <div class="col newbusiness">
              <h3>FOR NEW BUSINESS ENQUIRIES: </h3>
              <a href="mailto:ben.stephens@stackworks.com">ben.stephens@stackworks.com</a>
              <a href="tel:02079273600">020 7927 3600</a>
              <a href="tel:07768387175">07768 387 175</a>
            </div>
          </div>
        </div>
      </div>

      <div class="map" data-bg="background-image:url('dist/img/contact.jpg');" id="stackmap"></div>

@endsection

@section('components')
  
@endsection


