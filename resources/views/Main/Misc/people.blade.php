@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')

      <header class="generic">
        <h1>People</h1>        
      </header>

       <section class="peopleGrid">
        @foreach( $people as $index=>$person )
        <article class="person {{ $person['colour'] }}@if ($index === ( count($people) - 1 ) ) lastPerson @endif" data-name="{{ $person['slug'] }}" data-portrait="{{ $person['portrait'] }}" data-index="{{ $index }}">
          <div class="box">
            <div class="portrait"><div class="frame"><img alt="{!! $person['name'] !!}" src="{{ $person['portrait'] }}"></div></div>
            <div class="info"><div>
              <h2>{!! $person['name'] !!}</h2>
              <h3>{!! $person['role'] !!}</h3>
            </div></div>
            <div class="metadata" data-tel="{!! $person['telephone'] !!}" data-email="{!! $person['email'] !!}" data-linked="{!! $person['linkedin'] !!}" data-twitter="{!! $person['twitter'] !!}">{!! $person['bio'] !!}</div>
            <a href="" class="triggerbio">view bio</a>
          </div>
        </article>
        @endforeach
      </section>

@endsection

@section('components')
  
@endsection


