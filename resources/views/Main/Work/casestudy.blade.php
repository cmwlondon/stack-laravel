@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')
      <header class="work">
        <h2>Work</h2>        
      </header>

      {{--
      <pre>{{ print_r($test,true) }}</pre>
      --}}
      @foreach( $modules as $module)
      {{--
        <pre>{{ $module['module-id'] }} {{ $module['type-id'] }} {{ $module['type'] }}</pre>
        <pre>{{ print_r($module['meta'],true)}}</pre>
      --}}

      {{--
      --}}
         @include('Main.Work.Modules.'.$module['type'])
      @endforeach
      <div class="buttonBox">
        <a href="/{{ $category }}" id="casestudyBackLink">BACK TO {{ strtoupper($category) }} STACK</a>
      </div>
      <hr>
      
@endsection

@section('components')
  
@endsection


