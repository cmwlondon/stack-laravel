@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')

      <header class="work">
        
        <div class="fixer" id="fixerWork">
          <div class="fixerItem">
            <h1>WORK</h1>        
          </div>
        </div>

      </header>

      <div class="workButtonContainer">

        <div class="buttons initial">
          
          @foreach( $casestudies as $casestudy )
          <div class="workItem {{ $casestudy['colour'] }} {{ $casestudy['branch'] }}">
            <div class="pad">
              <div class="frame">
                <div class="zoomFrame" style="background-image:url('/img/work/{{ $casestudy['image'] }}');">
                </div>
              </div>
              <div class="caption">
                <div class="c2">
                  <h3>{!! $casestudy['t1'] !!}</h3>
                  <h4>{!! $casestudy['t2'] !!}</h4>
                </div>
              </div>
              <a class="overlay" href="work/{{ $casestudy['slug'] }}/{{ $casestudy['id'] }}" title="{!! $casestudy['t1'] !!} - {!! $casestudy['t2'] !!}">{!! $casestudy['t1'] !!} - {!! $casestudy['t2'] !!}</a>
            </div>
          </div>
          @endforeach

        </div>

        <div class="buttons extra">
          <div class="clip">
          </div>
        </div>

        {{--
        <div class="control">
          <div class="clip">
            <a href="#" class="viewmore">VIEW MORE</a>
          </div>
        </div>
        --}}

      </div>
@endsection

@section('components')
  
@endsection


