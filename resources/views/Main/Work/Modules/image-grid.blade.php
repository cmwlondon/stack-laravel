<div class="multiImage grid2x2">
@foreach( $module['meta']['images'] as $image)
<div class="inline"><img src="{{ $image }}" alt="{{ $module['meta']['alt'] }}"></div>
@endforeach
</div>