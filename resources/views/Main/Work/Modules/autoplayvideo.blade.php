<div class="autovideo autovideoflex"  id="{{ $module['meta']['vid'] }}" @if ( $module['meta']['mp4'] !== '' ) data-mpv="{{ $module['meta']['mp4'] }}" @endif @if ( !is_null($module['meta']['webm'])  ) data-webm="{{ $module['meta']['webm'] }}" @endif @if ( !is_null($module['meta']['ogv']) ) data-ogv="{{ $module['meta']['ogv'] }}" @endif data-poster="{{ $module['meta']['poster'] }}">
	
</div>