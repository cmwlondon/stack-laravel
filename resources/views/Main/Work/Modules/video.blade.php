      <div class="video {{ $module['class'] }} {{ $module['link'] }}" id="{{ $module['video-id'] }}">
        <video preload="metadata" controls loop playsinline>
          <source src="/video/casestudies/{{ $module['source']['mp4'] }}" type="video/mp4">
          <source src="/video/casestudies/{{ $module['source']['webm'] }}" type="video/webm">
          <source src="/video/casestudies/{{ $module['source']['ogv'] }}" type="video/ogg">
        </video>
        <img class="placeholder" src="/img/casestudy/{{ $module['image'] }}" alt="">
        <a class="playButton" href="#" style="background-image:url('/img/casestudy/videoplay.png');" title="PLAY"><span>PLAY</span></a>
      </div>
