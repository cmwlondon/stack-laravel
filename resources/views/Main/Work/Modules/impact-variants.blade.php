      <div class="textBlock" id="pointTrigger">
        <h3 class="colourScheme">IMPACT</h3>
        <p>With their meticulous, knowledgeable approach, all Bartlett need is a way in &#8211; and their new collateral has given them the polish to get a foot in the door. In a world where independents are under pressure, Bartlett is thriving and even expanding into new specialist areas.</p>
      </div>

      <div class="textBlock" id="pointTrigger">
        <h3 class="colourScheme">IMPACT</h3>
        <p>THE RESULTS WERE OVERWHELMING</p>
        <div class="points onePoint">
          <div class="point" id="p11">
            <p><span class="l">34:1</span><br>ROI</p>
          </div>
        </div>
      </div>

      <div class="textBlock" id="pointTrigger">
        <h3 class="colourScheme">IMPACT</h3>
        <div class="points twoPoints">
          <div class="point" id="p11">
            <p><span class="l">4</span> TO <span class="l">8</span> MIN<br>DWELL TIMES ON VLOGGER&nbsp;CONTENT</p>
          </div>
          <div class="point" id="p12">
            <p><span class="l">N<sup>o</sup>1</span><br>RANKING IN THE PRESTIGE WOMEN&#8217;S EYE&nbsp;MARKET</p>
          </div>
        </div>

      </div>

      <div class="textBlock" id="pointTrigger">
        <h3 class="colourScheme">IMPACT</h3>

        <div class="points threePoints">
          <div class="point" id="p11">
            <p><span class="l">40</span><span class="s"><sup>+</sup></span><span class="l">%</span><br>RESPONSE RATE</p>
          </div>
          <div class="point" id="p12">
            <p>RESULTED&nbsp;IN<br><span class="l">17</span><br>MEETINGS</p>
          </div>
          <div class="point" id="p13">
            <p><span class="l">2.4</span><span class="s">M</span><br>OF NEW BUSINESS</p>
          </div>
        </div>

      </div>
