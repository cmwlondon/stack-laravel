      <div class="textBlock titleBlock">
        <h1>{!! $module['title'] !!}</h1>
        <h2>{!! $module['subtitle'] !!}</h2>
        <h3 class="colourScheme">{!! $module['subhead'] !!}</h3>
        {!! $module['copy'] !!}
      </div>
