  <div class="video {{ $module['meta']['class'] }}" id="{{ $module['meta']['vid'] }}">
    <video preload="metadata" controls loop  playsinline>

@if ( $module['meta']['mp4'] !== '' )
      <source src="{{ $module['meta']['mp4'] }}" type="video/mp4">
@endif
@if ( $module['meta']['webm'] !== '' )
      <source src="{{ $module['meta']['webm'] }}" type="video/mp4">
@endif
@if ( $module['meta']['ogv'] !== '' )
      <source src="{{ $module['meta']['ogv'] }}" type="video/mp4">
@endif

    </video>
    <img class="placeholder" src="{{ $module['meta']['poster'] }}">
    <a class="playButton" href="#" style="background-image:url('/img/casestudy/videoplay.png');" title="PLAY"><span>PLAY</span></a>
  </div>
