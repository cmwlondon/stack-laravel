      <div class="textBlock" id="pointTrigger">
        <h3 class="colourScheme">IMPACT</h3>

        @if ( $module['intro'] !== '' )
        <p>{!! $module['intro'] !!}</p>
        @endif

        @if ( count($module['items']) > 0  )
            <div class="points {{ $impact_options[ count($module['items']) ] }}">

              @foreach ($module['items'] as $item)
              <div class="point" id="p1{{ $loop->iteration}}">
                {!! $item !!}
              </div>
              @endforeach
            </div>
        @endif

      </div>
