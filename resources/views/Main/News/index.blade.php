@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')

      <header class="generic">
        <h1>NEWS</h1>        
      </header>

      <section class="newsItems">

        @foreach($stories as $story)
        <article>
          <div class="image">
            <!-- news/news-{{ $story['page'] }}.html -->
            <a href="/news/{{ $story['page'] }}/{{ $story['id'] }}" class="frame" style="background-image:url('/img/news/{{ $story['story'] }}/thumb.jpg');" title="{!! $story['title'] !!}"></a>
          </div>
          <div class="text">
            <div class="clip">
              <p class="date">{!! $story['date'] !!}</p>
              <h2>{!! $story['title'] !!}</h2>
              
              <a href="/news/{{ $story['page'] }}/{{ $story['id'] }}">READ MORE</a>
            </div>
          </div>
        </article>
        @endforeach

      </section>


@endsection

@section('components')
  
@endsection


