@extends('Main.Layouts.main')

@section('header')

@endsection

@section('content')
      <header class="generic">
        <h2>NEWS</h2>        
      </header>

      <div class="inline"><img src="../dist/img/news/201807-data-chief/banner.jpg" alt="Stack hires first Data Chief"></div>

      <div class="textBlock">
        <h1>{!! $stories['item']['title'] !!}</h1>
        <h2>{!! $stories['item']['date'] !!}</h2>
        <p>COPY</p>
        <a class="back" href="/news">BACK</a>
      </div>

		<section class="otherNewsItems">
			<div class="workButtonContainer">
				<div class="buttons">
			        @foreach($stories['extras'] as $story)
			           <div class="workItem lineBlue">
			            <div class="pad">
			              <div class="frame">
			                <div class="zoomFrame" style="background-image:url('/img/news/{{ $story['story'] }}/thumb.jpg');"></div>
			              </div>
			              <div class="caption">
			                <div class="c2">
			                  <h3>{!! $story['title'] !!}</h3>
			                  <h4>{!! $story['date'] !!}</h4>
			                </div>
			              </div>
			              <a class="overlay" href="/news/{{ $story['page'] }}/{{ $story['id'] }}" title="">[TITLE]</a>
			            </div>
			          </div>
			        @endforeach
				</div>
			</div>
		</section>

@endsection

@section('components')
  
@endsection


