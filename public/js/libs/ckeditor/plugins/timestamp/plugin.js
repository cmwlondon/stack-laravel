CKEDITOR.plugins.add( 'timestamp', {
    icons: 'timestamp', // plugins/timestamp/icons/timestamp.png
    init: function( editor ) {
        //Plugin logic goes here.

        // define what the plugin does
        // inserts a timestamp into the body at the current cursor position
        // define command 'insertTimestamp'
        editor.addCommand( 'insertTimestamp', {
		    exec: function( editor ) {
		        var now = new Date();
		        editor.insertHtml( 'The current date and time is: <em>' + now.toString() + '</em>' );
		    }
		});

        // add a button to the toolbar
		editor.ui.addButton( 'Timestamp', {
		    label: 'Insert Timestamp', // button label / alt tag
		    command: 'insertTimestamp', // link to command defined above 'insertTimestamp'
		    toolbar: 'insert' // add new button to 'insert' toolbar
		});		
    }
});