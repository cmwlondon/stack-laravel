/*
loopWorker
provides a steady timing signal 'tick' for the image rotation animation on the home page
runs as a background web worker thread and is not affected when the user switches between tabs
prevents the flurry of animations when the user switches back to the stack tab after viewing other tabs
*/

var duration,
	timer;

/*
messages:
['start', duration] <- ParallaxScroll
['stop'] <- ParallaxScroll

'tick' -> ParallaxScroll
*/

onmessage = function(e){
	
	var action = e.data[0];

	switch( action ) {
		case "start" : {
			duration = e.data[1];
			postMessage("grid_worker action: start duration: " + duration);

			/* */
			clearInterval(timer);
			timer = setInterval(function(){
				tick();
			}, duration);
			/* */
		} break;
		case "stop" : {
			postMessage("grid_worker action: stop");
			clearInterval(timer);
		} break;
	}
}

function tick() {
	postMessage("tick");
}
	/* ---------------------------------------------------------------------------- */


